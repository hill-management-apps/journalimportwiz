﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImport
{
    class History
    {
        private string _entityID = "";
        private string _acctNum = "";
        private string _department = "";
        private string _ref = "";
        private string _addlDesc = "";
        private decimal _itemAmt = 0M;
        private string _batchId = "";
        private DateTime _checkDt = DateTime.MinValue;
        private string _checkPd = "";
        private string _vendId = "";
        private string _item = "";
        private string _expPed = "";
        private string _checkNo = "";
        private string _bankId = "";
        private string _jobCode = "";

        public string EntityID { get => _entityID; set => _entityID = value; }
        public string AcctNum { get => _acctNum; set => _acctNum = value; }
        public string Department { get => _department; set => _department = value; }
        public string Ref { get => _ref; set => _ref = value; }
        public string AddlDesc { get => _addlDesc; set => _addlDesc = value; }
        public decimal ItemAmt { get => _itemAmt; set => _itemAmt = value; }
        public string BatchId { get => _batchId; set => _batchId = value; }
        public DateTime CheckDt { get => _checkDt; set => _checkDt = value; }
        public string CheckPd { get => _checkPd; set => _checkPd = value; }
        public string VendId { get => _vendId; set => _vendId = value; }
        public string Item { get => _item; set => _item = value; }
        public string ExpPed { get => _expPed; set => _expPed = value; }
        public string CheckNo { get => _checkNo; set => _checkNo = value; }
        public string BankId { get => _bankId; set => _bankId = value; }
        public string JobCode { get => _jobCode; set => _jobCode = value; }

        public override bool Equals(object obj)
        {
            return obj is History history &&
                   _entityID == history._entityID &&
                   _acctNum == history._acctNum &&
                   _department == history._department &&
                   _ref == history._ref &&
                   _addlDesc == history._addlDesc &&
                   _itemAmt == history._itemAmt &&
                   _batchId == history._batchId &&
                   _checkDt == history._checkDt &&
                   _checkPd == history._checkPd &&
                   _vendId == history._vendId &&
                   _item == history._item &&
                   _expPed == history._expPed &&
                   _checkNo == history._checkNo &&
                   _bankId == history._bankId;
        }

        public override int GetHashCode()
        {
            int hashCode = 1320060537;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_entityID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_acctNum);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_department);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_ref);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_addlDesc);
            hashCode = hashCode * -1521134295 + _itemAmt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_batchId);
            hashCode = hashCode * -1521134295 + _checkDt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_checkPd);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_vendId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_item);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_expPed);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_checkNo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_bankId);
            return hashCode;
        }
    }
}
