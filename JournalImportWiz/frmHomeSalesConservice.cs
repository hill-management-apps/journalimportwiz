﻿using JournalImportWiz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

namespace JournalImport
{
    public partial class frmHomeSalesConservice : Form
    {
        private List<Invoice> invoices = new List<Invoice>();
        private List<Check> checks = new List<Check>();
        private string strSQL = "";
        private string directory = Properties.Settings.Default["directoryConservice"].ToString().Trim();
        private const string conserviceVendId = "CONSYN";
        private const string intFMarker = "Conservice ACH";
        private int year = 0;
        private int month = 0;
        private OpenFileDialog dialog = new OpenFileDialog();
        private SqlAgent sqlAgent = new SqlAgent(ConfigurationManager.AppSettings["dbName"]);
        private List<string> lstEntities = new List<string>();
        public frmHomeSalesConservice()
        {
            InitializeComponent();
        }

        private void frmHomeSalesConservice_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.Text + " " + sqlAgent.DbName.Trim();
                year = DateTime.Now.Year;
                month = DateTime.Now.Month;

                GetCSVFiles(directory);

                txtPath.Text = directory.Trim();

                DataSet locTbl;

                strSQL = @"SELECT BLDGID as ENTITYID FROM BLDG
                           WHERE BLDGID <> '502'
                           UNION
                           SELECT RMPROPID FROM RMPROP";

                locTbl = sqlAgent.ExecuteQuery(strSQL);

                if(locTbl != null && locTbl.Tables.Count > 0)
                {
                    foreach(DataRow row in locTbl.Tables[0].Rows)
                    {
                        lstEntities.Add(row["ENTITYID"].ToString().Trim());
                    }
                }
            }
            catch (Exception err)
            {
                lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());
            }
        }

        /// <summary>
        /// Creates a <see cref="Check"/> object and adds it to the list of checks.
        /// </summary>
        /// <param name="total">This is the check total.</param>
        /// <param name="invoice">This is the invoice number.</param>
        /// <param name="entityID">This is the entity ID.</param>
        private void CreateCheck(decimal total, Invoice invoice, string entityID, string file, int start, int i)
        {
            try
            {
                DataSet locTbl = new DataSet();
                Check check = new Check();
                string bankId = "";
                string lastCheck = "";
                bool done = false;
                int achNo = 0;

                //this gets the Bank ID
                strSQL = "Select bankid from bmap " +
                         " where entityid = '" + entityID.Trim() + "' and CashType = 'OP'";

                locTbl = sqlAgent.ExecuteQuery(strSQL);

                //checks that a Bank ID exists
                if (locTbl.Tables[0].Rows.Count > 0)
                {
                    bankId = locTbl.Tables[0].Rows[0]["bankid"].ToString().Trim();
                }
                else
                {
                    check.SetErrorMessage(string.Format("The bank id is not found for entity id {0}.", entityID.Trim()));
                }

                //gets the last check number for the bank id
                strSQL = "Select max(cast(ltrim(CheckNo) as int)) as Checkno from SCHK where BankId = '" + bankId + "' and " +
                         " CHECKNO not like '%a%' and " +
                         " CHECKNO not like '%b%' and " +
                         " CHECKNO not like '%c%' and " +
                         " CHECKNO not like '%d%' and " +
                         " CHECKNO not like '%e%' and " +
                         " CHECKNO not like '%f%' and " +
                         " CHECKNO not like '%g%' and " +
                         " CHECKNO not like '%h%' and " +
                         " CHECKNO not like '%i%' and " +
                         " CHECKNO not like '%j%' and " +
                         " CHECKNO not like '%k%' and " +
                         " CHECKNO not like '%l%' and " +
                         " CHECKNO not like '%m%' and " +
                         " CHECKNO not like '%n%' and " +
                         " CHECKNO not like '%o%' and " +
                         " CHECKNO not like '%p%' and " +
                         " CHECKNO not like '%q%' and " +
                         " CHECKNO not like '%r%' and " +
                         " CHECKNO not like '%s%' and " +
                         " CHECKNO not like '%t%' and " +
                         " CHECKNO not like '%u%' and " +
                         " CHECKNO not like '%v%' and " +
                         " CHECKNO not like '%w%' and " +
                         " CHECKNO not like '%x%' and " +
                         " CHECKNO not like '%y%' and " +
                         " CHECKNO not like '%z%' " +
                         " and try_convert(int, checkno) < 900000";

                locTbl = sqlAgent.ExecuteQuery(strSQL);

                //if the check is found
                if (locTbl.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        //add one to the check number
                        achNo = int.Parse(locTbl.Tables[0].Rows[0]["CheckNo"].ToString().Trim()) + 1;
                    }//if there is an error
                    catch (Exception)
                    {
                        //set the check to 0
                        achNo = 0;
                    }
                }//if the check is not found
                else
                {
                    //set the number to 0
                    achNo = 0;
                }

                //if the check number is less than 800000
                if (achNo < 800000)
                {
                    //set the check to 800000
                    achNo = 800000;
                }

                //set the last check was not found from existing uncommited checks
                done = false;

                //loop through the existing list of uncommitted checks
                do
                {
                    //set the number of the possible last check
                    lastCheck = achNo.ToString().PadLeft(9, ' ');
                    //see if the check is in uncommitted
                    done = !(checks.Where(x => x.BankID == bankId.Trim() && x.CheckNo.Trim() == lastCheck.Trim()).ToList<Check>().Count > 0);
                    //if not found
                    if (!done)
                    {
                        //check the next number
                        achNo++;
                    }

                } while (!done);

                if (check.IsErr)
                {
                    throw new ApplicationException(check.ErrorMessage.Trim());
                }
                else
                {
                    //create the check and the information
                    check.BankID = bankId.Trim();
                    check.CheckNo = achNo.ToString().Trim();
                    check.EntityID = entityID;
                    check.VendID = invoice.VendId.Trim();
                    check.CheckDt = invoice.InvcDate;
                    check.CheckNet = total;
                    check.CkStatus = "O";
                    check.DStatChg = DateTime.Now;
                    check.VendName1 = "ACH Conservice " + check.EntityID.Trim();
                    check.UserID = Program.userID.Trim();
                    check.CheckPd = invoice.InvcDate.ToString("yyyyMM");

                    List<SqlParameter> sqlParameters = new List<SqlParameter>();

                    //add the check
                    strSQL = "INSERT INTO SCHK(BANKID, CHECKNO, VENDID, CHECKNET, CKSTATUS, USERID, VENDNME1, " +
                             " ENTITYID, CHECKDT, CHECKPD, LASTDATE) " +
                             " VALUES(@BANKID, @CHECKNO, @VENDID, @CHECKNET, @CKSTATUS, @USERID, @VENDNME1, " +
                             " @ENTITYID, @CHECKDT, @CHECKPD, @LASTDATE)";

                    sqlParameters.Add(new SqlParameter("@BANKID", check.BankID.Trim()));
                    sqlParameters.Add(new SqlParameter("@CHECKNO", check.CheckNo.Trim().PadLeft(9, ' ')));
                    sqlParameters.Add(new SqlParameter("@VENDID", check.VendID.Trim()));
                    sqlParameters.Add(new SqlParameter("@CHECKDT", check.CheckDt.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    sqlParameters.Add(new SqlParameter("@CHECKNET", check.CheckNet));
                    sqlParameters.Add(new SqlParameter("@CKSTATUS", check.CkStatus.Trim()));
                    sqlParameters.Add(new SqlParameter("@USERID", check.UserID.Trim()));
                    sqlParameters.Add(new SqlParameter("@VENDNME1", check.VendName1.Trim()));
                    sqlParameters.Add(new SqlParameter("@ENTITYID", check.EntityID.Trim()));
                    sqlParameters.Add(new SqlParameter("@CHECKPD", check.CheckPd));
                    sqlParameters.Add(new SqlParameter("@LASTDATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")));

                    sqlAgent.ExecuteNonQuery(strSQL, sqlParameters);
                    //add checks to the list of checks that are added
                    checks.Add(check);
                    sqlParameters.Clear();

                    //goes though each invoice using a counter
                    for (int j = start; j <= i - 1; j++)
                    {
                        //get the invoice at that index
                        Invoice invObj = invoices[j];
                        //insert the the invoice
                        strSQL = "INSERT INTO INVC(VENDID, INVOICE, EXPPED, INVCDATE, DUEDATE, INVCAMT, PAIDAMT, LASTDATE, USERID, PARTIALPRG, ATAXAMT, INVCTOT, SITEID," +
                                 " INCLATAXAMT, JOBCOST, CROSSSITE, DESCRPTN, OUTSIDEBUD, APINVCTYPEID, INTFMARKER) " +
                                 " VALUES(@VENDID, @INVOICE, @EXPPED, @INVCDATE, @DUEDATE, " +
                                 " @INVCAMT, @PAIDAMT, @LASTDATE, @USERID, @PARTIALPRG, @ATAXAMT, @INVCTOT, @SITEID," +
                                 " @INCLATAXAMT, @JOBCOST, @CROSSSITE, @DESCRPTN, @OUTSIDEBUD, @APINVCTYPEID, @INTFMARKER)";

                        sqlParameters.Add(new SqlParameter("@VENDID", invObj.VendId.Trim()));
                        sqlParameters.Add(new SqlParameter("@INVOICE", invObj.InvoiceNumber.Trim()));
                        sqlParameters.Add(new SqlParameter("@EXPPED", invObj.ExpPed.Trim()));
                        sqlParameters.Add(new SqlParameter("@INVCDATE", invObj.InvcDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                        sqlParameters.Add(new SqlParameter("@DUEDATE", invObj.DueDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                        sqlParameters.Add(new SqlParameter("@INVCAMT", invObj.InvcAmt));
                        sqlParameters.Add(new SqlParameter("@PAIDAMT", invObj.PaidAmt));
                        sqlParameters.Add(new SqlParameter("@LASTDATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                        sqlParameters.Add(new SqlParameter("@USERID", System.Security.Principal.WindowsIdentity.GetCurrent().Name.Trim()));
                        sqlParameters.Add(new SqlParameter("@PARTIALPRG", "N"));
                        sqlParameters.Add(new SqlParameter("@ATAXAMT", "0.00"));
                        sqlParameters.Add(new SqlParameter("@INVCTOT", invObj.InvcTot));
                        sqlParameters.Add(new SqlParameter("@SITEID", "@"));
                        sqlParameters.Add(new SqlParameter("@INCLATAXAMT", "0.00"));
                        sqlParameters.Add(new SqlParameter("@JOBCOST", "N"));
                        sqlParameters.Add(new SqlParameter("@CROSSSITE", "N"));
                        sqlParameters.Add(new SqlParameter("@DESCRPTN", invObj.Descrptn.Trim()));
                        sqlParameters.Add(new SqlParameter("@OUTSIDEBUD", "N"));
                        sqlParameters.Add(new SqlParameter("@APINVCTYPEID", "INV"));
                        sqlParameters.Add(new SqlParameter("@INTFMARKER", "Conservice ACH"));

                        try
                        {
                            sqlAgent.ExecuteNonQuery(strSQL, sqlParameters);
                        }
                        catch(SqlException err)
                        {
                            if (err.Number == 2601)
                            {
                                throw new Exception($"An invoice has already been added for this period {invObj.ExpPed}. If this is a catch up, please addad 'C' to the 'U' column. Then add 'CATCHUP' to the top row.");
                            }
                        }
                        sqlParameters.Clear();
                        //set the counter to position 1
                        int h = 1;
                        //go through each history item for the invoice
                        foreach (History histObj in invObj.Hist)
                        {
                            //set the check number, bank id, and item number
                            histObj.CheckNo = check.CheckNo;
                            histObj.BankId = bankId.Trim();
                            histObj.Item = h.ToString().Trim();
                            //insert the history item
                            strSQL = "INSERT INTO HIST(VENDID, INVOICE, EXPPED, ITEM, REF, ENTITYID, ACCTNUM, ITEMAMT, DISCAMT, STATUS, BALFLAG, CHECKNO, CHECKDT, CHECKPD," +
                                     " DISCTKN, CASHTYPE, ATAXAMT, DEPARTMENT, TAXITEM, BANKID, USERID, ECITEMAMT, ECDISCAMT," +
                                     " ECATAXAMT, PCHECKAMT, XITEMAMT, XDISCAMT, ORIGITEMAMT, DISCMOD, SUBWITH, POQTY, JOBCOST, CROSSSITE, AUTAXPAID, ADDLDESC) " +
                                     " VALUES(@VENDID, @INVOICE, @EXPPED, @ITEM, @REF, @ENTITYID, @ACCTNUM, @ITEMAMT, @DISCAMT, @STATUS, @BALFLAG, @CHECKNO, @CHECKDT, @CHECKPD," +
                                     " @DISCTKN, @CASHTYPE, @ATAXAMT, @DEPARTMENT, @TAXITEM, @BANKID, @USERID, @ECITEMAMT, @ECDISCAMT," +
                                     " @ECATAXAMT, @PCHECKAMT, @XITEMAMT, @XDISCAMT, @ORIGITEMAMT, @DISCMOD, @SUBWITH, @POQTY, @JOBCOST, @CROSSSITE, @AUTAXPAID, @ADDLDESC)";

                            sqlParameters.Add(new SqlParameter("@VENDID", histObj.VendId.Trim()));
                            sqlParameters.Add(new SqlParameter("@INVOICE", invObj.InvoiceNumber.Trim()));
                            sqlParameters.Add(new SqlParameter("@EXPPED", invObj.ExpPed.Trim()));
                            sqlParameters.Add(new SqlParameter("@ITEM", histObj.Item.ToString().Trim()));
                            sqlParameters.Add(new SqlParameter("@REF", histObj.Ref.Trim()));
                            sqlParameters.Add(new SqlParameter("@ACCTNUM", histObj.AcctNum.Trim()));
                            sqlParameters.Add(new SqlParameter("@ENTITYID", histObj.EntityID.Trim()));
                            sqlParameters.Add(new SqlParameter("@ITEMAMT", histObj.ItemAmt));
                            sqlParameters.Add(new SqlParameter("@DISCAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@STATUS", "R"));
                            sqlParameters.Add(new SqlParameter("@BALFLAG", "N"));
                            sqlParameters.Add(new SqlParameter("@CHECKNO", histObj.CheckNo.PadLeft(9, ' ')));
                            sqlParameters.Add(new SqlParameter("@CHECKDT", histObj.CheckDt.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                            sqlParameters.Add(new SqlParameter("@CHECKPD", histObj.CheckPd));
                            sqlParameters.Add(new SqlParameter("@DISCTKN", "0"));
                            sqlParameters.Add(new SqlParameter("@ATAXAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@CASHTYPE", "OP"));
                            sqlParameters.Add(new SqlParameter("@DEPARTMENT", histObj.Department.Trim()));
                            sqlParameters.Add(new SqlParameter("@TAXITEM", "N"));
                            sqlParameters.Add(new SqlParameter("@BANKID", histObj.BankId.Trim()));
                            sqlParameters.Add(new SqlParameter("@USERID", System.Security.Principal.WindowsIdentity.GetCurrent().Name));
                            sqlParameters.Add(new SqlParameter("@ECITEMAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@ECDISCAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@ECATAXAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@PCHECKAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@XITEMAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@XDISCAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@ORIGITEMAMT", "0.00"));
                            sqlParameters.Add(new SqlParameter("@DISCMOD", "N"));
                            sqlParameters.Add(new SqlParameter("@SUBWITH", "N"));
                            sqlParameters.Add(new SqlParameter("@POQTY", "0.00"));
                            sqlParameters.Add(new SqlParameter("@JOBCOST", "N"));
                            sqlParameters.Add(new SqlParameter("@CROSSSITE", "N"));
                            sqlParameters.Add(new SqlParameter("@AUTAXPAID", "N"));
                            sqlParameters.Add(new SqlParameter("@ADDLDESC", invObj.InvoiceNumber.Trim()));

                            sqlAgent.ExecuteNonQuery(strSQL, sqlParameters);
                            sqlParameters.Clear();
                            //insert the history into the text
                            WriteHisttoText(file, histObj, invoice);
                            h++;
                        }
                    }
                }
            }
            catch(SqlException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ApplicationException(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());
            }
        }

        /// <summary>
        /// Insert the <see cref="History"/> data to the history text file. It inserts each <see cref="History"/> item on the next line in the text file. 
        /// </summary>
        /// <param name="output">The text file.</param>
        /// <param name="hist">The history item.</param>
        /// <param name="inv">The invoice.</param>
        private void WriteHisttoText(string output, History hist, Invoice inv)
        {
            try
            {
                StreamWriter stream = File.AppendText(output.Trim());
                stream.Write("\"" + hist.ExpPed + "\"");
                stream.Write(", " + inv.VendId);
                stream.Write(", " + inv.InvoiceNumber);
                stream.Write(", " + hist.CheckNo);
                stream.Write(", " + hist.BankId);
                stream.Write(", \"" + inv.SiteId + "\"");
                stream.Write(", \"" + hist.Item + "\"");
                stream.Write(", " + hist.EntityID);
                stream.Write(", \"" + hist.AcctNum + "\"");
                stream.Write(", \"" + hist.Department + "\"");
                stream.Write(", \"" + hist.JobCode + "\"");
                stream.Write(", " + hist.ItemAmt.ToString("n2").Replace(",", ""));
                stream.Write(", " + hist.CheckDt.ToString("MM/dd/yyyy"));
                stream.Write(", \"" + hist.Ref + "\"");
                stream.Write(", \"" + hist.AddlDesc + "\"");
                stream.WriteLine("");
                stream.Close();
            }
            catch (NotSupportedException err)
            {
                throw err;
            }
            catch (DirectoryNotFoundException err)
            {
                throw err;
            }
            catch (PathTooLongException err)
            {
                throw err;
            }
            catch (ArgumentNullException err)
            {
                throw err;
            }
            catch (ArgumentException err)
            {
                throw err;
            }
            catch (UnauthorizedAccessException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Gets the data from the .CSV file. If formatted correctly, each line is retrieved as an <see cref="Invoice"/>. 
        /// </summary>
        /// <param name="fileData">The collection of the columns for a given line, in the .CSV file.</param>
        private void ParseInvoice(string[] fileData)
        {
            try
            {
                string synInvoice = "";
                int position = 0;
                bool found = false;
                string entityID = "";
                int histCount = 1;
                int division = 0;
                int i = 1;
                string vendId = "";
                string invoice = "";
                DataSet locTbl = new DataSet();
                History history = new History();
                Invoice inv = new Invoice();
                DateTime checkDate = DateTime.MinValue;
                try
                {
                    checkDate = DateTime.Parse(fileData[11]);
                    Console.WriteLine(fileData[11]);
                }
                catch (Exception)
                {
                    inv.SetErrorMessage(string.Format("Unable to parse {0} as the check date for invoice {1}.", fileData[11], synInvoice));
                    invoices.Add(inv);
                    return;
                }

                entityID = fileData[5].PadLeft(3, '0');
                synInvoice = string.Format("ACH-{0}-{1}", checkDate.ToString("yyMMdd"), entityID);

                if(fileData.Length > 20)
                {
                    if(fileData[20] != null)
                    {
                        if(fileData[20].ToString().Trim().ToUpper() == "C")
                        {
                            synInvoice += "C";
                        }
                    }
                }

                vendId = fileData[10].ToString().Trim();
                //find if an invoice exists
                found = invoices.Exists(x => x.VendId.Trim() == conserviceVendId.Trim() & x.InvoiceNumber.Trim() == synInvoice.Trim());
                //if found
                if (found)
                {
                    //set the existing invoice
                    i = invoices.FindIndex(x => x.VendId.Trim() == conserviceVendId.Trim() & x.InvoiceNumber.Trim() == synInvoice.Trim());
                    inv = invoices[i];
                    histCount = inv.Hist.Count;
                }

                string invoiceValue = fileData[17].ToString().Trim();
                position = invoiceValue.IndexOf('/');

                if (invoiceValue.Contains('/'))
                {
                    invoice = invoiceValue.Substring(0, position - 4);
                }
                else
                {
                    invoice = invoiceValue.ToString().Trim();
                }
                //if the invoice is not found
                if (!found)
                {
                    //set up a new invoice
                    inv = new Invoice();
                    try
                    {
                        checkDate = DateTime.Parse(fileData[11]);
                    }
                    catch (Exception)
                    {
                        inv.SetErrorMessage(string.Format("Unable to parse {0} as the check date for invoice {1}.", fileData[11], synInvoice));
                    }

                    inv.ExpPed = fileData[0];
                    inv.SiteId = fileData[3];
                    inv.VendId = conserviceVendId;
                    inv.InvcDate = checkDate;
                    inv.DueDate = inv.InvcDate;
                    inv.InvoiceNumber = synInvoice;
                    inv.Descrptn = fileData[10];
                    inv.InterfaceID = GetNextInterfaceID(intFMarker);
                    inv.IntfMaker = intFMarker;
                    inv.Userid = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                }
                
                history.EntityID = fileData[5].PadLeft(3, '0');
                history.AcctNum = fileData[6];

                if (history.EntityID.Trim() == "WCC" | history.EntityID.Trim() == "WC")
                {
                    if(history.AcctNum.Trim().Length >= 2)
                    {
                        if(history.AcctNum.Substring(0, 2).Trim() == "WC")
                        {

                        }
                        else
                        {
                            //set the error
                            inv.SetErrorMessage(string.Format("The invoice {0} has account {1} which invalid for entity WCC.",
                                                               inv.InvoiceNumber.Trim(), history.AcctNum.Trim()));
                        }
                    }
                    else
                    {
                        //set the error
                        inv.SetErrorMessage(string.Format("The invoice {0} has account {1} which invalid for entity WCC.", 
                                                           inv.InvoiceNumber.Trim(), history.AcctNum.Trim()));
                    }
                }
                else
                {
                    if (history.AcctNum.Trim().Length >= 2)
                    {
                        if (history.AcctNum.Substring(0, 2).Trim() == "HS")
                        {

                        }
                        else
                        {
                            //set the error
                            inv.SetErrorMessage(string.Format("The invoice {0} has account {1} which invalid for entity {2}.",
                                                               inv.InvoiceNumber.Trim(), history.AcctNum.Trim(), history.EntityID.Trim()));
                        }
                    }
                    else
                    {
                        //set the error
                        inv.SetErrorMessage(string.Format("The invoice {0} has account {1} which invalid for entity {2}.",
                                                           inv.InvoiceNumber.Trim(), history.AcctNum.Trim(), history.EntityID.Trim()));
                    }
                }

                try
                {
                    division = int.Parse(fileData[7]);
                    history.Department = division.ToString().PadLeft(2, '0');
                }
                catch (Exception)
                {
                    history.Department = fileData[7];
                    division = 0;
                }
                //lookup the invoice 
                strSQL = "SELECT * FROM INVC " +
                         " WHERE VENDID = '" + inv.InvoiceNumber.Trim() + "' AND EXPPED = '" + inv.ExpPed.Trim() + "' AND INVOICE = '" + inv.InvoiceNumber.Trim() + "'";

                locTbl = sqlAgent.ExecuteQuery(strSQL);
                //if the invoice exists
                if (locTbl.Tables[0].Rows.Count > 0)
                {
                    //set the error
                    inv.SetErrorMessage(string.Format("The invoice {0} has already been added.", inv.InvoiceNumber));
                }
                //if the entity is 30 and not division 3 and 4 or is entity 322 and not division 35 or is division 34
                if (((history.EntityID == "030") & (division != 3) & (division != 4)) | ((history.EntityID == "322") & !((division == 35) | (division == 34))))
                {
                    //set the error
                    inv.SetErrorMessage(string.Format("Invalid Division for {0} - {1}.", history.EntityID.Trim(), history.Department.Trim()));
                }//if the division is not 0 and entity is not 30 and entity is not 322
                else if ((division != 0) & (history.EntityID != "030") & (history.EntityID != "322"))
                {
                    //set the error
                    inv.SetErrorMessage(string.Format("Invalid Division for {0} - {1}.", history.EntityID.Trim(), history.Department.Trim()));
                }//if division is 0 and department is not '@'
                else if ((division == 0) & (history.Department != "@"))
                {
                    //set the error
                    inv.SetErrorMessage(string.Format("Invalid Division for {0}.", history.EntityID.Trim()));
                }
                else if (!lstEntities.Contains(history.EntityID))
                {
                    inv.SetErrorMessage(string.Format("Invalid Entity {0} on for line item {1}.", history.EntityID.Trim(), fileData[4].Trim()));
                }

                history.Ref = fileData[10];
                history.AddlDesc = invoice;

                try
                {
                    history.ItemAmt = decimal.Parse(fileData[9]);
                }
                catch (Exception)
                {
                    inv.SetErrorMessage(string.Format("Failed to convert amount on {0}.", history.AcctNum));
                }
                inv.InvcAmt = inv.InvcAmt + history.ItemAmt;
                inv.PaidAmt = inv.InvcAmt;
                inv.InvcTot = inv.InvcAmt;
                inv.Session = "";

                history.BatchId = "";
                history.CheckDt = checkDate;
                history.ExpPed = inv.ExpPed;
                history.CheckPd = inv.ExpPed;
                history.Item = histCount.ToString().Trim();
                history.VendId = conserviceVendId;

                inv.Hist.Add(history);
                //if the invoice does not exist
                if (!found)
                {
                    //add the invoice
                    invoices.Add(inv);
                }
                else
                {
                    //set the invoice
                    invoices[i] = inv;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Adds the checks based on the collection of the <see cref="Invoice"/>.
        /// </summary>
        /// <param name="invoices">The collection of invoices.</param>
        /// <param name="file"></param>
        private void ProcessChecks(List<Invoice> invoices, string file)
        {
            try
            {
                int i = 0;
                int start = 0;
                string lastEntity = "";
                string lastVend = "";
                Invoice lastInvoice = new Invoice();
                decimal total = 0M;

                

                //goes through each invoice
                foreach (Invoice invoice in invoices)
                {//if the it is not position one and the entity id or vendor id is different from the last invoice
                    if ((i != 0) & ((invoice.Hist[0].EntityID != lastEntity) | (invoice.VendId != lastVend)))
                    {//add the check
                        CreateCheck(total, lastInvoice, lastEntity, file, start, i);
                        total = 0M;
                        start = i;
                    }//total the invoices
                    total += invoice.InvcAmt;
                    lastEntity = invoice.Hist[0].EntityID.Trim();
                    lastVend = invoice.VendId;
                    lastInvoice = invoice;
                    i++;
                }
                CreateCheck(total, lastInvoice, lastInvoice.Hist[0].EntityID, file, start, i);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Makes sure the invoice data is formatted correctly.
        /// </summary>
        /// <param name="fileHeaderData">The .CSV file header row.</param>
        /// <returns>Return</returns>
        private bool IsValid(string fileHeaderData)
        {
            string[] headerRow = fileHeaderData.Split(',');

            if (fileHeaderData.Last() == ',')
            {
                fileHeaderData = fileHeaderData.Remove(fileHeaderData.Length - 1, 1);
            }

            fileHeaderData = fileHeaderData.Replace(" ", "");

            //if the header row has more than 21 columns
            if (headerRow.Length > 21)
            {//display error message and return false
                lstErrorMessages.Items.Add("There are too many columns in this document.");
                return false;
            }
            //if the header row does not have all these columns
            if (fileHeaderData.Trim() != "PERIOD,REF,SOURCE,SITEID,ITEM,ENTITYID,ACCTNUM,DEPARTMENT,JOBCODE,AMT,DESCRPN,ENTRDATE,REVERSAL,STATUS,OEXCHGREF,BASIS,LASTDATE,INVNUM,ADDLDESC,EXPDATE")
            {//display error message and return false
             //if the header row does not have all these columns
                if (fileHeaderData.Trim() != "PERIOD,REF,SOURCE,SITEID,ITEM,ENTITYID,ACCTNUM,DEPARTMENT,JOBCODE,AMT,DESCRPN,ENTRDATE,REVERSAL,STATUS,OEXCHGREF,BASIS,LASTDATE,INVNUM,ADDLDESC,EXPDATE,CATCHUP")
                {//display error message and return false
                    lstErrorMessages.Items.Add("The columns are either not correct, missing, or not in the correct format.\n" +
                                               "Please recheck the file from conservice and make sure the columns are as follows:\n \n" +
                                               "PERIOD,REF,SOURCE,SITEID,ITEM,ENTITYID,ACCTNUM,DEPARTMENT,JOBCODE,AMT,DESCRPN,ENTRDATE,REVERSAL,STATUS,OEXCHGREF,BASIS,LASTDATE,INVNUM,ADDLDESC,EXPDATE \n \n OR \n \n" +
                                               "PERIOD,REF,SOURCE,SITEID,ITEM,ENTITYID,ACCTNUM,DEPARTMENT,JOBCODE,AMT,DESCRPN,ENTRDATE,REVERSAL,STATUS,OEXCHGREF,BASIS,LASTDATE,INVNUM,ADDLDESC,EXPDATE,CATCHUP");
                    return false;
                }
            }
            //if there is nothing incorrect, return true
            return true;
        }

        /// <summary>
        /// Adds the header row to the .CSV file.
        /// </summary>
        /// <param name="output">The output file.</param>
        private void WriteHeaderRow(string output)
        {
            try
            {
                File.CreateText(output.Trim()).Close();
                StreamWriter stream = new StreamWriter(output.Trim());
                stream.WriteLine("PERIOD, VENDID, INVOICE, CHECKNO, BANKID, SITEID, ITEM, ENTITYID, ACCTNUM, DEPARTMENT, JOBCODE, ITEMAMT, CHECKDT, REF, ADDLDESC");
                stream.Close();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Gets the next interface ID.
        /// </summary>
        /// <param name="marker">The marker.</param>
        /// <returns>Returns the interface ID.</returns>
        private int GetNextInterfaceID(string marker)
        {
            try
            {
                DataSet locTbl = new DataSet();
                int id = 0;
                //lookup the existing interface id
                strSQL = "Select max(InterfaceId) as InterfaceId from INVC where IntfMarker = '" + marker.Trim() + "'";

                locTbl = sqlAgent.ExecuteQuery(strSQL);
                //if an interface id is found
                if (locTbl.Tables[0].Rows.Count > 0 && locTbl.Tables[0].Rows[0]["InterfaceId"].ToString().Trim() != "")
                {//add one to the interface id
                    id = int.Parse(locTbl.Tables[0].Rows[0]["InterfaceId"].ToString().Trim()) + 1;
                }
                else
                {//else get the total invoices and add one
                    id = invoices.Count + 1;
                }

                return id;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Gets the avaliable CSV files.
        /// </summary>
        /// <param name="directory">The directory the .CSV files are in.</param>
        /// <param name="year">The current year.</param>
        /// <param name="month">The current month.</param>
        private void GetCSVFiles(string directory)
        {
            try
            {//clear the the list of files found
                lstFilesToUpload.Items.Clear();

                string[] files;
                //look in directory for the folder 4-digit year and 2-digit month
                if (Directory.Exists(directory + @"\"))
                {
                    files = Directory.GetFiles(directory + @"\", "*.csv");

                    foreach (string file in files)
                    {
                        try
                        {
                            lstFilesToUpload.Items.Add(file);
                        }
                        catch (ArgumentNullException err)
                        {
                            lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                                "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                        }
                        catch (SystemException err)
                        {
                            lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                                "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                        }
                    }
                }
                //else
                //{//else look in directory for the folder 2-digit year and 2-digit month
                //    if (Directory.Exists(directory + year.ToString().Substring(2, 2) + "-" + month.ToString().PadLeft(2, '0') + @"\"))
                //    {
                //        files = Directory.GetFiles(directory + year.ToString().Substring(2, 2) + "-" + month.ToString().PadLeft(2, '0') + @"\", "*.csv");

                //        foreach (string file in files)
                //        {
                //            try
                //            {
                //                lstFilesToUpload.Items.Add(file);
                //            }
                //            catch (ArgumentNullException err)
                //            {
                //                lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                //                    "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                //            }
                //            catch (SystemException err)
                //            {
                //                lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                //                    "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                //            }
                //        }
                //    }
                //    else
                //    {//else look for folder
                //        if (Directory.Exists(directory + year + @"\"))
                //        {
                //            files = Directory.GetFiles(directory + year + @"\", "*.csv");

                //            foreach (string file in files)
                //            {
                //                try
                //                {
                //                    lstFilesToUpload.Items.Add(file);
                //                }
                //                catch (ArgumentNullException err)
                //                {
                //                    lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                //                        "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                //                }
                //                catch (SystemException err)
                //                {
                //                    lstErrorMessages.Items.Add(string.Format("File {0} was not able to added to the list. " +
                //                        "Read the error below: \n" + err.Message.Trim() + "\n \n" + err.StackTrace.Trim(), file));
                //                }
                //            }
                //        }
                //        else
                //        {

                //        }
                //    }
                //}
            }
            catch (Exception err)
            {
                lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                bool isErr = false;
                string message = "";
                string historyFile = "";

                lstErrorMessages.Items.Clear();

                this.Cursor = Cursors.WaitCursor;
                //if a file is selected 
                if (lstFilesToUpload.SelectedItems.Count > -1)
                {//goes through each selected file
                    foreach (string l in lstFilesToUpload.SelectedItems)
                    {
                        try
                        {
                            sqlAgent.BeginTransaction();

                            checks.Clear();
                            invoices.Clear();

                            if (File.Exists(l.Trim()))
                            {
                                string plainText = "";
                                string[] fileData;

                                StreamReader file = new StreamReader(l.Trim());

                                isErr = !IsValid(file.ReadLine());

                                if (!isErr)
                                {
                                    while (!file.EndOfStream)
                                    {
                                        plainText = file.ReadLine();

                                        fileData = plainText.Split('\"');
                                        if (fileData.Length > 1)
                                        {
                                            plainText = fileData[0].ToString().Trim() + fileData[1].ToString().Replace(",", "").Trim() + fileData[2].ToString().Trim();
                                        }
                                        fileData = plainText.Replace("\'", "").Split(',');

                                        if (!fileData.ToList().TrueForAll(x => x.Trim() == ""))
                                        {
                                            ParseInvoice(fileData);
                                        }
                                    }
                                }

                                file.Close();
                            }

                            DateTime importDate = DateTime.MinValue;
                            decimal total = 0M;
                            DialogResult response;

                            if (!isErr)
                            {
                                isErr = invoices.Any(x => x.IsErr == true);
                                foreach (Invoice invoice in invoices)
                                {
                                    message += invoice.ErrorMessage.Trim() + "\n";
                                    total += invoice.InvcAmt;
                                    importDate = invoice.InvcDate;
                                }
                            }

                            if (!isErr)
                            {
                                response = MessageBox.Show(string.Format("Total import for {0} is {1} - Ok to process?", importDate.ToString("MM/dd/yyyy"), total.ToString("c2")),
                                "Attention!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (response == DialogResult.Yes)
                                {
                                    historyFile = Path.GetDirectoryName(l.Trim()) + @"\processed\" + Path.GetFileName(l.Trim());

                                    if (!File.Exists(Path.GetDirectoryName(l.Trim()) + @"\processed\"))
                                    {
                                        try
                                        {
                                            Directory.CreateDirectory(Path.GetDirectoryName(l.Trim()) + @"\processed\");
                                        }
                                        catch (Exception err)
                                        {
                                            MessageBox.Show(string.Format("The directory 'processed' was not created and it could not be automatically created. " +
                                                            "Please make sure you have a processed folder and resubmit the conservice file {0}.", l.Trim()));
                                            lstErrorMessages.Items.Add(string.Format("The directory 'processed' was not created and it could not be automatically created. " +
                                                                       "Please make sure you have a processed folder and resubmit the conservice file {0}.", l.Trim()));
                                            goto skip_writing_conservice;
                                        }
                                    }

                                    WriteHeaderRow(historyFile.Trim());

                                    ProcessChecks(invoices, historyFile.Trim());

                                    File.Move(l.Trim(), l.Trim() + ".his");

                                    MessageBox.Show("The file(s) were imported successfully.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                skip_writing_conservice:;
                                }

                                sqlAgent.CommitTransaction();
                            }
                            else
                            {
                                string errorMessage = "";
                                //gets the errors already found
                                foreach(object item in lstErrorMessages.Items)
                                {
                                    errorMessage += item.ToString();
                                }
                                //display the additional errors
                                foreach (Invoice invoice in invoices.Where(x => x.IsErr == true).ToList<Invoice>())
                                {
                                    errorMessage += invoice.ErrorMessage.Trim();
                                    lstErrorMessages.Items.Add(invoice.ErrorMessage.Trim());
                                }
                                throw new Exception(errorMessage);
                            }
                        }
                        catch (SqlException err)
                        {
                            this.Cursor = Cursors.Default;

                            lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());

                            invoices.Clear();
                            checks.Clear();

                            sqlAgent.RollbackTransaction();

                            List<MailAddress> mailAddresses = new List<MailAddress>();
                            mailAddresses.Add(new MailAddress("helpdesk@hsales.freshservice.com"));

                            //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                            //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                            //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                            //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                            //                                                    err.GetType().Name.Trim(), false, "", null, false);

                            HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);

                            switch (err.Number) {
                                case 2627:
                                    MessageBox.Show("The data was already imported.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                default:
                                    MessageBox.Show(err.Message.Trim(), "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                            }

                        }
                        catch (Exception err)
                        {
                            this.Cursor = Cursors.Default;

                            lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());

                            invoices.Clear();
                            checks.Clear();

                            sqlAgent.RollbackTransaction();

                            List<MailAddress> mailAddresses = new List<MailAddress>();
                            mailAddresses.Add(new MailAddress("helpdesk@hsales.freshservice.com"));

                            //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                            //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                            //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                            //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                            //                                                    err.GetType().Name.Trim(), false, "", null, false);

                            HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);

                            MessageBox.Show(err.Message.Trim(), "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    GetCSVFiles(directory);

                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception err)
            {
                this.Cursor = Cursors.Default;

                lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());

                invoices.Clear();
                checks.Clear();
            }
        }

        private void lstFilesToUpload_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstFilesToUpload.SelectedItems.Count > 0)
                {
                    btnUpload.Enabled = true;
                }
                else
                {
                    btnUpload.Enabled = false;
                }
            }
            catch (Exception err)
            {
                lstErrorMessages.Items.Add(err.Message.Trim() + "\n \n" + err.StackTrace.Trim());
            }
        }

        private void btnExportErrors_Click(object sender, EventArgs e)
        {
            List<string> exportErrors = new List<string>();
            foreach (string error in lstErrorMessages.Items)
            {
                exportErrors.Add(error.Trim());
            }

            Program.WriteErrorLog(exportErrors.ToArray(), "HomeSales Conservice");
        }

        private void btnListImports_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            DialogResult dialogResult;

            openFileDialog.SelectedPath = directory.Trim();

            dialogResult = openFileDialog.ShowDialog();
            
            if(dialogResult == DialogResult.OK)
            {
                directory = openFileDialog.SelectedPath.Trim();
                Properties.Settings.Default["directoryConservice"] = openFileDialog.SelectedPath.Trim();
                Properties.Settings.Default.Save();
                txtPath.Text = openFileDialog.SelectedPath.Trim();

                GetCSVFiles(directory);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
