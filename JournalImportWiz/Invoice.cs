﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImport
{
    class Invoice
    {
        private string _vendId = "";
        private string _invoice = "";
        private string _expPed = "";
        private DateTime _invcDate;
        private DateTime _dueDate = DateTime.MinValue;
        private decimal _invcAmt = 0M;
        private decimal _paidAmt = 0M;
        private string _sepCheck = "";
        private string _poNum = "";
        private DateTime _lastDate = DateTime.MinValue;
        private string _session = "";
        private string _userid = "";
        private string _partialPrg = "";
        private decimal _aTaxAmt = 0M;
        private decimal _invcTot = 0M;
        private string _status = "";
        private DateTime _authDate = DateTime.MinValue;
        private string _authLoc = "";
        private string _authId = "";
        private string _eCurCode = "";
        private string _planId = "";
        private string _siteId = "";
        private string _proType = "";
        private DateTime _pBegDate = DateTime.MinValue;
        private DateTime _pEndDate = DateTime.MinValue;
        private decimal _inClaTaxAmt = 0M;
        private string _jobCost = "";
        private string _crossSite = "";
        private string _alloCode = "";
        private string _vendId2nd = "";
        private string _descrptn = "";
        private string _outSideBud = "";
        private DateTime _taxPtDateCalc = DateTime.MinValue;
        private string _expCtrlFlag = "";
        private string _apiNvcTypeID = "";
        private string _ownTbl = "";
        private string _ownYear = "";
        private double _ownNum = 0D;
        private string _ctrYTbl = "";
        private string _ctrYYear = "";
        private double _ctrYNum = 0D;
        private string _apImportNum = "";
        private DateTime _periodDate = DateTime.MinValue;
        private string _scannedCopyURL = "";
        private string _altAddRID = "";
        private double _interfaceID = 0D;
        private string _intfMaker = "";
        private string _checkPrintingPriorityID = "";
        private List<History> _hist = new List<History>();
        private bool _isErr = false;
        private string _errorMessage = "";

        public string VendId { get => _vendId; set => _vendId = value; }
        public string InvoiceNumber { get => _invoice; set => _invoice = value; }
        public string ExpPed { get => _expPed; set => _expPed = value; }
        public DateTime InvcDate { get => _invcDate; set => _invcDate = value; }
        public DateTime DueDate { get => _dueDate; set => _dueDate = value; }
        public decimal InvcAmt { get => _invcAmt; set => _invcAmt = value; }
        public decimal PaidAmt { get => _paidAmt; set => _paidAmt = value; }
        public string SepCheck { get => _sepCheck; set => _sepCheck = value; }
        public string PoNum { get => _poNum; set => _poNum = value; }
        public DateTime LastDate { get => _lastDate; set => _lastDate = value; }
        public string Session { get => _session; set => _session = value; }
        public string Userid { get => _userid; set => _userid = value; }
        public string PartialPrg { get => _partialPrg; set => _partialPrg = value; }
        public decimal ATaxAmt { get => _aTaxAmt; set => _aTaxAmt = value; }
        public decimal InvcTot { get => _invcTot; set => _invcTot = value; }
        public string Status { get => _status; set => _status = value; }
        public DateTime AuthDate { get => _authDate; set => _authDate = value; }
        public string AuthLoc { get => _authLoc; set => _authLoc = value; }
        public string AuthId { get => _authId; set => _authId = value; }
        public string ECurCode { get => _eCurCode; set => _eCurCode = value; }
        public string PlanId { get => _planId; set => _planId = value; }
        public string SiteId { get => _siteId; set => _siteId = value; }
        public string ProType { get => _proType; set => _proType = value; }
        public DateTime PBegDate { get => _pBegDate; set => _pBegDate = value; }
        public DateTime PEndDate { get => _pEndDate; set => _pEndDate = value; }
        public decimal InClaTaxAmt { get => _inClaTaxAmt; set => _inClaTaxAmt = value; }
        public string JobCost { get => _jobCost; set => _jobCost = value; }
        public string CrossSite { get => _crossSite; set => _crossSite = value; }
        public string AlloCode { get => _alloCode; set => _alloCode = value; }
        public string VendId2nd { get => _vendId2nd; set => _vendId2nd = value; }
        public string Descrptn { get => _descrptn; set => _descrptn = value; }
        public string OutSideBud { get => _outSideBud; set => _outSideBud = value; }
        public DateTime TaxPtDateCalc { get => _taxPtDateCalc; set => _taxPtDateCalc = value; }
        public string ExpCtrlFlag { get => _expCtrlFlag; set => _expCtrlFlag = value; }
        public string ApiNvcTypeID { get => _apiNvcTypeID; set => _apiNvcTypeID = value; }
        public string OwnTbl { get => _ownTbl; set => _ownTbl = value; }
        public string OwnYear { get => _ownYear; set => _ownYear = value; }
        public double OwnNum { get => _ownNum; set => _ownNum = value; }
        public string CtrYTbl { get => _ctrYTbl; set => _ctrYTbl = value; }
        public string CtrYYear { get => _ctrYYear; set => _ctrYYear = value; }
        public double CtrYNum { get => _ctrYNum; set => _ctrYNum = value; }
        public string ApImportNum { get => _apImportNum; set => _apImportNum = value; }
        public DateTime PeriodDate { get => _periodDate; set => _periodDate = value; }
        public string ScannedCopyURL { get => _scannedCopyURL; set => _scannedCopyURL = value; }
        public string AltAddRID { get => _altAddRID; set => _altAddRID = value; }
        public double InterfaceID { get => _interfaceID; set => _interfaceID = value; }
        public string IntfMaker { get => _intfMaker; set => _intfMaker = value; }
        public string CheckPrintingPriorityID { get => _checkPrintingPriorityID; set => _checkPrintingPriorityID = value; }
        public List<History> Hist { get => _hist; set => _hist = value; }
        public bool IsErr { get => _isErr; }
        public string ErrorMessage { get => _errorMessage; }

        public void SetErrorMessage(string message)
        {
            _isErr = true;
            _errorMessage += message.Trim() + "\n";
        }
        public void RemoveErrorMessage()
        {
            _isErr = false;
            _errorMessage = "";
        }

        public override bool Equals(object obj)
        {
            return obj is Invoice invoice &&
                   _vendId == invoice._vendId &&
                   _invoice == invoice._invoice &&
                   _expPed == invoice._expPed &&
                   _invcDate == invoice._invcDate &&
                   _dueDate == invoice._dueDate &&
                   _invcAmt == invoice._invcAmt &&
                   _paidAmt == invoice._paidAmt &&
                   _sepCheck == invoice._sepCheck &&
                   _poNum == invoice._poNum &&
                   _lastDate == invoice._lastDate &&
                   _session == invoice._session &&
                   _userid == invoice._userid &&
                   _partialPrg == invoice._partialPrg &&
                   _aTaxAmt == invoice._aTaxAmt &&
                   _invcTot == invoice._invcTot &&
                   _status == invoice._status &&
                   _authDate == invoice._authDate &&
                   _authLoc == invoice._authLoc &&
                   _authId == invoice._authId &&
                   _eCurCode == invoice._eCurCode &&
                   _planId == invoice._planId &&
                   _siteId == invoice._siteId &&
                   _proType == invoice._proType &&
                   _pBegDate == invoice._pBegDate &&
                   _pEndDate == invoice._pEndDate &&
                   _inClaTaxAmt == invoice._inClaTaxAmt &&
                   _jobCost == invoice._jobCost &&
                   _crossSite == invoice._crossSite &&
                   _alloCode == invoice._alloCode &&
                   _vendId2nd == invoice._vendId2nd &&
                   _descrptn == invoice._descrptn &&
                   _outSideBud == invoice._outSideBud &&
                   _taxPtDateCalc == invoice._taxPtDateCalc &&
                   _expCtrlFlag == invoice._expCtrlFlag &&
                   _apiNvcTypeID == invoice._apiNvcTypeID &&
                   _ownTbl == invoice._ownTbl &&
                   _ownYear == invoice._ownYear &&
                   _ownNum == invoice._ownNum &&
                   _ctrYTbl == invoice._ctrYTbl &&
                   _ctrYYear == invoice._ctrYYear &&
                   _ctrYNum == invoice._ctrYNum &&
                   _apImportNum == invoice._apImportNum &&
                   _periodDate == invoice._periodDate &&
                   _scannedCopyURL == invoice._scannedCopyURL &&
                   _altAddRID == invoice._altAddRID &&
                   _interfaceID == invoice._interfaceID &&
                   _intfMaker == invoice._intfMaker &&
                   _checkPrintingPriorityID == invoice._checkPrintingPriorityID &&
                   EqualityComparer<List<History>>.Default.Equals(_hist, invoice._hist) &&
                   _isErr == invoice._isErr &&
                   _errorMessage == invoice._errorMessage;
        }

        public override int GetHashCode()
        {
            int hashCode = 2123529186;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_vendId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_invoice);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_expPed);
            hashCode = hashCode * -1521134295 + _invcDate.GetHashCode();
            hashCode = hashCode * -1521134295 + _dueDate.GetHashCode();
            hashCode = hashCode * -1521134295 + _invcAmt.GetHashCode();
            hashCode = hashCode * -1521134295 + _paidAmt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_sepCheck);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_poNum);
            hashCode = hashCode * -1521134295 + _lastDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_session);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_userid);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_partialPrg);
            hashCode = hashCode * -1521134295 + _aTaxAmt.GetHashCode();
            hashCode = hashCode * -1521134295 + _invcTot.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_status);
            hashCode = hashCode * -1521134295 + _authDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_authLoc);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_authId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_eCurCode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_planId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_siteId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_proType);
            hashCode = hashCode * -1521134295 + _pBegDate.GetHashCode();
            hashCode = hashCode * -1521134295 + _pEndDate.GetHashCode();
            hashCode = hashCode * -1521134295 + _inClaTaxAmt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_jobCost);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_crossSite);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_alloCode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_vendId2nd);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_descrptn);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_outSideBud);
            hashCode = hashCode * -1521134295 + _taxPtDateCalc.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_expCtrlFlag);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_apiNvcTypeID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_ownTbl);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_ownYear);
            hashCode = hashCode * -1521134295 + _ownNum.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_ctrYTbl);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_ctrYYear);
            hashCode = hashCode * -1521134295 + _ctrYNum.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_apImportNum);
            hashCode = hashCode * -1521134295 + _periodDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_scannedCopyURL);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_altAddRID);
            hashCode = hashCode * -1521134295 + _interfaceID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_intfMaker);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_checkPrintingPriorityID);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<History>>.Default.GetHashCode(_hist);
            hashCode = hashCode * -1521134295 + _isErr.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_errorMessage);
            return hashCode;
        }
    }
}
