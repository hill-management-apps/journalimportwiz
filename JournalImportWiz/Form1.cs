﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Reflection;

namespace JournalImportWiz
{
    public partial class frmJournalImport : Form
    {
        List<IJournalWizardControl> ctrls = new List<IJournalWizardControl>();
        int currentControl = 0;
        PageInfo pi = new PageInfo();
        bool summaryFirst = false;

        public frmJournalImport()
        {
            InitializeComponent();
        }

        public frmJournalImport(bool SummaryFirst)
        {
            InitializeComponent();
            summaryFirst = SummaryFirst;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Fetch the database name from app.config.
            pi.DatabaseName = ConfigurationManager.AppSettings["dbName"];
            this.Text = string.Format("Journal Import = {0}", pi.DatabaseName);

            System.Reflection.Assembly prgAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fileVersionI = System.Diagnostics.FileVersionInfo.GetVersionInfo(prgAssembly.Location);
            string version = fileVersionI.FileVersion;

            this.Text = this.Text + " - V: " + version;

            // Fetch the userid from windows and put it in the PageInfo object
            string tmp = string.Empty;
            tmp = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            pi.UserId = tmp.Substring(tmp.IndexOf("\\") + 1);

            // The pages of the wizard are user controls.  After each is instantiated,
            // put them in this collection in the proper display order.
            ctrls.Add(ucInfo1);
            ctrls.Add(ucExcel1);
            ctrls.Add(ucValidate1);

            ResetControls();
            SetCurrentControl();

            if (summaryFirst)
            {
                GotoSummary();
                Environment.Exit(0);
            }
        }


        private void ResetControls()
        {
            foreach (IJournalWizardControl uc in ctrls)
            {
                ((UserControl)uc).Dock = DockStyle.Fill;
                ((UserControl)uc).Visible = false;
            }
        }

        private void SetCurrentControl()
        {
            ResetControls();
            if (currentControl < ctrls.Count)
                ((UserControl)ctrls[currentControl]).Visible = true;
            else
                currentControl--;

            ctrls[currentControl].PerformWork(pi);
        }

        private void ucInfo1_NextButtonClick(int myIndex)
        {
            currentControl = 1;
            SetCurrentControl();
        }

        private void ucExcel1_NextButtonClick(int myIndex)
        {
            currentControl = 2;
            SetCurrentControl();
        }

        private void ucValidate1_DoneButtonClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ucValidate1_ReRunButtonClick(object sender, EventArgs e)
        {
            currentControl = 0;
            SetCurrentControl();
        }

        private void GotoSummary()
        {
            frmSummary f = new frmSummary(pi);
            f.ShowDialog();
        }

        private void ucInfo1_Load(object sender, EventArgs e)
        {

        }
    }
}
