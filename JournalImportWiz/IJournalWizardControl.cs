﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    interface IJournalWizardControl
    { 
        void PerformWork(PageInfo pi);
    }
}
