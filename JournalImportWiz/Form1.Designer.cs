﻿
namespace JournalImportWiz
{
    partial class frmJournalImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucValidate1 = new JournalImportWiz.ucValidate();
            this.ucExcel1 = new JournalImportWiz.ucExcel();
            this.ucInfo1 = new JournalImportWiz.ucInfo();
            this.SuspendLayout();
            // 
            // ucValidate1
            // 
            this.ucValidate1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ucValidate1.Location = new System.Drawing.Point(14, 686);
            this.ucValidate1.Name = "ucValidate1";
            this.ucValidate1.Size = new System.Drawing.Size(377, 313);
            this.ucValidate1.TabIndex = 2;
            this.ucValidate1.DoneButtonClick += new System.EventHandler(this.ucValidate1_DoneButtonClick);
            this.ucValidate1.ReRunButtonClick += new System.EventHandler(this.ucValidate1_ReRunButtonClick);
            // 
            // ucExcel1
            // 
            this.ucExcel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ucExcel1.Location = new System.Drawing.Point(14, 334);
            this.ucExcel1.Name = "ucExcel1";
            this.ucExcel1.Size = new System.Drawing.Size(377, 346);
            this.ucExcel1.TabIndex = 1;
            this.ucExcel1.NextButtonClick += new JournalImportWiz.ucExcel.NextButtonPressedHandler(this.ucExcel1_NextButtonClick);
            // 
            // ucInfo1
            // 
            this.ucInfo1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ucInfo1.Location = new System.Drawing.Point(12, 12);
            this.ucInfo1.Name = "ucInfo1";
            this.ucInfo1.Size = new System.Drawing.Size(598, 316);
            this.ucInfo1.TabIndex = 0;
            this.ucInfo1.NextButtonClick += new JournalImportWiz.ucInfo.NextButtonPressedHandler(this.ucInfo1_NextButtonClick);
            this.ucInfo1.Load += new System.EventHandler(this.ucInfo1_Load);
            // 
            // frmJournalImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 333);
            this.Controls.Add(this.ucValidate1);
            this.Controls.Add(this.ucExcel1);
            this.Controls.Add(this.ucInfo1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmJournalImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Journal Import";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ucInfo ucInfo1;
        private ucExcel ucExcel1;
        private ucValidate ucValidate1;
    }
}

