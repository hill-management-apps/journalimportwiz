﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImport
{
    class IncomeAnalysis
    {
        private string _siteID = "";
        private decimal _discounts = 0M;
        private decimal _grossPotentialPerfect = 0M;
        private decimal _effectiveVacancy = 0M;
        private decimal _rentalIncome = 0M;
        private decimal _moveInDiscount = 0M;
        private decimal _grossVacant = 0M;

        public IncomeAnalysis(string siteID)
        {
            _siteID = siteID;
        }

        public decimal Discounts { get => _discounts; set => _discounts = value; }
        public decimal GrossPotentialPerfect { get => _grossPotentialPerfect; set => _grossPotentialPerfect = value; }
        public decimal GrossVacant { get => _grossVacant; set => _grossVacant = value; }
        public decimal EffectiveVacancy { get => _effectiveVacancy; set => _effectiveVacancy = value; }
        public decimal RentalIncome { get => _rentalIncome; set => _rentalIncome = value; }
        public decimal MoveInDiscount { get => _moveInDiscount; set => _moveInDiscount = value; }
        public string SiteID { get => _siteID; set => _siteID = value; }
    }
}
