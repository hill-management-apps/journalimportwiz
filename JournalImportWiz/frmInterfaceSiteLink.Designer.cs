﻿
namespace JournalImport
{
    partial class frmInterfaceSiteLink
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgvGeneralJournalEntries = new System.Windows.Forms.DataGridView();
            this.btnPostToJournal = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.lstMessages = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGeneralJournalEntries)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "General Journal Entries:";
            // 
            // dgvGeneralJournalEntries
            // 
            this.dgvGeneralJournalEntries.AllowUserToAddRows = false;
            this.dgvGeneralJournalEntries.AllowUserToDeleteRows = false;
            this.dgvGeneralJournalEntries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGeneralJournalEntries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGeneralJournalEntries.Location = new System.Drawing.Point(15, 97);
            this.dgvGeneralJournalEntries.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvGeneralJournalEntries.Name = "dgvGeneralJournalEntries";
            this.dgvGeneralJournalEntries.ReadOnly = true;
            this.dgvGeneralJournalEntries.RowHeadersWidth = 51;
            this.dgvGeneralJournalEntries.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGeneralJournalEntries.Size = new System.Drawing.Size(1083, 656);
            this.dgvGeneralJournalEntries.TabIndex = 1;
            this.dgvGeneralJournalEntries.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvGeneralJournalEntries_CellFormatting);
            // 
            // btnPostToJournal
            // 
            this.btnPostToJournal.Enabled = false;
            this.btnPostToJournal.Location = new System.Drawing.Point(1003, 761);
            this.btnPostToJournal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPostToJournal.Name = "btnPostToJournal";
            this.btnPostToJournal.Size = new System.Drawing.Size(100, 28);
            this.btnPostToJournal.TabIndex = 16;
            this.btnPostToJournal.Text = "Post";
            this.btnPostToJournal.UseVisualStyleBackColor = true;
            this.btnPostToJournal.Click += new System.EventHandler(this.btnPostToJournal_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1621, 32);
            this.label3.TabIndex = 18;
            this.label3.Text = "SiteLink Interface";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(716, 50);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 29);
            this.label4.TabIndex = 19;
            this.label4.Text = "Period:";
            // 
            // cboPeriod
            // 
            this.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Location = new System.Drawing.Point(827, 47);
            this.cboPeriod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(160, 37);
            this.cboPeriod.TabIndex = 20;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(1612, 761);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(100, 28);
            this.btnExport.TabIndex = 22;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // lstMessages
            // 
            this.lstMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstMessages.FormattingEnabled = true;
            this.lstMessages.HorizontalScrollbar = true;
            this.lstMessages.ItemHeight = 16;
            this.lstMessages.Location = new System.Drawing.Point(1105, 92);
            this.lstMessages.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.Size = new System.Drawing.Size(605, 660);
            this.lstMessages.TabIndex = 21;
            // 
            // frmInterfaceSiteLink
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1728, 804);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lstMessages);
            this.Controls.Add(this.cboPeriod);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnPostToJournal);
            this.Controls.Add(this.dgvGeneralJournalEntries);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmInterfaceSiteLink";
            this.Text = "SiteLink Interface";
            this.Load += new System.EventHandler(this.frmInterfaceSiteLink_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGeneralJournalEntries)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvGeneralJournalEntries;
        private System.Windows.Forms.Button btnPostToJournal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboPeriod;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ListBox lstMessages;
    }
}