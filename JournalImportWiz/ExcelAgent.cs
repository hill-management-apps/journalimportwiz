﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ExcelDataReader;
using HelpfulClasses;

namespace JournalImportWiz
{
    class ExcelAgent
    {
        private bool IgnoreJobCost = ucInfo.OneSided;
        //private const int COL
        decimal runningDebitTotal = 0;
        decimal runningCreditTotal = 0;

        int rowCounter = 0;
        int firstrow = 0;

        string[] recurringJve = new string[100];
        List<string> TaxBasisJve = new List<string>();

        DataSet ds = null;
        DataTable dt = null;
        PageInfo pi;
        public ExcelAgent()
        {
            for (int i=0; i < 100; i++)
            {
                recurringJve[i] = (299 + i).ToString().Trim();
            }
        }

        public void OpenSpreadsheet(PageInfo _pi)
        {
            pi = _pi;

            SqlAgent sa = new SqlAgent(pi);
            DataTable locTblTaxBasis = sa.ExecuteQuery($@"EXEC	[dbo].[HS_GetTaxBasis]").Tables[0];

            foreach (DataRow row in locTblTaxBasis.Rows)
            {
                TaxBasisJve.Add(row["JVNO"].ToString().Trim());
            }

            using (var stream = File.Open(pi.ExcelFileName, FileMode.Open, FileAccess.Read))
            {
                IExcelDataReader reader;
                if (Path.GetExtension(pi.ExcelFileName).ToUpper() == ".XLS")
                {
                    reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);
                }
                else
                {
                    //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = false
                    }
                };

                ds = reader.AsDataSet(conf);
            }
        }
        public void UpdateSpreadsheet(PageInfo _pi)
        {
            pi = _pi;
            ExcelAgent ea = new ExcelAgent();
            using (var stream = File.Open(pi.ExcelFileName, FileMode.Open, FileAccess.ReadWrite))
            {
                IExcelDataReader reader;
                reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = false
                    }
                };

                ds = reader.AsDataSet(conf);
            }
        }
        public void PerformValidation()
        {
            pi.ValidationMessages.Clear();
            pi.JournalRows.Clear();
            pi.GrandTotalDebit = 0;
            pi.GrandTotalCredit = 0;
            pi.ReferenceNumber = "XXXxXX"; // CalculateRefNum();

            // There is a DataTable for every sheet in the Excel file.
            foreach (DataTable dt in ds.Tables)
            {
                try
                {
                    ValidateSingleSheet(dt);
                }
                catch (Exception ex)
                {
                    // When "Reference Number not a number" is encountered on a particular sheet, it throws this error.
                    // This allows me to skip that entire sheet using continue to skip to the next.
                    // Any other message should be considered a fatal error.
                    pi.ErrorHasOccurred = true;
                    if (ex.Message == "The Reference Number is not a number")
                        continue;
                    else
                        throw;
                }
            }

            //check that all sheets are the same Tax Basis or not
            /*         var obj = (from JournalEntry p in pi.JournalRows.GetItems()
                                where TaxBasisJve.Contains(p.jve)
                                select p).Count(); */
            if (pi.JournalRows.GetItems().Count > 0)
            {
                JournalEntry j1 = pi.JournalRows.GetItems().First();
                pi.IsTaxBasis = TaxBasisJve.Contains(j1.jve);
                foreach (JournalEntry j in pi.JournalRows.GetItems())
                {
                    //if a Journal Entry with JVNO 253 if found
                    if (TaxBasisJve.Contains(j.jve) != pi.IsTaxBasis)
                    {
                        //write an error stating that a sheet cannot be Tax and Non Tax
                        pi.ValidationMessages.Add(" ");
                        pi.ValidationMessages.Add("You cannot have a sheet that is Tax Basis and others that are not.");
                        pi.ErrorHasOccurred = true;
                        //leave the foreach
                        break;
                    }
                }//check if the specific recurring happened for the selected period
                foreach (var j in pi.JournalRows.GetItems().Select(x => new { x.jve, x.EntityID }).Distinct())
                {
                    if (recurringJve.Contains(j.jve))
                    {
                        SqlAgent sqlAgent = new SqlAgent(pi.DatabaseName);
                        DataSet locTbl = new DataSet();
                        
                        locTbl = sqlAgent.ExecuteQuery("SELECT COUNT(*) FROM JOURNAL WHERE HS_JVNO = '" + j.jve.Trim() + "' AND ENTITYID = '" + j.EntityID.Trim() + "' AND PERIOD = '" + pi.Period  + "'");

                        if (locTbl.Tables[0].Rows[0][0].ToString().Trim() != "0")
                        {
                            pi.ValidationMessages.Add(string.Format("The recurrings have a already been posted for this period and property {0} for {1}.", j.EntityID.Trim(), j.jve.Trim()));
                            pi.jve = "(NULL)";
                            pi.ErrorHasOccurred = true;
                        }
                    }
                }

                List<string> entities = (from JournalEntry x in pi.JournalRows.GetItems()
                                         group x by x.EntityID into z
                                         select z.Key).ToList<string>();

                if (entities.Contains("WCC") & entities.Count > 1)
                {
                    pi.ValidationMessages.Add(" ");
                    pi.ValidationMessages.Add("This spreadsheet contains line items with project 'WCC' and other projects.");
                    pi.ValidationMessages.Add("You cannot have both lines with 'WCC' and other that are from other projects.");
                    pi.ErrorHasOccurred = true;
                }

                // This validation ensures that the total CreditAmount across all sheets within the 
                // Excel file add up to the Expected Total Amount specified on the wizard.
                // Marty's Test Spreadsheet has an ovarall total of: 322288.09
                if (pi.ExpectedBatchTotal != Math.Round(pi.GrandTotalDebit, 5))
                {
                    pi.ValidationMessages.Add(" ");
                    pi.ValidationMessages.Add(string.Format("The expected total amount was {0:c} but the actual total amount was {1:c}  differnce of {2}", pi.ExpectedBatchTotal, pi.GrandTotalDebit,
                        pi.ExpectedBatchTotal - pi.GrandTotalDebit));
                    pi.ErrorHasOccurred = true;
                }
            }
        }

        private bool ValidateSingleSheet(DataTable dt)
        {
            try
            {
                // All sheets have the same format and are validated in the same way.
                pi.ValidationMessages.Add($"Starting Sheet {dt.TableName}");
                decimal debitTotal = 0;
                decimal detaildebitTotal = 0;
                decimal creditTotal = 0;
                Dictionary<string, decimal> journalsUnbalanced = new Dictionary<string, decimal>();
                Dictionary<string, decimal> journalTotals = new Dictionary<string, decimal>();
                ENTITYID proj;

                bool CheckJobCost = true;
                bool JobCostIsBlank = true;

                //            string refNo = string.Empty;
                string jveinput = string.Empty;
                DateTime period = DateTime.MinValue;
                rowCounter = 0;
                string dept = "@";

                if (dt.Columns.Count < 17)
                {
                    CheckJobCost = false;
                    pi.ValidationMessages.Add("Warning Job Cost columns missing - not validated.");
                }
                else
                {
                    CheckJobCost = true;
                }


                foreach (DataRow dr in dt.Rows)
                {
                    switch (rowCounter)
                    {
                        case 0:                 // check the date against the period?

                            if (dr.ItemArray.Count() <= 5 || dr[5].ToString().Length == 0)
                            {
                                pi.ValidationMessages.Add(string.Format("The period is missing from cell F1.  Skipping the entire sheet.", period.ToShortDateString()));
                                pi.ErrorHasOccurred = true;
                            }
                            else if (String.Equals(dr[5].ToString(), "SKIP", StringComparison.OrdinalIgnoreCase))
                            {
                                pi.ValidationMessages.Add(" - Date is SKIP - Skipping this sheet ");
                                return false;
                            }
                            else
                            {
                                try
                                {
                                    period = DateTime.Parse(dr[5].ToString());
                                    string monthWithZero = string.Format("{0}", period.Month).PadLeft(2, '0');
                                    if ((period.Year.ToString() != pi.Period.Substring(0, 4) || (monthWithZero != pi.Period.Substring(4, 2))) && (period.Year.ToString().Substring(2, 2) != pi.Period.Substring(0, 2) || (monthWithZero != pi.Period.Substring(2, 2))))
                                    {
                                        pi.ValidationMessages.Add(string.Format("The period on this sheet, {0}, does not match what was entered in the wizard.  Skipping the entire sheet.", period.ToShortDateString()));
                                        pi.ErrorHasOccurred = true;
                                    }
                                }
                                catch (Exception e)
                                {
                                    pi.ValidationMessages.Add($"Unable to parse sheet Date {dr[5].ToString()}-{e} ");
                                    pi.ErrorHasOccurred = true;
                                    return false;
                                }
                                // Ensure that the reversal flag is either Y or N
                                if (dr[8].ToString().ToLower() != "y" && dr[8].ToString().ToLower() != "n")
                                {
                                    pi.ValidationMessages.Add(string.Format("The Reversal Flag [{0}] in cell I1 is invalid.  It must be either Y or N.  Skipping the entire sheet.", dr[8].ToString()));
                                    pi.ErrorHasOccurred = true;
                                }
                                else
                                {
                                    pi.Reversing = dr[8].ToString().ToUpper();
                                }
                            }

                            break;

                        case 1:                 // Pickup the Debit Total
                            debitTotal = Math.Round(decimal.Parse(dr[5].ToString()), 2);
                            break;

                        case 2:                 // Pickup the Credit Total
                            creditTotal = Math.Round(decimal.Parse(dr[5].ToString()), 2);
                            break;

                        case 3:                 // Pickup the JVE Number
                            pi.jve = dr[1].ToString();
                            try
                            {
                                if (recurringJve.Contains(pi.jve) & !Program.IsRecurring)
                                {
                                    pi.ValidationMessages.Add(string.Format("You did not check that the JE is recurring on the first screen but the sheet {0} is one.", pi.jve));
                                    pi.jve = "(NULL)";
                                }
                                else if (!recurringJve.Contains(pi.jve) & Program.IsRecurring)
                                {
                                    pi.ValidationMessages.Add(string.Format("You checked that the JE is recurring on the first screen but the sheet {0} is not one.", pi.jve));
                                    pi.jve = "(NULL)";
                                }
                                else if (recurringJve.Contains(pi.jve) & dr[5].ToString() != HillConstants.RecurringBatchNumberAsString)
                                {
                                    pi.ValidationMessages.Add(string.Format("The JE cannot be {0} and not Batch {1}.", pi.jve,HillConstants.RecurringBatchNumberAsString));
                                    pi.jve = "(NULL)";
                                }
                                else if (!recurringJve.Contains(pi.jve) & dr[5].ToString() == HillConstants.RecurringBatchNumberAsString)
                                {
                                    pi.ValidationMessages.Add(string.Format("The Batch cannot be {0} and the JE not 300's range.", HillConstants.RecurringBatchNumberAsString));
                                    pi.jve = "(NULL)";
                                }
                                if (pi.jve != string.Empty  && pi.jve != "(NULL)")
                                {

                                    int intjve = 0;
                                    if (!int.TryParse(pi.jve, out intjve))         // JVe is not a number
                                    {
                                        pi.ValidationMessages.Add(string.Format("The Jve on this sheet, {0}, was not a number. Ignoring JVE.", pi.jve));
                                        pi.jve = "(NULL)";
                                    }
                                    else
                                        pi.jve = string.Format("{0:D3}", intjve);
                                }
                                else
                                    pi.jve = "(NULL)";
                            }
                            catch
                            {
                                pi.jve = "(NULL)";
                            }
                            if (pi.jve == "(NULL)")
                            {
                                pi.ValidationMessages.Add(String.Format("Journal Voucher # must be supplied in cell B4. {0}", dr[1].ToString()));
                                pi.ErrorHasOccurred = true;
                            }
                            break;

                        case 4:                 // The headers and ...
                            break;

                        case 5:                 // ...white space between...
                            break;

                        case 6:                 // ...the meta data and data rows
                            break;

                        default:
                            //try
                            //{
                            //    string tmpDeb1 = dr[4].ToString();           // We need to deconstruct this value becasue it might be an Excel null
                            //    if (tmpDeb1 != "")
                            //    {
                            //        decimal tmpdebit = decimal.Parse(tmpDeb1);
                            //        pi.alldebitTotal += tmpdebit;
                            //        detaildebitTotal += tmpdebit;
                            //    }
                            //}
                            //catch
                            //{ }
                            // must be a detail row.  This limits the loop as well becasue only detail rows have a Z

                            if (dr[6].ToString().ToUpper() != "Z")
                            {
                                rowCounter++;
                                continue;
                            }

                            // If we're here, we KNOW it's a detail row.  (It's got a Z)
                            if (firstrow == 0) 
                                firstrow = rowCounter;
                            if (CheckJobCost == true)
                            {
                                if (dr[15].ToString().Length == 0 ||
                                    dr[16].ToString().Length == 0 ||
                                            dr[17].ToString().Length == 0 ||
                                            dr[18].ToString().Length == 0)
                                {
                                    JobCostIsBlank = true;
                                }
                                else
                                {
                                    JobCostIsBlank = false;
                                }

                            }
                            else
                            {
                                JobCostIsBlank = true;
                            }
                            // first check for a blank Z-Code line
                            if ((dr[1].ToString().Length == 0 ||
                                        dr[2].ToString().Length == 0 ||
                                        dr[3].ToString().Length == 0) &
                                        (JobCostIsBlank))
                            {
                                pi.ValidationMessages.Add(String.Format(" Row {0} has a Z Code but appears to be incomplete.{1}", rowCounter + 1, dr[6].ToString()));
                                pi.ErrorHasOccurred = true;
                                continue;
                            }

                            string extra = "";

                            if ((dr[8].ToString().Trim().Length > 10) & (DateTime.TryParse(dr[8].ToString().Trim(), out DateTime value)))
                            {
                                extra = value.ToString("MM/yy");
                            }
                            else
                            {
                                extra = dr[8].ToString().Trim();
                            }

                            // This validates that the Description fields is not greater than 37 bytes.
                            if (!pi.ErrorHasOccurred & (dr[7].ToString().Length + extra.Length > 37))
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {0} Description is {1} long - Max Length is 37", rowCounter + 1, dr[7].ToString().Length + dr[8].ToString().Length));
                                pi.ErrorHasOccurred = true;
                            }

                            // Accmulate some totals
                            decimal debitAmt = 0;
                            decimal creditAmt = 0;

                            try
                            {
                                string tmpDeb = dr[4].ToString().Trim().PadLeft(1, '0');           // We need to deconstruct this value becasue it might be an Excel null
                                if (tmpDeb != "")
                                    debitAmt = Math.Round(decimal.Parse(tmpDeb),2);       // which doesn't play nice with the other children, must be in  pennies
                            }
                            catch
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {0} - '{1}' is invalid for the debit ", rowCounter + 1, dr[4].ToString()));
                                pi.ErrorHasOccurred = true;
                                debitAmt = 0;
                            }

                            try
                            {
                                string tmpCr = dr[5].ToString().Trim().PadLeft(1, '0');            // Same here.
                                if (tmpCr != "")
                                    creditAmt = Math.Round(decimal.Parse(tmpCr),2);  // must be in  pennies
                            }
                            catch
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {0} - '{1}' is invalid for the credit ", rowCounter + 1, dr[5].ToString()));
                                pi.ErrorHasOccurred = true;
                                creditAmt = 0;
                            }

                            // These grand totals are used at the end of the process to ensure that the sum of all
                            // detail rows on all sheets is equal to the Expected Total Amount specified by the user on the wizard.
                            pi.GrandTotalCredit += creditAmt;
                            pi.GrandTotalDebit += debitAmt;

                            // This validation ensures that a single row of data does not have a negative credit or debit.
                            if (!pi.ErrorHasOccurred & (creditAmt < 0 || debitAmt < 0))
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {0} Debit or Credit Amount is negative ", rowCounter + 1));
                                pi.ErrorHasOccurred = true;
                            }

                            // This validation ensures that a single row of data does not contain both credit and debit amounts.
                            if (!pi.ErrorHasOccurred & (creditAmt != 0 && debitAmt != 0))
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {0} Debit and Credit Amounts cannot both have a value", rowCounter + 1));
                                pi.ErrorHasOccurred = true;
                            }
                            // If we have a value for jobcode, go lookup the information,
                            // otherwise, use the  values from the dr.
                            JournalEntry je = new JournalEntry();
                            bool localError = false;
                            proj = null;
                            if (!JobCostIsBlank)
                            {
                                try
                                {
                                    je.JobCode = dr[15].ToString();
                                    je.PhaseCode = dr[16].ToString();
                                    je.CostList = dr[17].ToString();
                                    je.CostCode = dr[18].ToString();
                                    LookupProjDivClass(je, rowCounter + 1);
                                    proj = new ENTITYID(je.EntityID);
                                }
                                catch (Exception ex)
                                {
                                    pi.ValidationMessages.Add(ex.Message);
                                    pi.ErrorHasOccurred = true;
                                    localError = true;
                                }
                            }
                            else
                            {
                                dr[1] = dr[1].ToString().Trim();
                                proj = new ENTITYID(dr[1].ToString());
                                je.JobCode = "";
                                je.PhaseCode = "";
                                je.CostList = "";
                                je.CostCode = "";
                            }

                            if (proj == null)
                            {
                                pi.ValidationMessages.Add("This sheet has one or more rows with incomplete Job Code information.");
                                pi.ErrorHasOccurred = true;
                                localError = true;
                                return false;
                            }
                            // This validation ensures that if there is a value in the Job Code column, the remaining
                            // job code columns also have values.  No Job code data is OK, All JC columns having
                            // data is OK. Partial JC info FAIL.


                            if (proj.value.Trim() == "WCC")
                            {
                                pi.Source = "GW";
                            }
                            else
                            {
                                pi.Source = "GJ";
                            }

                            if (!JobCostIsBlank)
                            {
                                if (dr[15].ToString().Length > 0)
                                {
                                    if (dr[16].ToString().Length == 0 ||
                                        dr[17].ToString().Length == 0 ||
                                    dr[18].ToString().Length == 0)
                                    {
                                        pi.ValidationMessages.Add("This sheet has one or more rows with incomplete Job Code information.");
                                        pi.ErrorHasOccurred = true;
                                        localError = true;
                                    }

                                }
                            }

                            // This validation ensures that if there is a job code, that proj, div and class
                            // are all blank.  They will be filled in by the results of an extra query.
                            if ((dr[1].ToString().Length != 0 ||
                                    dr[2].ToString().Length != 0 ||
                                    dr[3].ToString().Length != 0) &
                                (!JobCostIsBlank))
                            {
                                pi.ValidationMessages.Add($" Row {rowCounter + 1} - You cannot specify both JobCode and Proj, Div and Class. Remove the Proj, Div and Class information from the spreadsheet.");
                                pi.ErrorHasOccurred = true;
                                localError = true;
                            }

                            if (proj.value.Length != 0 & !DoesEntityExist(proj))
                            {
                                pi.ValidationMessages.Add(string.Format(" Row {1} The specified entity {0} does not exist.", proj.value, rowCounter + 1 + 1));
                                pi.ErrorHasOccurred = true;
                            }
                            if (!proj.SendToAs400)  
                                pi.NonAs400Total += debitAmt; 
                            if (pi.IsTaxBasis)
                            {
                                pi.Basis = "T";
                            } 
                            else
                            {
                                pi.Basis = proj.Basis;
                            }
                            
                            if (JobCostIsBlank)
                            {
                                // This validation ensures that the value in the CLASS column matches a value in the GACC table in MRI
                                GACC GLClass = new GACC(dr[3].ToString(), proj);
                                if (!DoesClassExist(GLClass.SqlValue))
                                {
                                    pi.ValidationMessages.Add(string.Format(" Row {1} The specified class {0} does not exist.", GLClass.DisplayValue, rowCounter + 1));
                                    pi.ErrorHasOccurred = true;
                                }
                                else
                                    je.AccountNumber = GLClass.SqlValue;
                                // This validation ensures that the relations of the PROJ, DIV and CLASS 
                                // are such that the row can be inserted into the Journal table.  See the code from a trigger below:
                                // IF exists(select * from inserted as i where i.entityid = '322' and i.AccTNUM > 'HS499999000000' and department = '@')   --REJECT THIS ROW
                                dept = dr[2].ToString().Trim();
                                dept = dept.Replace(@"'", "");
                                if (dept != "@")
                                {
                                    try
                                    {
                                        dept = string.Format("{0:D2}", int.Parse(dept));
                                        if (dept == "00")
                                            dept = "@";
                                    }
                                    catch
                                    {
                                        pi.ValidationMessages.Add(string.Format(" Row {0} - {0} is invalid for the department ", rowCounter + 1, dept));
                                        pi.ErrorHasOccurred = true;
                                        dept = "@";
                                    }

                                }
                                if (proj.HasDepartments)
                                {
                                    if (string.Compare(je.AccountNumber, "HS499999ZZZZZZ") == 1)   // AcctNum is greater that 'hs4999...'
                                    {
                                        if (!pi.ErrorHasOccurred & dept == "@")
                                        {
                                            pi.ValidationMessages.Add(" If the PROJ is 322 and the CLASS is greater than hs4999... the DIV must not be 00");
                                            pi.ErrorHasOccurred = true;
                                        }
                                    }
                                    else
                                    {
                                        if (!pi.ErrorHasOccurred & dept != "@")
                                        {
                                            pi.ValidationMessages.Add(" If the PROJ is 322 and the CLASS is less than hs4999... the DIV must be 00");
                                            pi.ErrorHasOccurred = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!pi.ErrorHasOccurred & dept != "@")
                                    {
                                        pi.ValidationMessages.Add(string.Format(" Row {0} If the PROJ is NOT 322 the DIV {1} must be 00", rowCounter + 1, dept));
                                        pi.ErrorHasOccurred = true;
                                    }
                                }
                            }
                            //=======================================================================
                            // Create a JournalEntry object and add it to the collection in pi.
                            // We will use this collection to create an INSERT statement to put all
                            // of these rows in the Journal table.  Since THIS loop is only for a 
                            // single tab in the spreadsheet, we need to save this collection and
                            // keep adding to it until all tabs are exhausted.
                            //=======================================================================
                            // period
                            // REF = batchid
                            // SOURCE will be HF in all cases
                            // SITEID will be @
                            // ITEM will be an internal counter provided by the JournalEntries collection at INSERT time.
                            // ENTITYID is the PROJ column
                            // ACCTNUM is the mangling of the CLASS column.  Will be in pi right now. (before looping)
                            // DEPARTMENT will be the DIV columm - @ if zero
                            // AMT will be:  If the value is in debit, it's positive.  If the value is in credit it's negative.
                            // DESCRPTN is the DESCRIPTION columm
                            // ENTRDATE is Now()
                            // REVERSAL is N
                            // STATUS is P
                            // USERID is the uid of the person running the wizard.  It's in pi.
                            DateTime entryDate = DateTime.Now;


                            je.Period = pi.Period;
                            je.ReferenceNumber = pi.ReferenceNumber;
                            je.Source = pi.Source;
                            je.SiteID = "@";
                            je.EntityID = proj.value;
                            je.jve = pi.jve;
                            je.Department = dept;
                            je.Basis = proj.Basis;
                            // Do NOT insert a row if both credit and debit are blank
                            bool OKToInsert = true;
                            if (dr[4].ToString().Length == 0 && dr[5].ToString().Length == 0)
                            {
                                OKToInsert = false;
                            }
                            if (debitAmt != 0)
                            {
                                je.Amount = debitAmt;
                            }
                            if (creditAmt != 0)
                            {
                                je.Amount = -creditAmt;
                            }

                            if ((dr[8].ToString().Trim().Length > 10) & (DateTime.TryParse(dr[8].ToString().Trim(), out DateTime value2)))
                            {
                                je.Description = dr[7].ToString().Trim() + " " + dr[8].ToString().Trim().Substring(0, 10);
                            }
                            else
                            {
                                je.Description = dr[7].ToString().Trim() + " " + dr[8].ToString().Trim();
                            }
                            je.EntryDate = DateTime.Now.ToShortDateString();
                            je.Reversal = "N"; //  pi.Reversing; we write our own reversing entries now.
                            je.Status = "P";
                            je.UserID = pi.UserId;



                            // now check to see if Period is closed for entity
                            if (!localError)
                            {
                                bool entperiod = pi.IsPeriodClosed(je.EntityID.Trim(), pi.Period, rowCounter);
                                if (rowCounter == firstrow)  // first journal row
                                {
                                    pi.PriorPeriod = entperiod;
                                }
                                else
                                {
                                    if (pi.PriorPeriod != entperiod)
                                    {
                                        pi.ValidationMessages.Add($@" Row {rowCounter + 1} The entire journal must be for a closed or open period.");
                                        pi.ErrorHasOccurred = true;
                                    }
                                }
                            }
                            if (OKToInsert && je.Amount != 0)
                                pi.JournalRows.Add(je);

                            if (!journalTotals.ContainsKey(je.EntityID.Trim()))
                            {
                                journalTotals.Add(je.EntityID.Trim(), je.Amount);
                            }
                            else
                            {
                                journalTotals[je.EntityID.Trim()] += je.Amount;
                            }

                            break;
                    }
                    rowCounter++;
                }

                foreach (KeyValuePair<string, decimal> journalAmount in journalTotals)
                {
                    if (journalAmount.Value != 0M & !IgnoreJobCost)
                    {
                        journalsUnbalanced.Add(journalAmount.Key.Trim(), journalAmount.Value);
                        pi.ValidationMessages.Add(string.Format("Project {0} journal does not balance. It is {1}.", journalAmount.Key.Trim(), journalAmount.Value));
                        pi.ErrorHasOccurred = true;
                    }
                }


                if (debitTotal != creditTotal)
                {
                    pi.ValidationMessages.Add(string.Format(" Credit Total of {0:c} is not equal to the Debit Total of {1:c}", creditTotal, debitTotal));
                    pi.ErrorHasOccurred = true;
                }
                //else if (debitTotal != Math.Round(detaildebitTotal, 5))
                //{
                //    pi.ValidationMessages.Add(string.Format(" Detail Total of {0:c} is not equal to the Header Total of {1:c}  differnce of {2} ", detaildebitTotal, debitTotal, detaildebitTotal - debitTotal));
                //    pi.ErrorHasOccurred = true;
                //}
                else
                {
                    pi.ValidationMessages.Add($" Debit Total {debitTotal:c}");
                }
                return true;
            } 
            catch(Exception err)
            {
                pi.ValidationMessages.Add(err.Message.Trim());
                return false;
            }
        }

        
        private bool DoesEntityExist(ENTITYID entity )
        {
            DataSet dscls;
            string sql = string.Format("select entityid from entity where entityid = '{0}'", entity.value);

            try
            {
                SqlAgent sa = new SqlAgent(pi);
                dscls = sa.ExecuteQuery(sql);
            }
            catch
            {
                throw;
            }

            if (dscls.Tables[0].Rows.Count > 0)   // Found!
            {
                return true;
            }
            else
                return false;
        }

        private bool DoesClassExist(string cls)
        {
            DataSet dscls;
            string sql = string.Format("select acctnum from gacc where acctnum = '{0}'", cls);

            try
            {
                SqlAgent sa = new SqlAgent(pi);
                dscls = sa.ExecuteQuery(sql);
            }
            catch
            {
                throw;
            }

            if (dscls.Tables[0].Rows.Count > 0)   // Found!
            {
                return true;
            }
            else
                return false;
        }

        private void LookupProjDivClass(JournalEntry je, int rowNum)
        {
            string sql = string.Format(
                "select j.jobcode, m.entityid, m.department, m.acctnum " +
                "from gjob j " +
                "    inner join JC_MAP m on j.jobcode = m.jobcode " +
                "where m.jobcode = '{0}' " +
                "and m.jc_phasecode = '{1}' " +
                "and m.jc_costlist = '{2}' " +
                "and m.jc_costcode = '{3}' ", je.JobCode, je.PhaseCode,je.CostList, je.CostCode);

            try
            {
                SqlAgent sa = new SqlAgent(pi);
                DataSet ds = sa.ExecuteQuery(sql);

                je.EntityID = ds.Tables[0].Rows[0][1].ToString();
                je.Department = ds.Tables[0].Rows[0][2].ToString();
                je.AccountNumber = ds.Tables[0].Rows[0][3].ToString();
            }
            catch
            {
                throw new Exception(string.Format("The job information for the following was not found on row {4}:  JobCode: {0}, PhaseCode: {1}, CostList: {2}, CostCode: {3}",
                    je.JobCode, je.PhaseCode, je.CostList, je.CostCode, rowNum));
            }
        }
    }
}
