﻿using JournalImportWiz;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImport
{
    class Check
    {
        private string _BankID = "";
        private string _CheckNo = "";
        private string _VendID = "";
        private DateTime _CheckDt = DateTime.Parse("1899-01-01 12:00:00.000");
        private string _CheckPd = "";
        private decimal _CheckNet = 0M;
        private string _CkStatus = "";
        private DateTime _LastDate = DateTime.Parse("1899-01-01 12:00:00.000");
        private string _UserID = "";
        private DateTime _DStatChg = DateTime.Parse("1899-01-01 12:00:00.000");
        private string _VendName1 = "";
        private string _EntityID = "";
        private string _PosPay = "";
        private string _BatchID = "";
        private string _WvrPrtd = "";
        private string _Vendid2nd = "";
        private string _Vendnme2 = "";
        private string _VendAddr = "";
        private string _City = "";
        private string _State = "";
        private string _Zip = "";
        private string _DistrbTypeID = "";
        private int _BankRecId = 0;
        private string _AltAddrID = "";
        private double _PayInterFaceID = 0D;
        private string _PayIntfMarker = "";
        private DateTime _LastExportDate = DateTime.MinValue;
        private bool _isErr = false;
        private string _errorMessage = "";

        public string BankID { get => _BankID; set => _BankID = value; }
        public string CheckNo { get => _CheckNo; set => _CheckNo = value; }
        public string VendID { get => _VendID; set => _VendID = value; }
        public DateTime CheckDt { get => _CheckDt; set => _CheckDt = value; }
        public string CheckPd { get => _CheckPd; set => _CheckPd = value; }
        public decimal CheckNet { get => _CheckNet; set => _CheckNet = value; }
        public string CkStatus { get => _CkStatus; set => _CkStatus = value; }
        public DateTime LastDate { get => _LastDate; set => _LastDate = value; }
        public string UserID { get => _UserID; set => _UserID = value; }
        public DateTime DStatChg { get => _DStatChg; set => _DStatChg = value; }
        public string VendName1 { get => _VendName1; set => _VendName1 = value; }
        public string EntityID { get => _EntityID; set => _EntityID = value; }
        public string PosPay { get => _PosPay; set => _PosPay = value; }
        public string BatchID { get => _BatchID; set => _BatchID = value; }
        public string WvrPrtd { get => _WvrPrtd; set => _WvrPrtd = value; }
        public string Vendid2nd { get => _Vendid2nd; set => _Vendid2nd = value; }
        public string Vendnme2 { get => _Vendnme2; set => _Vendnme2 = value; }
        public string VendAddr { get => _VendAddr; set => _VendAddr = value; }
        public string City { get => _City; set => _City = value; }
        public string State { get => _State; set => _State = value; }
        public string Zip { get => _Zip; set => _Zip = value; }
        public string DistrbTypeID { get => _DistrbTypeID; set => _DistrbTypeID = value; }
        public int BankRecId { get => _BankRecId; set => _BankRecId = value; }
        public string AltAddrID { get => _AltAddrID; set => _AltAddrID = value; }
        public double PayInterFaceID { get => _PayInterFaceID; set => _PayInterFaceID = value; }
        public string PayIntfMarker { get => _PayIntfMarker; set => _PayIntfMarker = value; }
        public DateTime LastExportDate { get => _LastExportDate; set => _LastExportDate = value; }
        public bool IsErr { get => _isErr; set => _isErr = value; }
        public string ErrorMessage { get => _errorMessage; set => _errorMessage = value; }

        public void SetErrorMessage(string message)
        {
            _isErr = true;
            _errorMessage += message.Trim() + "\n";
        }
        public void RemoveErrorMessage()
        {
            _isErr = false;
            _errorMessage = "";
        }

        public override bool Equals(object obj)
        {
            return obj is Check check &&
                   _BankID == check._BankID &&
                   _CheckNo == check._CheckNo &&
                   _VendID == check._VendID &&
                   _CheckDt == check._CheckDt &&
                   _CheckPd == check._CheckPd &&
                   _CheckNet == check._CheckNet &&
                   _CkStatus == check._CkStatus &&
                   _LastDate == check._LastDate &&
                   _UserID == check._UserID &&
                   _DStatChg == check._DStatChg &&
                   _VendName1 == check._VendName1 &&
                   _EntityID == check._EntityID &&
                   _PosPay == check._PosPay &&
                   _BatchID == check._BatchID &&
                   _WvrPrtd == check._WvrPrtd &&
                   _Vendid2nd == check._Vendid2nd &&
                   _Vendnme2 == check._Vendnme2 &&
                   _VendAddr == check._VendAddr &&
                   _City == check._City &&
                   _State == check._State &&
                   _Zip == check._Zip &&
                   _DistrbTypeID == check._DistrbTypeID &&
                   _BankRecId == check._BankRecId &&
                   _AltAddrID == check._AltAddrID &&
                   _PayInterFaceID == check._PayInterFaceID &&
                   _PayIntfMarker == check._PayIntfMarker &&
                   _LastExportDate == check._LastExportDate &&
                   _isErr == check._isErr &&
                   _errorMessage == check._errorMessage;
        }

        public override int GetHashCode()
        {
            int hashCode = 158179034;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_BankID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_CheckNo);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_VendID);
            hashCode = hashCode * -1521134295 + _CheckDt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_CheckPd);
            hashCode = hashCode * -1521134295 + _CheckNet.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_CkStatus);
            hashCode = hashCode * -1521134295 + _LastDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_UserID);
            hashCode = hashCode * -1521134295 + _DStatChg.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_VendName1);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_EntityID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_PosPay);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_BatchID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_WvrPrtd);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_Vendid2nd);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_Vendnme2);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_VendAddr);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_City);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_State);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_Zip);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_DistrbTypeID);
            hashCode = hashCode * -1521134295 + _BankRecId.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_AltAddrID);
            hashCode = hashCode * -1521134295 + _PayInterFaceID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_PayIntfMarker);
            hashCode = hashCode * -1521134295 + _LastExportDate.GetHashCode();
            hashCode = hashCode * -1521134295 + _isErr.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_errorMessage);
            return hashCode;
        }
    }
}
