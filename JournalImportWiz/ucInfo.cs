﻿using HelpfulClasses;
using JournalImport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    public partial class ucInfo : UserControl, IJournalWizardControl
    {
        public delegate void NextButtonPressedHandler(int myIndex);
        public event NextButtonPressedHandler NextButtonClick;
        
        PageInfo pi;
        bool CheckHasBeenPressed = false;
        private static bool oneSided = false;

        public static bool OneSided { get => oneSided; set => oneSided = value; }

        public ucInfo()
        {
            InitializeComponent();
        }

        private void ucInfo_Load(object sender, EventArgs e)
        {
            if(HelpfulClasses.HelpfulFunctions.UserID.Trim().ToUpper().Contains("BHEPNER") | 
                HelpfulClasses.HelpfulFunctions.UserID.Trim().ToUpper().Contains("JHALL"))
            {
                chkOneSided.Visible = true;
            }
            else
            {
                chkOneSided.Visible = false;
            }
        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {
            EnableNextButton();
            decimal tmp = 0;
            if (decimal.TryParse(txtTotal.Text, out tmp))
                pi.ExpectedBatchTotal = tmp;
        }

        private void txtPeriod_TextChanged(object sender, EventArgs e)
        {
            EnableNextButton();



            //if (int.TryParse(txtPeriod.Text, out period))
            //    EnableNextButton();
            //else
            //{
            //    if (txtPeriod.Text.Length <= 1)
            //        txtPeriod.Text = string.Empty;
            //    else
            //        txtPeriod.Text = txtPeriod.Text.Substring(0, txtPeriod.Text.Length - 1);
            //}

            //SendKeys.Send("{END}");

        }

        private void EnableNextButton()
        {
            bool btnEnabled = false;

            if (txtPeriod.Text.Length >= 5)
            {
                btnEnabled = true;
            }

            btnNext.Enabled = btnEnabled;
        }

        //private void EnableCheckButton()
        //{
        //    bool btnEnabled = false;

        //    // Enhancement 2, Determine if the Source, Ref, Period already exists.
        //    if (txtRefNum.Text.Length > 0 && txtPeriod.Text.Length == 6)
        //        btnEnabled = true;

        //    btnCheck.Enabled = btnEnabled;
        //}

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                // First, last minute validation of the Period field.
                if (txtPeriod.Text.Length < 5 | !DateTime.TryParse("01/" + txtPeriod.Text.Trim(), out DateTime period))
                {
                    MessageBox.Show("Period must be in the format 'mm/yyyy' or 'mm/yy'.  Use preceding zeros.  Must have a / symbol between month and year.", "Period");
                    txtPeriod.Focus();
                    return;
                }

                if (!int.TryParse(txtTotal.Text.Trim(), out int intTotal) & !decimal.TryParse(txtTotal.Text.Trim(), out decimal decTotal))
                {
                    MessageBox.Show("The total needs to be numeric.", "Period");
                    txtTotal.Focus();
                    return;
                }

                int mon = 0;

                if (int.TryParse(txtPeriod.Text.Substring(0, 2), out mon))
                {
                    if (mon < 1 || mon > 12)
                    {
                        MessageBox.Show("Month is outside the range of an actual month.", "Period");
                        txtPeriod.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("the month needs to be a number.", "Period");
                    txtPeriod.Focus();
                    return;
                }

                if (txtPeriod.Text.Trim().Length >= 5)
                {
                    if (txtPeriod.Text.Substring(2, 1) != "/")
                    {
                        MessageBox.Show("You've placed the '/' character in the wrong place.  It must be between the month and year.", "Period");
                        txtPeriod.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("The period entered is not formatted properly. It must be month/year.", "Period");
                    txtPeriod.Focus();
                    return;
                }

                if (txtDescription.Text.Length == 0)
                {
                    MessageBox.Show("You must enter a description");
                    txtDescription.Focus();
                    return;
                }

                // We're good!
                if (txtPeriod.Text.Trim().Length == 7)
                {
                    pi.Period = string.Format("{0}{1}", txtPeriod.Text.Substring(3, 4), txtPeriod.Text.Substring(0, 2));
                    pi.Description = txtDescription.Text;
                }
                else if (txtPeriod.Text.Trim().Length == 5)
                {
                    int cenYear = int.Parse(DateTime.Now.Year.ToString().Substring(0, 2) + "00");
                    int curYear = int.Parse(txtPeriod.Text.Substring(3, 2));
                    int cmpYear = cenYear + curYear;


                    pi.Period = string.Format("{0}{1}", cmpYear, txtPeriod.Text.Substring(0, 2));
                    pi.Description = txtDescription.Text.Replace("'", "''");
                }

                HelpfulFunctions.LogMessage($@"A batch has been started. No records have been written yet. 
                                               The Journal Entry is {(chkOneSided.Checked ? "one sided." : "not one sided.")}
                                               The Journal Entry is {(chkJVE.Checked ? "is recurring." : "is not recurring.")}
                                               The Journal Entry period is {txtPeriod.Text.Trim()}, batch total {txtTotal.Text.Trim()}, 
                                               and the description {txtDescription.Text.Trim()}.", "Application",
                               System.Diagnostics.EventLogEntryType.Information);

                // Raise the event to the form.
                if (this.NextButtonClick != null)
                    this.NextButtonClick(0);
            } 
            catch(Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), "Application", System.Diagnostics.EventLogEntryType.Error);

                btnNext.Enabled = false;
            }
        }
        public void PerformWork(PageInfo _pi)
        {
            pi = _pi;
            pi.Source = "GJ";
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            CheckHasBeenPressed = true;
            DataSet ds = null;
            SqlAgent sa = new SqlAgent(pi);

            string sql =
                "select * from {3} " +
                "where period = {0} " +
                "and ref = {1} " +
                "and source = '{2}' ";
            sql = string.Format(sql, pi.Period, pi.ReferenceNumber, pi.Source, "Journal");

            try
            {
                sa = new SqlAgent(pi);
                ds = sa.ExecuteQuery(sql);
            }
            catch(Exception ex)
            {
                throw;
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                // Total the amount
                decimal amount = 0;
                foreach (DataRow drow in ds.Tables[0].Rows)
                {
                    decimal d = decimal.Parse(drow[9].ToString());
                    if (d > 0)
                        amount += decimal.Parse(drow[9].ToString());
                }

                string msg = string.Format("A batch of rows with this Source, Ref and Period has been found containing {0} items for a total of {1}.  Would you like to remove this batch from the Journal? ",
                    ds.Tables[0].Rows.Count.ToString(), amount.ToString("C"));

                DialogResult dr = MessageBox.Show(msg, "Rows Found!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    frmInputBox f = new frmInputBox();
                    f.ShowDialog();
                    if(f.OKWasPressed)
                    {
                        decimal d = decimal.Parse(f.TextResult);
                        if (d == amount)
                        {
                            // delete the batch
                            try
                            {

                                sql =
                                    "delete from journal " +
                                    "where period = {0} " +
                                    "and ref = {1} " +
                                    "and source = '{2}' ";
                                sql = string.Format(sql, pi.Period, pi.ReferenceNumber, pi.Source);

                                sa.BeginTransaction();
                                sa.ExecuteNonQuery(sql);
                                sa.CommitTransaction();

                                MessageBox.Show("Batch Deleted");
                            }
                            catch
                            {
                                sa.RollbackTransaction();
                                throw;
                            }
                        }
                        else
                            MessageBox.Show("Amount entered does not match the expected amount.  Batch delete not performed.");
                    }
                }
                else
                {
                }
            }
            else
            {
            }
        }

        private void btnSummary_Click(object sender, EventArgs e)
        {
            frmSummary f = new frmSummary(pi);
            f.ShowDialog();
        }

        private void txtPeriod_KeyPress(object sender, KeyPressEventArgs e)
        {
            //prevent the keying of anything but numbers and /
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '/'))
            {
                e.Handled = true;
            }

        }

        private void btnSiteLinkInterface_Click(object sender, EventArgs e)
        {
            frmInterfaceSiteLink siteLink = new frmInterfaceSiteLink();
            siteLink.ShowDialog();
        }

        private void chkJVE_CheckedChanged(object sender, EventArgs e)
        {
            Program.IsRecurring = chkJVE.Checked;
        }

        private void txtTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //prevent the keying of anything but numbers and .
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void chkOneSided_CheckedChanged(object sender, EventArgs e)
        {
            oneSided = chkOneSided.Checked;
        }
    }
}
