﻿
namespace JournalImport
{
    partial class frmHomeSalesConservice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnListImports = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.lstFilesToUpload = new System.Windows.Forms.ListBox();
            this.lstErrorMessages = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExportErrors = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload Directory:";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(107, 33);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(617, 20);
            this.txtPath.TabIndex = 1;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // btnListImports
            // 
            this.btnListImports.Location = new System.Drawing.Point(634, 59);
            this.btnListImports.Name = "btnListImports";
            this.btnListImports.Size = new System.Drawing.Size(90, 23);
            this.btnListImports.TabIndex = 2;
            this.btnListImports.Text = "List Imports";
            this.btnListImports.UseVisualStyleBackColor = true;
            this.btnListImports.Click += new System.EventHandler(this.btnListImports_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(634, 88);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Enabled = false;
            this.btnUpload.Location = new System.Drawing.Point(182, 248);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(90, 23);
            this.btnUpload.TabIndex = 4;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // lstFilesToUpload
            // 
            this.lstFilesToUpload.FormattingEnabled = true;
            this.lstFilesToUpload.Location = new System.Drawing.Point(3, 278);
            this.lstFilesToUpload.Name = "lstFilesToUpload";
            this.lstFilesToUpload.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstFilesToUpload.Size = new System.Drawing.Size(795, 173);
            this.lstFilesToUpload.TabIndex = 6;
            this.lstFilesToUpload.SelectedIndexChanged += new System.EventHandler(this.lstFilesToUpload_SelectedIndexChanged);
            // 
            // lstErrorMessages
            // 
            this.lstErrorMessages.FormattingEnabled = true;
            this.lstErrorMessages.HorizontalScrollbar = true;
            this.lstErrorMessages.Location = new System.Drawing.Point(804, 31);
            this.lstErrorMessages.Name = "lstErrorMessages";
            this.lstErrorMessages.Size = new System.Drawing.Size(469, 420);
            this.lstErrorMessages.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(801, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Errors:";
            // 
            // btnExportErrors
            // 
            this.btnExportErrors.Location = new System.Drawing.Point(723, 248);
            this.btnExportErrors.Name = "btnExportErrors";
            this.btnExportErrors.Size = new System.Drawing.Size(75, 23);
            this.btnExportErrors.TabIndex = 9;
            this.btnExportErrors.Text = "Export Errors";
            this.btnExportErrors.UseVisualStyleBackColor = true;
            this.btnExportErrors.Click += new System.EventHandler(this.btnExportErrors_Click);
            // 
            // frmHomeSalesConservice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 457);
            this.Controls.Add(this.btnExportErrors);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstErrorMessages);
            this.Controls.Add(this.lstFilesToUpload);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnListImports);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.Name = "frmHomeSalesConservice";
            this.Text = "Home Sales Conservice";
            this.Load += new System.EventHandler(this.frmHomeSalesConservice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button btnListImports;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.ListBox lstFilesToUpload;
        private System.Windows.Forms.ListBox lstErrorMessages;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExportErrors;
    }
}