﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    public partial class ucExcel : UserControl, IJournalWizardControl
    {
        public delegate void NextButtonPressedHandler(int myIndex);
        public event NextButtonPressedHandler NextButtonClick;

        PageInfo pi;

        public ucExcel()
        {
            InitializeComponent();
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                pi.ExcelFileName = openFileDialog1.FileName;
                string[] FilePath = pi.ExcelFileName.Trim().Split('\\');

                for (int i = 0; i < FilePath.Length - 2; i++)
                {
                    lblFilePath.Text += FilePath[i].Trim() + "\\";
                }

                lblFileName.Text = FilePath[FilePath.Length - 1];
                btnNext.Enabled = true;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            HelpfulFunctions.LogMessage($@"A file has been selected for the batch.
                                           The file selected is {lblFileName.Text.Trim()}.
                                           The file is found here {lblFilePath.Text.Trim()}.", "Application",
                               System.Diagnostics.EventLogEntryType.Information);

            if (this.NextButtonClick != null)
                this.NextButtonClick(1);
        }

        public void PerformWork(PageInfo _pi)
        {
            pi = _pi;
            lblFileName.Width = this.ClientRectangle.Width - 10;
            btnChoose.Focus();
        }

        private void ucExcel_Load(object sender, EventArgs e)
        {
            lblFileName.Text = "";
            lblFilePath.Text = "";
        }
    }
}
