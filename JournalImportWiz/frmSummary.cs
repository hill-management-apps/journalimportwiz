﻿using HelpfulClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    public partial class frmSummary : Form
    {
        PageInfo pi = null;

        public frmSummary(PageInfo _pi)
        {
            InitializeComponent();
            pi = _pi;
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmSummary_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSummaryInfo();
            }
            catch
            {
                throw;
            }

        }

        private void LoadSummaryInfo()
        {
            dgvSummary.DataSource = null;

            try
            {
                SqlAgent sa = new SqlAgent(pi);
                DataSet ds = sa.ExecuteQuery("select " +
                    "glsource as Source," +
                    "glref as RefNum," +
                    "substring(period, 5, 2) + '/' + substring(period, 1, 4) as Periodstr, " +
                    "basis as Basis, " +
                    "format(batchamt, 'C') as Amount, " +
                    "release as Released," +
                    "descr as Description " +
                    "from hs_glbnctl where glsource = 'GJ' and release = 'N' And period > 202012" +
                    "Order by period, GLREF" );

                dgvSummary.DataSource = ds.Tables[0];
                dgvSummary.Columns[5].Width = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error ocurred: {0}", ex.Message));
            }
        }

        private void dgvSummary_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DialogResult dr = DialogResult.Cancel;

            try
            {
                switch (e.ColumnIndex)
                {
                    case 0:     // Delete button
                        dr = MessageBox.Show(string.Format("Are you sure you wish to DELETE the BATCH where RefNum = {0} Period = {1} ?", 
                                dgvSummary.Rows[e.RowIndex].Cells[3].Value, dgvSummary.Rows[e.RowIndex].Cells[4].Value.ToString()
                                             ), "Delete Batch", MessageBoxButtons.YesNo);
                        if (dr == DialogResult.Yes)
                            DeleteBatch(dgvSummary.Rows[e.RowIndex].Cells[3].Value.ToString(), dgvSummary.Rows[e.RowIndex].Cells[4].Value.ToString());

                        break;

                    case 1:     // Release button
                        dr = MessageBox.Show(string.Format("Are you sure you wish to RELEASE the BATCH where RefNum = {0} ?", dgvSummary.Rows[e.RowIndex].Cells[3].Value), "Release Batch", MessageBoxButtons.YesNo);
                        if (dr == DialogResult.Yes)
                            ReleaseBatch(dgvSummary.Rows[e.RowIndex].Cells[3].Value.ToString(), dgvSummary.Rows[e.RowIndex].Cells[4].Value.ToString());
                        break;

                    default:    // Do Nothing.
                        break;
                }
                
                LoadSummaryInfo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteBatch(string batchID, string grdPeriod)
        {
            SqlAgent sa = null;
            string newPeriod = string.Format("{0}{1}", grdPeriod.Substring(3, 4), grdPeriod.Substring(0, 2));
            string sqlJournal = string.Format("delete from journal where period = {0} and ref = {1} and source = 'GJ' ", newPeriod, batchID);
            string sqlJournalHistory = string.Format("insert into GL_Journal_History (period, ref, source, siteid, actiontype, lastdate, userid) VALUES " +
                "( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )",
                newPeriod, batchID, pi.Source, "@", "REMOVE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), pi.UserId);
            string sqlGLBNCTL = string.Format("delete from hs_glbnctl where glSource = 'GJ' and glref = {0} and period = {1}", batchID, newPeriod);

            try
            {
                sa = new SqlAgent(pi);
                sa.BeginTransaction();

                sa.ExecuteNonQuery(sqlJournal);
                sa.ExecuteNonQuery(sqlGLBNCTL);
                sa.ExecuteNonQuery(sqlJournalHistory);

                sa.CommitTransaction();
            }
            catch(Exception ex)
            {
                sa.RollbackTransaction();
                MessageBox.Show(string.Format("The selected batch WAS NOT REMOVED due to the following condition: {0}.", ex.Message));
                throw ex;
            }
        }

        private void ReleaseBatch(string batchID, string grdPeriod)
        {
            SqlAgent sa = null;
            string newPeriod = string.Format("{0}{1}", grdPeriod.Substring(3, 4), grdPeriod.Substring(0, 2));
            string sqlGLBNCTL = string.Format("update hs_glbnctl set release = 'Y' where period = {0} and glref = {1} and glsource = 'GJ' ", newPeriod, batchID);
            string sqlJournal;
            string sqlJournalHistory;

            if (batchID.Trim() != HillConstants.RecurringBatchNumberAsString) {
                sqlJournal = string.Format("update Journal set HS_GLBATCHNO = {2}  where period = {0} and ref = '{1}' and source = 'GJ' ", newPeriod, batchID, -999);
                sqlJournalHistory = string.Format("insert into GL_Journal_History (period, ref, source, siteid, actiontype, lastdate, userid) VALUES " +
                    "( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )",
                    newPeriod, batchID, pi.Source, "@", "RELEASE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), pi.UserId);
            } else
            {
                sqlJournal = string.Format("update Journal set HS_GLBATCHNO = {2}  where period = {0} and ref = '{1}' and source = 'GJ' ", newPeriod, batchID, -HillConstants.RecurringBatchNumber  );
                sqlJournalHistory = string.Format("insert into GL_Journal_History (period, ref, source, siteid, actiontype, lastdate, userid) VALUES " +
                    "( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )",
                    newPeriod, batchID, pi.Source, "@", "RELEASE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), pi.UserId);
            }

            try
            {
                sa = new SqlAgent(pi);
                sa.BeginTransaction();

                sa.ExecuteNonQuery(sqlJournal);
                sa.ExecuteNonQuery(sqlGLBNCTL);
                sa.ExecuteNonQuery(sqlJournalHistory);

                sa.CommitTransaction();
            }
            catch (Exception ex)
            {
                sa.RollbackTransaction();
                MessageBox.Show(string.Format("The selected batch WAS NOT RELEASED due to the following condition: {0}.", ex.Message));
                throw ex;
            }
        }
    }
}
