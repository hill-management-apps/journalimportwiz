﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImportWiz
{
    public class ENTITYID
    {
        string id;
        int idint;

        public ENTITYID(string ID)
        {
            try
            {
                idint = int.Parse(ID);
                id = string.Format("{0:D3}", idint);
            }
            catch
            {
                id = ID;
            } 
        }
        public string value
        {
            get
            {
                return id;
            }
        }
        public string ledgerid
        {
            get
            {
                if (id == "WCC")
                    return "WC";
                else
                    return "HS";
            }
        }
        public string Basis
        {
            get
            {
                if (id == "WCC")
                    return "A";
                else
                    return "C";
            }
        }
        public bool SendToAs400
        {
            get
            {
                if ((ledgerid != "HS") || (id == "502") || (id == "451"))
                    return false;
                else
                    return true;
            }
        }
        public bool HasDepartments
        {
            get
            {
                if ((id == "322") || (id == "030") || (id  == "313") || (id == "069") )
                    return true;
                else
                    return false;
            }
        }
    }

    public class GACC
    {
        string major;
        string sub;
        string original;
        string ledger;
        bool validformat = true;
        public GACC(String Acct, ENTITYID proj)
        {
            original = Acct;
            Acct = Acct.Replace("-", "").
                        Replace("/", "").
                        Replace(".", "").
                        Replace(",", "").
                        Replace("|", "").
                        Replace("`", "").
                        Replace("\\","");

            if (Acct.Length < 6)
                validformat = false;
            if (validformat)
            {
                ledger = Acct.Substring(0, 2);
                if ((ledger == "HS") || (ledger == "WC"))
                {
                    Acct = Acct.Substring(3);   // drop off Ledger Code
                }
                ledger = proj.ledgerid;
            }

            if (Acct.Length < 6)
                validformat = false;
            if (validformat)
            {
                this.major = Acct.Substring(0, 6);
                if (this.major.Length != 6)
                    validformat = false;

                if (Acct.Length > 6 & Acct.Length <= 12)
                {
                    sub = Acct.Substring(6).PadRight(6,'0');
                }
                else if(Acct.Length == 6)
                {
                    sub = "000000";
                }
                else
                {
                    validformat = false;
                }
            }

        }
        public string SqlValue
        {
            get
            {
                if (validformat)
                    return ledger + this.major + this.sub;
                else
                    return original;
            }
        }
        public string DisplayValue
        {
            get
            {
                if (validformat)
                    return this.major + "-" + this.sub;
                else
                    return original;

            }
        }
    }
    
    public class JournalEntry
    {
        public string Period = string.Empty;
        public string ReferenceNumber = string.Empty;
        public string Source = "HF";
        public string SiteID = "@";
        public int Item = 0;
        public string EntityID = string.Empty;
        public string AccountNumber = string.Empty;
        public string jve = string.Empty;
        private string department = string.Empty;   // There's a public property for this member
        public decimal Amount = 0;
        public string Description = string.Empty;
        public string AddlDesc = string.Empty;
        public string EntryDate = string.Empty;
        public string Reversal = "N";
        public string Status = "p";
        public string Basis = "C";
        public string UserID = string.Empty;
        public string JobCode = string.Empty;
        public string PhaseCode = string.Empty;
        public string CostList = string.Empty;
        public string CostCode = string.Empty;
        public string trueEntity = string.Empty;
        public bool ProgramGenerated = false;

        public JournalEntry()
        {
        }



        public string Department
        {
            get { return department; }
            set
            {
                if (value.Trim() == "@")
                {
                    department = value;
                }
                else
                {
                    string tmp = value;
                    if (tmp == string.Empty)
                        tmp = "0";

                    if (int.Parse(tmp) == 0)
                        department = "@";
                    else
                        department = tmp;
                }
            }
        }

        public string GetInsertStatement(string RefNum, bool PriorPeriod)
        {
            // Here's the column list declared in JournalEntries:
            // (period, ref, source, siteid, item, entityid, acctnum, department, amt, descrpn, *BALFOR, entrdate, reversal, status, userid, basis, jobcode, phasecode, costlist, costcode, lastdate, addldesc)
            string sql;
            if (!PriorPeriod)
            {
                if (JobCode != "")
                {
                    sql = string.Format("('{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', {8}, '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', {20}",
                       Period, RefNum, Source, SiteID, Item, EntityID, AccountNumber, Department, Amount, Description.Replace("'", "''"), EntryDate, Reversal, Status, UserID, Basis, JobCode, PhaseCode, CostList, CostCode, DateTime.Now, jve);
                }
                else
                {
                    sql = string.Format("('{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', {8}, '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', NULL, NULL, NULL, NULL, '{15}', {16}  ",
                        Period, RefNum, Source, SiteID, Item, EntityID, AccountNumber, Department, Amount, Description.Replace("'", "''"), EntryDate, Reversal, Status, UserID, Basis, DateTime.Now, jve);
                }
            }
            else
            {
                if (JobCode != "")
                {
                    sql = string.Format("('{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', {8}, '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', {19}",
                       Period, RefNum, Source, SiteID, Item, EntityID, AccountNumber, Department, Amount, Description.Replace("'", "''"), EntryDate, UserID, Basis, 'N', JobCode, PhaseCode, CostList, CostCode, DateTime.Now, jve);
                }
                else
                {
                    sql = string.Format("('{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', {8}, '{9}', '{10}', '{11}', '{12}', '{13}',NULL, NULL, NULL, NULL,  '{14}', {15}  ",
                        Period, RefNum, Source, SiteID, Item, EntityID, AccountNumber, Department, Amount, Description.Replace("'", "''"), EntryDate, UserID, Basis, 'N', DateTime.Now, jve);
                }
            }
            if (AddlDesc != "")
            {
                sql = sql + string.Format(", ' {0}'", AddlDesc.Replace("'", "''"));
            }
            else
            {
                sql = sql + ", NULL";
            }
            sql = sql + "), ";
            return sql;
        }
    }

}
