﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImportWiz
{
    public class JournalEntries
    {
        private List<JournalEntry> journalEntries = null;
        private int currentCounter = 1;     // This is used to fill the ITEM column, then incremented for each row.

        public JournalEntries()
        {
            journalEntries = new List<JournalEntry>();
        }

        public void Add(JournalEntry je)
        {
            je.Item = currentCounter++;
            journalEntries.Add(je);
        }

        public List<JournalEntry> GetItems()
        {
            return journalEntries;
        }

        public void Clear()
        {
            currentCounter = 1;
            journalEntries.Clear();
        }

        public string GetInsertStatement(int start, string RefNum, bool PriorPeriod)
        {
            string sql;
            if (!PriorPeriod)
            {
                sql = String.Format("insert into journal (period, ref, source, siteid, item, entityid, acctnum, department, amt, descrpn, entrdate, reversal, status, userid, basis, jobcode, jc_phasecode, jc_costlist, jc_costcode, lastdate, HS_JVNO, ADDLDESC) VALUES ");
            }
            else
            {
                sql = String.Format("insert into ghis (period, ref, source, siteid, item, entityid, acctnum, department, amt, descrpn, entrdate, userid, basis, BALFOR, jobcode, jc_phasecode, jc_costlist, jc_costcode, lastdate, HS_JVNO, ADDLDESC) VALUES ");
            }


            for (int s = 0 + start; (s < 200 + start) &  (s < journalEntries.Count); s++)
            {
                JournalEntry je = journalEntries[s];
                sql += je.GetInsertStatement(RefNum, PriorPeriod);
            }

            sql += "; ";

            sql = sql.Substring(0, sql.Length - 4);     // Trying to strip the trailing comma.  Subtract 4 because of spaces after the comma.
            sql += "; ";                                // Then add a trailing semi to indicate the end of the batch.

            return sql;

        }

        public decimal GetTotalCredits()
        {
            decimal credits = 0;

            credits = (from JournalEntry genJrn in journalEntries
                       where genJrn.Amount < 0
                       select genJrn.Amount).Sum();

            return credits;                    
        }

        public decimal GetTotalDebits()
        {
            decimal debits = 0;

            debits = (from JournalEntry genJrn in journalEntries
                       where genJrn.Amount > 0
                       select genJrn.Amount).Sum();

            return debits;
        }
    }
}
