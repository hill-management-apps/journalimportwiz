﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HelpfulClasses;
using SqlDataAccess;

namespace JournalImportWiz
{
    public partial class ucValidate : UserControl, IJournalWizardControl
    {
        PageInfo pi;
        public event EventHandler DoneButtonClick;
        public event EventHandler ReRunButtonClick;
        SqlAgent sa;
        string RefNum;
        decimal credit;
        decimal debit;

        public ucValidate()
        {
            InitializeComponent();
        }

        private void WriteError(string errorMessage)
        {
            List<string> errorMessages = txtErrorMessages.Lines.ToList();
            
            errorMessages.Add(errorMessage.Trim());

            txtErrorMessages.Lines = errorMessages.ToArray();
        }

        public void PerformWork(PageInfo _pi)
        {
            try
            {
                HelpfulFunctions.LogMessage($@"Beginning to validate batch.", "Application",
               System.Diagnostics.EventLogEntryType.Information);

                pi = _pi;
                pi.ErrorHasOccurred = false;

                // Perform the validation routines.
                ExcelAgent ea = new ExcelAgent();
                try
                {
                    ea.OpenSpreadsheet(pi);
                }
                catch (IOException e)
                {
                    WriteError("This file is already open by someone else. Please make sure the file is not being used.");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    return;
                }
                catch (Exception e)
                {
                    WriteError($"Unexpected Error 10: '{e}'");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    return;
                }

                try
                {
                    ea.PerformValidation();
                }
                catch (Exception e)
                {
                    WriteError($"Unexpected Error 11: '{e}'");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    return;
                }

                try
                {
                    sa = new SqlAgent(pi);
                }
                catch (Exception e)
                {
                    WriteError($"Unexpected Error 12: '{e}'");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    return;
                }

                Cursor = Cursors.WaitCursor;
                string originalref = string.Empty;
                pi.CompletionMessages.Clear();

                try
                {
                    sa.BeginTransaction();
                    HelpfulFunctions.LogMessage($@"Commitment control has began.", "Application",
                               System.Diagnostics.EventLogEntryType.Information);
                }
                catch (Exception err)
                {
                    WriteError($"Unexpected Error: '{err}'");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }

                HelpfulFunctions.LogMessage($@"Beginning to write batch.", "Application",
                                               System.Diagnostics.EventLogEntryType.Information);

                try
                {
                    WriteBatch(pi);
                    pi.CompletionMessages.Add($@"Created Batch {pi.ReferenceNumber} in Period {pi.Period}");
                    originalref = RefNum;

                    if (pi.Reversing == "Y")
                    {
                        string perdate = pi.Period.Substring(4, 2) + "/01/" + pi.Period.Substring(0, 4);
                        string originalPeriod = pi.Period;
                        DateTime nextPeriod = DateTime.Parse(perdate).AddMonths(1);
                        pi.Period = nextPeriod.Period();
                        FileInfo fi = new FileInfo(pi.ExcelFileName.Trim());
                        bool first = true;
                        foreach (JournalEntry j in pi.JournalRows.GetItems())
                        {
                            if (first)
                            {
                                pi.PriorPeriod = pi.IsPeriodClosed(j.EntityID, pi.Period, 1);
                                first = false;
                            }
                            j.Amount = -j.Amount;
                            j.Description = string.Format("Rev {0}", j.Description);
                            if (j.Description.Length > 37)
                                j.Description = j.Description.Substring(0, 37);
                            j.Period = pi.Period;
                            j.AddlDesc = string.Format("REV {0} Ref:{1} {2}", originalPeriod, originalref, fi.Name);
                        }

                        WriteBatch(pi);
                        pi.CompletionMessages.Add($@"Created Reversing Batch {pi.ReferenceNumber} in Period {pi.Period}");

                        pi.ReferenceNumber = pi.ReferenceNumber + originalref;
                    }
                }
                catch (FormatException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (ArgumentNullException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (OverflowException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (InvalidCastException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (SqlException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (IOException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (ObjectDisposedException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (InvalidOperationException err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
                catch (Exception err)
                {
                    WriteError($"{err.Message.Trim()}");
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Batch has been rolled back. Commitment control has ended.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);
                    return;
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        public void WriteBatch(PageInfo _pi)
        {
            // Need to make this next section it's own function
            try
            {
                // This is the beginning of a three-part transaction.  First we update the Journal,
                // then the Journal History, then a tickler file for the 400 called HS_GLBNCTL.
                // We start by beginning a transaction.
                if (!pi.ErrorHasOccurred)
                {
                    // get the reference number now so as to limit the posibility of two processes getting the same number.  
                    try
                    {
                        SqlAgent sqlAgent = new SqlAgent(pi);
                        //call the stored procedure to get the next ref number and store the output
                        DataTable locTbl = sqlAgent.ExecuteQuery($@"exec dbo.HS_GetNextRefNum 
                                                     '{pi.Period.Trim()}', '{pi.Source.Trim()}', 0, 199").Tables[0];
                        //if a record is returned
                        if (locTbl.Rows.Count > 0)
                        {
                            RefNum = locTbl.Rows[0][0].ToString().Trim();
                            HelpfulFunctions.LogMessage($@"Reference number {RefNum.Trim()} gotten.", "Application", EventLogEntryType.Information);
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                    pi.ReferenceNumber = RefNum;

                    string sqlJournalHistory = string.Format("insert into GL_Journal_History (period, ref, source, siteid, actiontype, lastdate, userid) VALUES " +
                        "( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )",
                        pi.Period, pi.ReferenceNumber, pi.Source, "@", "CREATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), pi.UserId);

                    string sqlGLBNCTL;

                    try
                    {
                        sqlGLBNCTL = GetInsertForGLBNCTL();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                    // Next we execute the various updates
                    // Create the SQL necessary to update the Journal, JournalHistory and HS_GLBNCTL tables.
                    for (int s = 0; s < pi.JournalRows.GetItems().Count; s += 200)
                    {
                        string sqlJournal = pi.JournalRows.GetInsertStatement(s, RefNum, pi.PriorPeriod);
                        try
                        {

                            sa.ExecuteNonQuery(sqlJournal);
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                    // need to validate here 
                    string sqlValidate = string.Format("Select coalesce(sum(AMT),0) as debit from {3} with (nolock)  where period = {0} and source = '{1}' and ref = '{2}' and amt > 0", 
                             pi.Period, pi.Source, pi.ReferenceNumber, pi.JournalTableName);
                    //                    sa.RollbackTransaction();
                    SqlAgent sav = new SqlAgent(pi);
                    DataSet ds;
                    try
                    {
                        ds = sav.ExecuteQuery(sqlValidate);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    debit = decimal.Parse(ds.Tables[0].Rows[0][0].ToString());
                    sqlValidate = string.Format("Select coalesce(sum(AMT),0) as credit from {3} with (nolock) where period = {0} and source = '{1}' and ref = '{2}' and amt < 0", 
                            pi.Period, pi.Source, pi.ReferenceNumber, pi.JournalTableName);
                    try
                    {
                        ds = sav.ExecuteQuery(sqlValidate);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    credit = decimal.Parse(ds.Tables[0].Rows[0][0].ToString());

                    if ((debit == -credit) && (debit == Math.Round(pi.GrandTotalDebit, 5)))
                    {
                        try
                        {
                            sa.ExecuteNonQuery(sqlJournalHistory);
                            sa.ExecuteNonQuery(sqlGLBNCTL);
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                    else
                    {
                        pi.ValidationMessages.Add(string.Format("Total Debits {0:C} <> Total Credits {1:C} or Grand Total {2:C}", debit, -credit, pi.GrandTotalDebit));
                        pi.ErrorHasOccurred = true;
                    }
                }

                if (pi.ErrorHasOccurred)
                {
                    label1.BackColor = Color.Red;
                    label1.Text = "Validation Errors have Occurred";
                    btnUpdate.Enabled = false;
                    sa.RollbackTransaction();
                }
                else
                {
                    label1.BackColor = Color.Green;
                    label1.Text = "Validation OK";
                    btnUpdate.Enabled = true;
                }

                txtErrorMessages.Lines = null;
                foreach (string msg in pi.ValidationMessages)
                    if (!txtErrorMessages.Lines.Contains(msg))
                    {
                        WriteError(msg);
                    }
            }
            catch (FormatException e)
            {
                throw e;
            }
            catch (ArgumentNullException e)
            {
                throw e;
            }
            catch (OverflowException e)
            {
                throw e;
            }
            catch (InvalidCastException e)
            {
                throw e;
            }
            catch (SqlException e)
            {
                throw e;
            }
            catch (IOException e)
            {
                throw e;
            }
            catch (ObjectDisposedException e)
            {
                throw e;
            }
            catch (InvalidOperationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            if (DoneButtonClick != null)
                DoneButtonClick(sender, e);
        }

        private void btnRerun_Click(object sender, EventArgs e)
        {
            try
            {
                sa.RollbackTransaction();
            }
            catch(Exception err)
            {

            }
            if (ReRunButtonClick != null)
                ReRunButtonClick(sender, e);
        }
        public string CalculateRefNum()
        {
            string retVal = string.Empty;

            try
            {
                DataSet ds = new DataSet();
                string sql = "";

                sql = string.Format("select max(ref) from (select max(ref) as ref from journal where source = '{0}' and period = {1} and ref not like '%[^0-9]%'", pi.Source, pi.Period) +
                      string.Format("union select max(ref) as ref from ghis where source = '{0}' and period = {1} and ref not like '%[^0-9]%') as refunion HAVING MAX(REF) <= '000099'", pi.Source, pi.Period);

                ds = sa.ExecuteQuery(sql);

                if (ds.Tables[0].Rows.Count < 1)
                {
                    retVal = "000001";
                }
                else
                {
                    // We have a value for max(ref)
                    int numericValue = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                    numericValue++;

                    if (numericValue <= 99)
                    {
                        retVal = numericValue.ToString().PadLeft(6, '0');
                    }
                    else
                    {
                        int x;

                        for (x = 1; x < 99; x++)
                        {
                            sql = string.Format("select ref from (select ref from journal where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%'", pi.Source, pi.Period) +
                            string.Format("union select ref from ghis where source = '{0}' and period = '{1}' and ref not like '%[^0-9]%') as refunion WHERE REF = '0000{2}'", pi.Source, pi.Period, x.ToString().PadLeft(2, '0'));

                            ds = sa.ExecuteQuery(sql);

                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                retVal = x.ToString().PadLeft(6, '0');
                                break;
                            }
                        }

                        if (x >= 99)
                        {
                            pi.ValidationMessages.Add(string.Format("There are too many references created for this period. " +
                                " A reference number cannot be 99 or greater. The next one will be {0}.", numericValue));
                            pi.ErrorHasOccurred = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
                //                ex.Message = string.Format("Error ocurred: {0}", ex.Message);
            }


            return retVal;
        }



        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // The validation has discovered NO ERRORS and
            // the user has elected to update the MRI Journal table.
            // Perform the SQL INSERT here
            if (!pi.ErrorHasOccurred)
            {
                try
                {
                    // Then we commit all of the above operations
                    sa.CommitTransaction();

                    if (pi.Reversing.ToUpper() != "Y")
                    {
                        FileInfo fi = new FileInfo(pi.ExcelFileName.Trim());
                        fi.MoveTo(fi.DirectoryName.Trim() + "\\" + fi.Name.Trim().Replace(fi.Extension.Trim(), "") + pi.Source + RefNum.Trim() + fi.Extension.Trim());
                    }
                    else
                    {
                        FileInfo fi = new FileInfo(pi.ExcelFileName.Trim());
                        fi.MoveTo(fi.DirectoryName.Trim() + "\\" + fi.Name.Trim().Replace(fi.Extension.Trim(), "") + pi.Source + 
                                  pi.ReferenceNumber.Trim().Substring(0,6) + pi.Source + pi.ReferenceNumber.Trim().Substring(6) + fi.Extension.Trim());
                    }

                    //UpdateSpreadsheet(pi);
                    Cursor = Cursors.Default;
                    string msg = "";
                    foreach (string s in pi.CompletionMessages)
                    {
                        msg += s + "\n";
                    }
                    MessageBox.Show(msg);

                    System.Windows.Forms.Application.Exit();

                    HelpfulFunctions.LogMessage($@"Reference number {RefNum.Trim()} is successfully committed.", "Application", EventLogEntryType.Information);

                    // Display the summary screen
                    // frmSummary fs = new frmSummary(pi);
                    // fs.ShowDialog();
                    //                        Close();
                    //                        ucValidate.DialogResult = DialogResult.OK;

                }
                catch (Exception err)
                {
                    // If any of the SQL operations above fail for any reason,
                    // Rollback all operations that have taken place.
                    sa.RollbackTransaction();
                    HelpfulFunctions.LogMessage($@"Reference number {RefNum.Trim()} was not committed. The batch was rolled back due to error.
                                                   {err.Message.Trim()} {err.StackTrace.Trim()}", "Application", EventLogEntryType.Error);

                    Cursor = Cursors.Default;
                    MessageBox.Show(string.Format("An error occurred while adding Journal Entries: {0}.  All database operations have been rolled back.", err.Message.Trim()));
                }
            }

        }


        private string GetInsertForGLBNCTL()
        {
            string retVal = string.Format("insert into hs_glbnctl (glsource, glref, period, basis, batchamt, release, sent, descr, userid, NonAS400Amt) VALUES " +
                "( '{0}', '{1}', '{2}', '{3}', {4}, '{5}', 'N', '{6}', '{7}', {8})",
                pi.Source, pi.ReferenceNumber, pi.Period, pi.Basis, pi.GrandTotalDebit, pi.Release, pi.Description, pi.UserId, pi.NonAs400Total);

            return retVal;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            string filename = "errorExport.txt";

            if (File.Exists(filename))
                File.Delete(filename);

            for (int i = 0; i < txtErrorMessages.Lines.Length; i++)
            {
                File.AppendAllText(filename, txtErrorMessages.Lines[i].ToString() + "\n");
            }

            Process.Start("notepad.exe", filename);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
