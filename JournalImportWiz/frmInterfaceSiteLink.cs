﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using HelpfulClasses;
using JournalImportWiz;
using SqlDataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net.Mail;
using System.Security.Principal;
using System.Windows.Forms;

namespace JournalImport
{
    public partial class frmInterfaceSiteLink : Form
    {
        private string DatabaseName = "";
        /// <summary>
        /// This is the iOrder Number for the Gross Potential Perfect from Income Analysis data on SiteLink.
        /// </summary>
        private const int IOrder_GrossPotentialPerfect = 0;
        /// This is the iOrder Number for the Gross Vacant from Income Analysis data on SiteLink.
        /// </summary>
        private const int IOrder_GrossVacant = 4;
        /// <summary>
        /// This is the iOrder Number for the Rental Income from the Income Analysis data on SiteLink.
        /// </summary>
        private const int IOrder_RentalIncome = 10;
        /// <summary>
        /// This is the iOrder Number for the Move In Discount from the Income Analysis data on SiteLink.
        /// </summary>
        private const int IOrder_MoveInDiscount = 21;
        /// <summary>
        /// Use to store a string value of a SQL command.
        /// </summary>
        private string strSQL = "";
        /// <summary>
        /// Use to the store any data sets that need to used in the program.
        /// </summary>
        private Dictionary<string, DataTable> siteLink = new Dictionary<string, DataTable>();
        /// <summary>
        /// The current period's year.
        /// </summary>
        private int year = 0;
        /// <summary>
        /// The current period's month.
        /// </summary>
        private int month = 0;
        /// <summary>
        /// Use to temporarily store data return from a SQL query.
        /// </summary>
        DataSet sqlDataSet;
        /// <summary>
        /// The object that interfaces any SQL queries or commands.
        /// </summary>
        SqlAgent sqlAgent = new SqlAgent(ConfigurationManager.AppSettings["dbName"]);
        /// <summary>
        /// A list of property id's that data is being collected for.
        /// </summary>
        bool HasErrors = false;
        DataTable locTblAccounts = new DataTable();
        DataTable locTblEntities = new DataTable();
        JournalEntries genJrnEntries = new JournalEntries();
        private const string correctingDepositEmail = @"<html>
                                                        <body>
                                                            <table style='text-align:center;'>
                                                                <tr>
                                                                    <td>
                                                                        <h1>Correcting Deposits</h1>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Below are the <br /> correcting deposits.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <ol>
                                                                            {0}
                                                                        </ol>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </body>
                                                        </html>";
        private const string correctingDepositLineItem = @"<li style='text-align:left;'>{0}</li>";
        private const string depositAccount = "HS103010533776";
        private string body = "";
        const string CONNECTION_STRING = @"data source=HMFINSQL;initial catalog={0};Integrated Security=true;";

        public frmInterfaceSiteLink()
        {
            InitializeComponent();
        }

        private void frmInterfaceSiteLink_Load(object sender, EventArgs e)
        {
            try
            {
                DatabaseName = ConfigurationManager.AppSettings["dbName"];
                this.Text = this.Text + " " + DatabaseName.Trim();

                CreateDropdowns();

                cboPeriod.SelectedIndexChanged += cboPeriod_SelectedIndexChanged;

                strSQL = "SELECT * FROM GACC";

                locTblAccounts = sqlAgent.ExecuteQuery(strSQL).Tables[0];

                //                strSQL = "SELECT * FROM ENTITY";
                // Type 10 = mini storage in BLDG means it is managed by Peak
                strSQL = "select e.* from BLDG b join entity e on b.entityid = e.entityid where e.ENTTYPE = '10'  order by e.entityid";

                locTblEntities = sqlAgent.ExecuteQuery(strSQL).Tables[0];
            }
            catch (NullReferenceException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (SqlException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboPeriod.Text.Trim() != "")
                {
                    string[] period = cboPeriod.Text.Split('/');

                    month = int.Parse(period[0]);
                    year = int.Parse(period[1]);

                    GetData(year, month);

                    if (dgvGeneralJournalEntries.Rows.Count > 0)
                    {
                        btnPostToJournal.Enabled = true;
                    }
                    else
                    {
                        btnPostToJournal.Enabled = false;
                    }
                }
            }
            catch (NullReferenceException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (OverflowException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (FormatException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (ArgumentNullException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Creates the <see cref="JournalBatch"/> object. It parses a <see cref="DataSet"/> or a collection of
        /// <see cref="DataTable"/> objects the represent a workbook sheet.
        /// </summary>
        /// <param name="ds">The <see cref="DataSet"/> to parse through.</param>
        /// <param name="nameBatch">The name of the batch.</param>
        /// <param name="TransactionDate">The Transaction Date of the Batch. It can be parsed from a 
        /// specific cell in a worksheet. This set based on a template from Accounting.</param>
        /// <returns></returns>
        protected XLWorkbook CreateJournalBatch(DataSet ds, string nameBatch, DateTime TransactionDate)
        {
            List<JournalSheet> sheets = new List<JournalSheet>();
            int i = 1;
            //go through each row in the first data table, it is the meta table that defines the subsequent table in 
            //the data set
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //if the table is after the meta table
                if (ds.Tables[i].Rows.Count > 0)
                {
                    JournalSheet sheet = new JournalSheet();
                    sheet.WorkSheetName = dr["worksheetname"].ToString().Trim();
                    //check if the worksheet is set to be not be skipped
                    if (dr["skipflag"].ToString().Trim() == "N")
                    {//define the worksheet header information
                        sheet.JeVoNo = int.Parse(dr["jeVoNo"].ToString().Trim());
                        sheet.TransactionDate = TransactionDate;
                        sheet.ReversalFlag = dr["reversal"].ToString().Trim() == "N" ? false : true;
                        sheet.Ref = dr["ref"].ToString().Trim();
                        sheet.Batch = dr["batch"].ToString().Trim();
                        sheet.workSheetType = JournalSheet.WorkSheetType.POSTING;
                    }
                    else
                    {
                        sheet.workSheetType = JournalSheet.WorkSheetType.SKIP;
                    }
                    //set the Excel worksheet rows and columns
                    sheet.Data = ds.Tables[i];
                    //add the worksheet to the workbook
                    sheets.Add(sheet);
                }

                i++;
            }

            //removes the meta table from the data set, it is not needed anymore
            ds.Tables.RemoveAt(0);

            //setup the workbook and set name from the provided parameter
            XLWorkbook wb = new XLWorkbook();
            wb.Properties.Title = nameBatch;

            //go through each worksheet and format the based on the accounting template
            foreach (JournalSheet sheet in sheets)
            {
                CreateJournalBatchPage(wb, sheet);
            }

            return wb;
        }

        /// <summary>
        /// Formats a worksheet in a workbook. If the worksheet is not set to be skipped, it will format it according
        /// to the accounting Journal Entry Batch tamplate.
        /// </summary>
        /// <param name="wb"></param>
        /// <param name="sheet"></param>
        private void CreateJournalBatchPage(XLWorkbook wb, JournalSheet sheet)
        {
            string workSheetName = "";
            workSheetName = sheet.WorkSheetName.Trim().Replace("SKIP_", "");
            bool skipSheet = sheet.workSheetType == JournalSheet.WorkSheetType.SKIP ? true : false;
            IXLWorksheet worksheet = wb.AddWorksheet(workSheetName);
            int y = 1;

            //if the sheet is set as skip, it will be formatted according to the accounting template
            if (!skipSheet)
            {
                worksheet.Cell("A1").Value = "Prepared By:";
                worksheet.Cell("A1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("A1").Style.Font.Bold = true;
                worksheet.Cell("B1").Value = Program.userID.ToString().Trim();
                worksheet.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("B1").Style.Font.Bold = true;
                worksheet.Cell("A2").Value = "Approved By:";
                worksheet.Cell("A2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("A2").Style.Font.Bold = true;
                worksheet.Cell("B2").Value = Program.userID.ToString().Trim();
                worksheet.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("B2").Style.Font.Bold = true;
                worksheet.Cell("A3").Value = "Date:";
                worksheet.Cell("A3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("A3").Style.Font.Bold = true;
                worksheet.Cell("B3").Value = DateTime.Now.ToString("MM/dd/yyyy");
                worksheet.Cell("B3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("B3").Style.Font.Bold = true;
                worksheet.Cell("A4").Value = "JE";
                worksheet.Cell("M7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("A4").Style.Font.Bold = true;
                worksheet.Cell("B4").Value = sheet.JeVoNo;
                worksheet.Cell("B4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("B4").Style.Font.Bold = true;
                worksheet.Cell("D1").Value = "Trans Date";
                worksheet.Cell("D1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("D1").Style.Font.Bold = true;
                worksheet.Cell("F1").Value = sheet.TransactionDate.ToString("MM/dd/yyyy");
                worksheet.Cell("F1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("F1").Style.Font.Bold = true;
                worksheet.Cell("D2").Value = "Debit Total";
                worksheet.Cell("D2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("D2").Style.Font.Bold = true;
                worksheet.Cell("F2").FormulaA1 = "=SUMIF(G8:G10000,\"=Z\",E8:E10000)";
                worksheet.Cell("F2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("F2").Style.Font.Bold = true;
                worksheet.Cell("D3").Value = "Credit Total";
                worksheet.Cell("D3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("D3").Style.Font.Bold = true;
                worksheet.Cell("F3").FormulaA1 = "=SUMIF(G8:G10000,\"=Z\",F8:F10000)";
                worksheet.Cell("F3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("F3").Style.Font.Bold = true;
                worksheet.Cell("I1").Value = sheet.ReversalFlag ? "Y" : "N";
                worksheet.Cell("T1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("I1").Style.Font.Bold = true;
                worksheet.Cell("U1").Value = "Ref";
                worksheet.Cell("U1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("U1").Style.Font.Bold = true;
                worksheet.Cell("V1").Value = sheet.Ref.Trim();
                worksheet.Cell("V1").Style.Font.Bold = true;
                worksheet.Cell("U2").Value = "Batch";
                worksheet.Cell("U2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                worksheet.Cell("U2").Style.Font.Bold = true;
                worksheet.Cell("V2").Value = sheet.Batch.Trim();
                worksheet.Cell("V2").Style.Font.Bold = true;
                worksheet.Range("B6:D6").Merge().Value = "ACCOUNT NUMBER";
                worksheet.Range("B6:D6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Range("B6:D6").Style.Font.Bold = true;
                worksheet.Range("E6:F6").Merge().Value = "TRANSACTION AMOUNT";
                worksheet.Range("E6:F6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Range("E6:F6").Style.Font.Bold = true;
                worksheet.Cell("H6").Value = "SUPPLEMENTARY INFORMATION";
                worksheet.Cell("H6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("H6").Style.Font.Bold = true;
                worksheet.Cell("I6").Value = "EXTRA";
                worksheet.Cell("I6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("I6").Style.Font.Bold = true;
                worksheet.Cell("I7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Range("K4:N4").Merge().Value = "Z CODE - CHECK # OF CHARS";
                worksheet.Range("K4:N4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Range("K4:N4").Style.Font.Bold = true;
                worksheet.Range("K5:N5").Merge().Value = "MUST BE 37 OR LESS";
                worksheet.Range("K5:N5").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Range("K5:N5").Style.Font.Bold = true;
                worksheet.Range("K6:N6").Merge().Value = "CHECK IT; BUT DO NOT PRINT";
                worksheet.Range("K6:N6").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Range("K6:N6").Style.Font.Bold = true;
                worksheet.Cell("A7").Value = "ACCOUNT DESCRIPTION";
                worksheet.Cell("A7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("A7").Style.Font.Bold = true;
                worksheet.Cell("A7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("B7").Value = "PROJ";
                worksheet.Cell("B7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("B7").Style.Font.Bold = true;
                worksheet.Cell("B7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("C7").Value = "DIV";
                worksheet.Cell("C7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("C7").Style.Font.Bold = true;
                worksheet.Cell("C7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("D7").Value = "CLASS";
                worksheet.Cell("D7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("D7").Style.Font.Bold = true;
                worksheet.Cell("D7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("E7").Value = "DEBIT";
                worksheet.Cell("E7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("E7").Style.Font.Bold = true;
                worksheet.Cell("E7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("F7").Value = "CREDIT";
                worksheet.Cell("F7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("F7").Style.Font.Bold = true;
                worksheet.Cell("F7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("G7").Value = "CD";
                worksheet.Cell("G7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("G7").Style.Font.Bold = true;
                worksheet.Cell("G7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("H7").Value = "DESCRIPTION";
                worksheet.Cell("H7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("H7").Style.Font.Bold = true;
                worksheet.Cell("H7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("K7").Value = "COL H";
                worksheet.Cell("K7").Style.Font.Bold = true;
                worksheet.Cell("K7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("K7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("L7").Value = "COL I";
                worksheet.Cell("L7").Style.Font.Bold = true;
                worksheet.Cell("L7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("L7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("M7").Value = "TOTAL";
                worksheet.Cell("M7").Style.Font.Bold = true;
                worksheet.Cell("M7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell("M7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("N7").Value = "IF > 37";
                worksheet.Cell("N7").Style.Font.Bold = true;
                worksheet.Cell("N7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("N7").Style.Border.SetBottomBorder(XLBorderStyleValues.Thick);
                worksheet.Range("P5:S5").Merge().Value = "You must include ALL columns or NO columns for Job Costs";
                worksheet.Range("P5:S5").Style.Font.FontColor = XLColor.Red;
                worksheet.Range("P5:S5").Style.Font.Bold = true;
                worksheet.Range("P5:S5").Style.Font.Italic = true;
                worksheet.Range("P5:S5").Style.Font.FontSize = 9;
                worksheet.Columns("O:T").Hide();
                worksheet.Range("P6:S6").Merge().Value = "Incomplete Job Cost information will prevent importing spreadsheet";
                worksheet.Range("P6:S6").Style.Font.FontColor = XLColor.Red;
                worksheet.Range("P6:S6").Style.Font.Bold = true;
                worksheet.Range("P6:S6").Style.Font.Italic = true;
                worksheet.Range("P6:S6").Style.Font.FontSize = 9;
                worksheet.Cell("P7").Value = "Job Code";
                worksheet.Cell("P7").Style.Font.Bold = true;
                worksheet.Cell("P7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("Q7").Value = "Phase Code";
                worksheet.Cell("Q7").Style.Font.Bold = true;
                worksheet.Cell("Q7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("R7").Value = "Cost List";
                worksheet.Cell("R7").Style.Font.Bold = true;
                worksheet.Cell("R7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("S7").Value = "Cost Code";
                worksheet.Cell("S7").Style.Font.Bold = true;
                worksheet.Cell("S7").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                worksheet.Cell("A8").Value = null;
                worksheet.Cell("A8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("B8").Value = null;
                worksheet.Cell("B8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("C8").Value = null;
                worksheet.Cell("C8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("D8").Value = null;
                worksheet.Cell("D8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("E8").Value = null;
                worksheet.Cell("E8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("F8").Value = null;
                worksheet.Cell("F8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("G8").Value = null;
                worksheet.Cell("G8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("H8").Value = null;
                worksheet.Cell("H8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("I8").Value = null;
                worksheet.Cell("I8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("K8").Value = null;
                worksheet.Cell("K8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("L8").Value = null;
                worksheet.Cell("L8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("M8").Value = null;
                worksheet.Cell("M8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);
                worksheet.Cell("N8").Value = null;
                worksheet.Cell("N8").Style.Border.SetTopBorder(XLBorderStyleValues.Thick);

                //start Journal Batch lines at line 8 in the worksheet
                y = 8;

                foreach (DataRow row in sheet.Data.Rows)
                {//start the column at the secord column because first column is account description and is only used when
                 //batch is manually created
                    for (int x = 2; x < row.Table.Columns.Count + 2; x++)
                    {//prior to the job cost columns
                        if (x < 9)
                        {
                            worksheet.Cell(y, x).Value = row[x - 2];
                        }//the job cost columns
                        else
                        {
                            worksheet.Cell(y, x + 5).Value = row[x - 2];
                        }
                    }

                    y++;
                }
            }
            else
            {//the worksheet is set to skip
                //put skip in the proper cell
                worksheet.Cell("F1").Value = "SKIP";
                //go to the second row
                y = 2;
                int x = 1;

                //go through each column in the data table and write the header text in same cell postion
                //that it is in the data table
                foreach (DataColumn column in sheet.Data.Columns)
                {
                    worksheet.Cell(y, x).Value = column.ColumnName.Trim();

                    x++;
                }

                //move to next row
                y++;

                //write each row in the data table in next row below the columns and match the columns
                //in the header row
                foreach (DataRow row in sheet.Data.Rows)
                {
                    for (x = 1; x < row.Table.Columns.Count + 1; x++)
                    {
                        worksheet.Cell(y, x).Value = row[x - 1];
                    }

                    y++;
                }
            }

            //after the data is written, resize the column to match the size of the longest string in each column
            foreach (IXLColumn column in worksheet.Columns())
            {
                column.AdjustToContents();
            }
        }

        /// <summary>
        /// Formats the header row for the General Journal Entry grid and the Income Analysis grid. It changes the header text to a readible header and the hides unecessary columns.
        /// </summary>
        private void FormatHeader()
        {
            try
            {
                //change the General Journal Entries columns
                dgvGeneralJournalEntries.Columns["ENTITYID"].HeaderText = "Entity ID";
                dgvGeneralJournalEntries.Columns["ACCTNAME"].HeaderText = "Account Name";
                dgvGeneralJournalEntries.Columns["ACCTNUM"].HeaderText = "Account Number";
                dgvGeneralJournalEntries.Columns["CREDIT"].HeaderText = "Credit";
                dgvGeneralJournalEntries.Columns["DEBIT"].HeaderText = "Debit";
                dgvGeneralJournalEntries.Columns["DESCRIPTION"].HeaderText = "Description";
            }
            catch (NullReferenceException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Creates the data in the Drop Down's on the form.
        /// </summary>
        private void CreateDropdowns()
        {
            try
            {
                //clear any existing items
                cboPeriod.Items.Clear();

                //recreate the items in the drop down
                for (int i = 0; i < 50; i++)
                {
                    DateTime curDate = DateTime.Now.AddMonths(-i);
                    int year = curDate.Year;
                    int month = curDate.Month;

                    cboPeriod.Items.Add(month.ToString().Trim().PadLeft(2, '0') + "/" + year.ToString().Trim());
                }

                cboPeriod.Items.Insert(0, "");
            }
            catch (NullReferenceException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (ArgumentNullException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Gets the data to display in the form's grids.
        /// </summary>
        /// <param name="year">This the period's year for which to get the data.</param>
        /// <param name="month">This is the period's month for which to get the data.</param>
        private void GetData(int year, int month)
        {
            try
            {
                this.Wait();

                decimal credit = 0;
                decimal debit = 0;
                DataTable locTblLedger = new DataTable();
                locTblLedger.Columns.Add("ENTITYID");
                locTblLedger.Columns.Add("ACCTNAME");
                locTblLedger.Columns.Add("ACCTNUM");
                locTblLedger.Columns.Add("DEBIT");
                locTblLedger.Columns.Add("CREDIT");
                locTblLedger.Columns.Add("DESCRIPTION");
                locTblLedger.Columns.Add("TRUEENTITY");

                //clear the data in the grids and the dictionary object
                siteLink.Clear();
                dgvGeneralJournalEntries.DataSource = null;
                genJrnEntries.Clear();

                //get the General Journal Entries
                strSQL = "SELECT DISTINCT RTRIM(SITEID) as SITEID, RTRIM(DSTRT) as DSTRT, RTRIM(DEND) as DEND, RTRIM(DESCRIPTN) as DESCRIPTN, RTRIM(ACCTNAME) as ACCTNAME, " +
                         " RTRIM(SUBACCTNM) as SUBACCTNM, RTRIM(ACCTTYPNM) as ACCTTYPNM, RTRIM(DEBIT) as DEBIT, RTRIM(CREDIT) as CREDIT, RTRIM(SDEFACCTTNM) as SDEFACCTTNM, " +
                         " RTRIM(TYPEID) as TYPEID, RTRIM(SDEFACCTCD) as SDEFACCTCD, RTRIM(ACCTCODE) as ACCTCODE FROM HS_SLGENJOURN" +
                         " WHERE YEAR(DEND) = " + year + " AND MONTH(DEND) = " + month +
                         " ORDER BY SITEID ASC, ACCTCODE";

                sqlDataSet = sqlAgent.ExecuteQuery(strSQL);

                siteLink.Add("General Journal", sqlDataSet.Tables[0]);

                //Get the the Income Analysis
                strSQL = "SELECT DISTINCT RTRIM(SITEID) as SITEID, cast(IORDER as int) as IORDER, RTRIM(SDESC) as SDESC, RTRIM(DCSUBCOL) as DCSUBCOL, RTRIM(DCMAINCOL) as DCMAINCOL, " +
                         " RTRIM(DCPERCENT) as DCPERCENT, RTRIM(DCPCTTOT) as DCPCTTOT, RTRIM(DSTRT) as DSTRT, " +
                         " RTRIM(DEND) as DEND FROM HS_SLINCANAL" +
                         " WHERE YEAR(DEND) = " + year + " AND MONTH(DEND) = " + month +
                         " ORDER BY SITEID ASC, cast(IORDER as int) ASC";

                sqlDataSet = sqlAgent.ExecuteQuery(strSQL);

                siteLink.Add("Income Analysis", sqlDataSet.Tables[0]);

                JournalEntry genJrnEntry;
                DataSet locTbl = new DataSet();
                IncomeAnalysis locationIncome = new IncomeAnalysis("");
                List<IncomeAnalysis> locationIncomes = new List<IncomeAnalysis>();
                string siteID = "";

                //cycles through the locations' General Journal Entries and Income Analysis

                foreach(DataRow entRow in locTblEntities.Rows )
                {
                    string entityID = entRow[0].ToString();
                    //cycle through the General Journal Entries
                    foreach (DataRow dr in siteLink["General Journal"].Rows)
                    {
                        if (dr["SITEID"].ToString().Trim().PadLeft(3, '0') == entityID.Trim())
                        {
                            string accountName = "";

                            if (dr["SUBACCTNM"].ToString().Trim() != "")
                            {
                                accountName = dr["SUBACCTNM"].ToString().Trim().Replace(" ", "");
                            }
                            else
                            {
                                accountName = dr["ACCTNAME"].ToString().Trim();
                            }

                            if (decimal.Parse(dr["DEBIT"].ToString().Trim()) > 0M)
                            {
                                genJrnEntry = new JournalEntry();

                                genJrnEntry.EntityID = dr["SITEID"].ToString().Trim().PadLeft(3, '0');
                                genJrnEntry.Description = accountName;
                                genJrnEntry.AccountNumber = "HS" + dr["ACCTCODE"].ToString().Trim() + "000000";
                                genJrnEntry.Amount = decimal.Parse(dr["DEBIT"].ToString().Trim());
                                genJrnEntry.Period = DateTime.Parse(dr["DEND"].ToString().Trim()).ToString("yyyyMM");
                                genJrnEntry.trueEntity = dr["SITEID"].ToString().Trim().PadLeft(3, '0');

                                genJrnEntries.Add(genJrnEntry);
                            }
                            if (decimal.Parse(dr["CREDIT"].ToString().Trim()) > 0M)
                            {
                                genJrnEntry = new JournalEntry();

                                genJrnEntry.EntityID = dr["SITEID"].ToString().Trim().PadLeft(3, '0');
                                genJrnEntry.Description = accountName;
                                genJrnEntry.AccountNumber = "HS" + dr["ACCTCODE"].ToString().Trim() + "000000";
                                genJrnEntry.Amount = -1 * decimal.Parse(dr["CREDIT"].ToString().Trim());
                                genJrnEntry.Period = DateTime.Parse(dr["DEND"].ToString().Trim()).ToString("yyyyMM");
                                genJrnEntry.trueEntity = dr["SITEID"].ToString().Trim().PadLeft(3, '0');

                                genJrnEntries.Add(genJrnEntry);
                            }
                        }
                    }

                    //cycle through the Income Analysis
                    foreach (DataRow dr in siteLink["Income Analysis"].Rows)
                    {
                        if (dr["SITEID"].ToString().Trim().PadLeft(3, '0') == entityID.Trim())
                        {
                            if (dr["SiteID"].ToString().Trim().PadLeft(3, '0') != siteID)
                            {
                                siteID = dr["SiteID"].ToString().Trim().PadLeft(3, '0');
                                locationIncome = new IncomeAnalysis(siteID);
                            }

                            if ((int.Parse(dr["iOrder"].ToString().Trim()) == IOrder_GrossPotentialPerfect) & (decimal.Parse(dr["dcMainCol"].ToString().Trim()) != 0M))
                            {  // 0
                                locationIncome.GrossPotentialPerfect = decimal.Parse(dr["dcMainCol"].ToString().Trim());
                            }
                            if ((int.Parse(dr["iOrder"].ToString().Trim()) == IOrder_GrossVacant) & (decimal.Parse(dr["dcMainCol"].ToString().Trim()) != 0M))
                            {
                                locationIncome.GrossVacant = decimal.Parse(dr["dcMainCol"].ToString().Trim());
                            }
                            if ((int.Parse(dr["iOrder"].ToString().Trim()) == IOrder_RentalIncome) & (decimal.Parse(dr["dcMainCol"].ToString().Trim()) != 0M))
                            {
                                locationIncome.RentalIncome = decimal.Parse(dr["dcMainCol"].ToString().Trim());
                            }
                            if ((int.Parse(dr["iOrder"].ToString().Trim()) == IOrder_MoveInDiscount) & (decimal.Parse(dr["dcSubCol"].ToString().Trim()) != 0M))
                            {
                                locationIncome.MoveInDiscount = decimal.Parse(dr["dcSubCol"].ToString().Trim());
                            }
                            if (int.Parse(dr["iOrder"].ToString().Trim()) == 22 & decimal.Parse(dr["dcSubCol"].ToString().Trim()) != 0)
                            {
                                locationIncome.Discounts = decimal.Parse(dr["dcSubCol"].ToString().Trim());
                            }
                            if (int.Parse(dr["iOrder"].ToString().Trim()) == 24)
                            {
//                              locationIncome.EffectiveVacancy = locationIncome.GrossPotentialPerfect - locationIncome.RentalIncome - locationIncome.MoveInDiscount;
                                locationIncome.EffectiveVacancy = locationIncome.GrossVacant - locationIncome.MoveInDiscount;
                                genJrnEntry = new JournalEntry();

                                genJrnEntry.EntityID = siteID;
                                genJrnEntry.Description = "AJE VL";
                                genJrnEntry.AccountNumber = "HS521000000000";
                                genJrnEntry.Amount = locationIncome.EffectiveVacancy;
                                genJrnEntry.Period = DateTime.Parse(dr["DEND"].ToString().Trim()).ToString("yyyyMM");
                                genJrnEntry.trueEntity = siteID;
                                genJrnEntry.ProgramGenerated = true;

                                genJrnEntries.Add(genJrnEntry);

                                genJrnEntry = new JournalEntry();

                                genJrnEntry.EntityID = siteID;
                                genJrnEntry.Description = "AJE VL";
                                genJrnEntry.AccountNumber = "HS511000000000";
                                genJrnEntry.Amount = -1 * locationIncome.EffectiveVacancy;
                                genJrnEntry.Period = DateTime.Parse(dr["DEND"].ToString().Trim()).ToString("yyyyMM");
                                genJrnEntry.trueEntity = siteID;
                                genJrnEntry.ProgramGenerated = true;

                                genJrnEntries.Add(genJrnEntry);
                                locationIncomes.Add(locationIncome);
                            }
                        }
                    }

                    decimal nsf = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                    where genJrn.EntityID == entityID.Trim() & 
                                    (genJrn.Description.Trim() == "Cash" | 
                                    genJrn.Description.Trim() == "Checks" | 
                                    genJrn.Description.Trim() == "MoneyOrder") & genJrn.Amount < 0
                                    select genJrn.Amount).Sum();

                    if(nsf < 0)
                    {
                        genJrnEntry = new JournalEntry();

                        genJrnEntry.ProgramGenerated = true;
                        genJrnEntry.EntityID = "551";
                        genJrnEntry.Description = $"NSF Total {entityID.Trim()}";
                        genJrnEntry.AccountNumber = depositAccount;
                        genJrnEntry.Amount = nsf;
                        genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                        genJrnEntry.trueEntity = entityID.Trim();

                        genJrnEntries.Add(genJrnEntry);

                        genJrnEntry = new JournalEntry();

                        genJrnEntry.ProgramGenerated = true;
                        genJrnEntry.EntityID = "551";
                        genJrnEntry.Description = $"NSF - {entityID.Trim()}";
                        genJrnEntry.AccountNumber = GetInterCompanyAccount(entityID.Trim());
                        genJrnEntry.Amount = -nsf;
                        genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                        genJrnEntry.trueEntity = entityID.Trim();

                        genJrnEntries.Add(genJrnEntry);
                    }
                }
                //cycle through each Journal Entry
                foreach (JournalEntry genJrn in genJrnEntries.GetItems())
                {
                    decimal rentalIncome = 0M;
                    try
                    {
                        rentalIncome = locationIncomes.Where(x => x.SiteID == genJrn.EntityID.PadLeft(3, '0')).Single().RentalIncome;
                    }
                    catch(ArgumentException err)
                    {
                        
                    }
                    catch (InvalidOperationException err)
                    {

                    }
                    decimal moveInDiscount = 0M;
                    try
                    {
                        moveInDiscount = locationIncomes.Where(x => x.SiteID == genJrn.EntityID.PadLeft(3, '0')).Single().MoveInDiscount;
                    }
                    catch(ArgumentException err)
                    {

                    }
                    catch(InvalidOperationException err)
                    {

                    }

                    //if the criteria is met, it updates the existing Journal Entry

                    if ((genJrn.AccountNumber.ToString().Trim() == "HS511000000000") & (genJrn.Amount < 0M) &
                        ( genJrn.ProgramGenerated ==  false) &
                        (genJrn.Amount < -(rentalIncome +moveInDiscount) ))
                    { 
                        decimal amount = -(rentalIncome + moveInDiscount);
                        genJrn.Amount = amount;
                    }
                    if ((genJrn.AccountNumber.ToString().Trim() == "HS527100000000"))
                        if ((genJrn.Amount > 0M) )
                    {
                        decimal amount = moveInDiscount;
                        genJrn.Amount = amount;
                    }
                }
                // old 
                //get the Journal Entries for current period and within the account number range
                strSQL = $@"SELECT period, '970' REF, 'GJ' Source, '551'EntityId, ovrentity, ACCTNUM, SUM(Payment) AMT, SUM(OVRAMT - Payment) as OVRAMT, CONCAT('Dep # ',depositid, ' - Entity ',entityid, FORMAT(depositdate, 'MM/dd/yyyy'), ' ', BANKREF) DESCRPTN, s.entityid as property, notdeposited 
from                 (SELECT dbo.udf_Base_ConvertDateToMRIPeriod(r.RECEIPTDAT) period, b.depositid, actdate depositdate, b.ENTITYID as ovrentity, 
                            (pmt_cash + pmt_check) as Payment, OVRAMT, b.ENTITYID, t.ACCTNUM, BANKREF, coalesce(notdeposited,0) notdeposited FROM HS_BANKDEPLINE l JOIN HS_BANKDEP b ON L.DEPOSITID = b.DEPOSITID 
                            JOIN HS_SLRECEIPTS r ON L.RECEIPTNO = r.IRECEIPTNU AND CONVERT(INT, b.entityid) = CONVERT(INT, r.siteid)
                            JOIN bmap t ON T.entityid = b.ENTITYID   AND T.cashtype = b.cashtype
                            Join entity e on b.entityid = e.entityid
                            WHERE e.ENTTYPE = '10' and dbo.udf_Base_ConvertDateToMRIPeriod(r.RECEIPTDAT) = '{year.ToString() + month.ToString().PadLeft(2, '0')}' and pmt_check+ r.PMT_CASH  != 0 ) s
                            GROUP BY PERIOD, depositid, depositdate, entityid, ovrentity, s.ACCTNUM, BANKREF, notdeposited
                            ORDER BY s.depositdate ASC, sum(Payment) desc";
                 locTbl = sqlAgent.ExecuteQuery(strSQL);

                foreach (DataRow dr in locTbl.Tables[0].Rows)
                {
                    if (dr["NotDeposited"].ToString() == "0")
                    {
                        //add the transaction for the inter-company Journal Entry
                        genJrnEntry = new JournalEntry();
                        genJrnEntry.EntityID = "551";
                        genJrnEntry.Description = dr["DESCRPTN"].ToString().Trim();
                        genJrnEntry.AccountNumber = GetInterCompanyAccount(dr["property"].ToString().Trim());
                        genJrnEntry.Amount = -1 * decimal.Parse(dr["AMT"].ToString().Trim());
                        genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                        genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                        genJrnEntries.Add(genJrnEntry);

                        if (decimal.Parse(dr["AMT"].ToString().Trim()) > 0M)
                        {
                            genJrnEntry = new JournalEntry();

                            genJrnEntry.EntityID = "551";
                            genJrnEntry.Description = dr["DESCRPTN"].ToString().Trim();
                            genJrnEntry.AccountNumber = depositAccount;
                            genJrnEntry.Amount = decimal.Parse(dr["AMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);
                        }
                        else
                        {
                            genJrnEntry = new JournalEntry();

                            genJrnEntry.EntityID = "551";
                            genJrnEntry.Description = dr["DESCRPTN"].ToString().Trim();
                            genJrnEntry.AccountNumber = depositAccount;
                            genJrnEntry.Amount = decimal.Parse(dr["AMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);
                        }



                        if (dr["OVRAMT"] != DBNull.Value && (decimal.Parse(dr["OVRAMT"].ToString().Trim()) > 0M | decimal.Parse(dr["OVRAMT"].ToString().Trim()) < 0M))
                        {
                            genJrnEntry = new JournalEntry();

                            genJrnEntry.EntityID = dr["OVRENTITY"].ToString().Trim();
                            genJrnEntry.Description = string.Format("{0} Correction", dr["DESCRPTN"].ToString().Trim());
                            genJrnEntry.AccountNumber = "HS599000000000";
                            genJrnEntry.Amount = -1 * decimal.Parse(dr["OVRAMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);

                            genJrnEntry = new JournalEntry();

                            genJrnEntry.EntityID = "551";
                            genJrnEntry.Description = string.Format("{0} Correction", dr["DESCRPTN"].ToString().Trim());
                            genJrnEntry.AccountNumber = depositAccount;
                            genJrnEntry.Amount = decimal.Parse(dr["OVRAMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);

                            genJrnEntry = new JournalEntry();
                            genJrnEntry.EntityID = dr["OVRENTITY"].ToString().Trim();
                            genJrnEntry.Description = string.Format("{0} Correction", dr["DESCRPTN"].ToString().Trim());
                            genJrnEntry.AccountNumber = GetInterCompanyAccount(dr["OVRENTITY"].ToString().Trim());
                            genJrnEntry.Amount = decimal.Parse(dr["OVRAMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);

                            genJrnEntry = new JournalEntry();

                            genJrnEntry.EntityID = "551";
                            genJrnEntry.Description = string.Format("{0} Correction", dr["DESCRPTN"].ToString().Trim());
                            genJrnEntry.AccountNumber = GetInterCompanyAccount(dr["OVRENTITY"].ToString().Trim());
                            genJrnEntry.Amount = -1 * decimal.Parse(dr["OVRAMT"].ToString().Trim());
                            genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                            genJrnEntry.trueEntity = dr["property"].ToString().Trim();

                            genJrnEntries.Add(genJrnEntry);
                        }
                    }
                    else  // flagged as not deposited.  Need to only do property cash side
                    {
                        genJrnEntry = new JournalEntry();
                        genJrnEntry.EntityID = dr["OVRENTITY"].ToString().Trim();
                        genJrnEntry.Description = string.Format("{0} Not Deposited", dr["DESCRPTN"].ToString().Trim());
                        genJrnEntry.AccountNumber = GetInterCompanyAccount(dr["OVRENTITY"].ToString().Trim());
                        genJrnEntry.Amount = -1 * decimal.Parse(dr["AMT"].ToString().Trim()) ;
                        genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                        genJrnEntry.trueEntity = dr["property"].ToString().Trim();
                        genJrnEntries.Add(genJrnEntry);
                        genJrnEntry = new JournalEntry();
                        genJrnEntry.EntityID = dr["OVRENTITY"].ToString().Trim();
                        genJrnEntry.Description = string.Format("{0} Not Deposited", dr["DESCRPTN"].ToString().Trim());
                        genJrnEntry.AccountNumber = "HS599000000000";  // Miscellaneous income 
                        genJrnEntry.Amount = decimal.Parse(dr["AMT"].ToString().Trim());
                        genJrnEntry.Period = year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0');
                        genJrnEntry.trueEntity = dr["property"].ToString().Trim();
                        genJrnEntries.Add(genJrnEntry); 
                    }
                }

                //get the Journal Entries for the improper depopsits 
                strSQL = $@"SELECT period, RECEIPTDAT, '970' REF, 'GJ' Source, '551'EntityId, ACCTNUM, SUM(Payment) AMT, SUM(OVRAMT) as OVRAMT, CONCAT('Corr Dep # ',depositid, ' - ',entityid, FORMAT(depositdate, 'MM/dd/yyyy'), ' ', BANKREF) DESCRPTN, s.entityid as property from
                            (SELECT dbo.udf_Base_ConvertDateToMRIPeriod(r.RECEIPTDAT) period, RECEIPTDAT, b.depositid, actdate depositdate, (pmt_check+ r.PMT_CASH) Payment, OVRAMT, b.ENTITYID, t.ACCTNUM, BANKREF, notdeposited FROM HS_BANKDEPLINE l JOIN HS_BANKDEP b ON L.DEPOSITID = b.DEPOSITID 
                            JOIN HS_SLRECEIPTS r ON L.RECEIPTNO = r.IRECEIPTNU AND CONVERT(INT, b.entityid) = CONVERT(INT, r.siteid)
                            JOIN bmap t ON T.entityid = b.ENTITYID   AND T.cashtype = b.cashtype
                            WHERE dbo.udf_Base_ConvertDateToMRIPeriod(r.RECEIPTDAT) = '{year.ToString() + month.ToString().PadLeft(2, '0')}' and (notdeposited is not null or ovramt is not null) ) s
                            GROUP BY PERIOD, RECEIPTDAT, depositid, depositdate, entityid, s.ACCTNUM, BANKREF
                            ORDER BY s.ENTITYID ASC, s.depositdate ASC";
                locTbl = sqlAgent.ExecuteQuery(strSQL);

                string lines = "";

                foreach (DataRow dr in locTbl.Tables[0].Rows)
                {
                    lines += string.Format(correctingDepositLineItem, string.Format("{0} was originally this amount {1} but was overridden to this amount {2}. The receipt date is {3}.", dr["DESCRPTN"].ToString().Trim(), dr["AMT"].ToString().Trim().PadLeft(1, '0'), dr["OVRAMT"].ToString().Trim().PadLeft(1,'0'), dr["RECEIPTDAT"].ToString().Trim()));
                }

                if (locTbl.Tables[0].Rows.Count > 0)
                {
                    body = string.Format(correctingDepositEmail, lines.Trim());
                }

                //get the totals of debits and credits
                credit = genJrnEntries.GetTotalCredits();
                debit = genJrnEntries.GetTotalDebits();

                //add each entry to the grid
                foreach (JournalEntry jrnEntry in genJrnEntries.GetItems())
                {
                    DataRow dr = locTblLedger.NewRow();
                    dr["ENTITYID"] = jrnEntry.EntityID.Trim();
                    dr["ACCTNAME"] = GetName(jrnEntry.AccountNumber.Trim());
                    dr["ACCTNUM"] = jrnEntry.AccountNumber.Trim();

                    if (jrnEntry.Amount < 0)
                    {
                        dr["CREDIT"] = jrnEntry.Amount;
                    }
                    else
                    {
                        dr["DEBIT"] = jrnEntry.Amount;
                    }

                    dr["DESCRIPTION"] = jrnEntry.Description.Trim();
                    dr["TRUEENTITY"] = jrnEntry.trueEntity.Trim();

                    locTblLedger.Rows.Add(dr);
                }

                //sort data and add the data to the grid
                dgvGeneralJournalEntries.DataSource = (from DataRow row in locTblLedger.Rows
                                                      orderby row["ENTITYID"] ascending, row["ACCTNUM"] ascending
                                                      select row).ToList().CopyToDataTable();

                dgvGeneralJournalEntries.Columns["TRUEENTITY"].Visible = false;

                FormatHeader();
                IsValid();
                dgvGeneralJournalEntries.Focus();
            }
            catch (NullReferenceException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (SqlException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (ArgumentNullException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (ConstraintException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (ArgumentException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (InvalidOperationException err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
            }
            finally
            {
                this.Useable();
            }
        }

        private string GetInterCompanyAccount(string propId)
        {
            try
            {
                string InterCompanyAccount = (from DataRow row in locTblEntities.Rows
                                              where row["ENTITYID"].ToString().Trim() == propId.Trim()
                                              select row["INTRACCT"].ToString().Trim()).Single();

                return InterCompanyAccount.Trim();
            }
            catch(ArgumentException err)
            {
                return "";
            }
            catch(InvalidOperationException err)
            {
                return "";
            }
        }

        private string GetName(string AcctNum)
        {
            try
            {
                string AccountName = (from DataRow row in locTblAccounts.Rows
                                      where row["ACCTNUM"].ToString().Trim() == AcctNum.Trim()
                                      select row["ACCTNAME"].ToString().Trim()).Single();

                return AccountName.Trim();
            }
            catch(ArgumentException err)
            {
                return "";
            }
            catch(InvalidOperationException err)
            {
                return "";
            }
        }

        /// <summary>
        /// Validates the data on the form.
        /// </summary>
        /// <returns>Return true if the data has no errors. Return false if there are errors.</returns>
        private bool IsValid()
        {
            try
            {
                //clear the errors for next run
                lstMessages.Items.Clear();

                DataSet locTbl;
                //this is individual error message placeholder
                string errorLineItem = "";

                //validates the that the credits and debits are equal
                if (genJrnEntries.GetTotalCredits() != -1 * genJrnEntries.GetTotalDebits())
                {
                    //clear the individual error message placeholder
                    errorLineItem = "";
                    errorLineItem = $"Credits {genJrnEntries.GetTotalCredits().ToString("c2")} need to equal the debits {(-1 * genJrnEntries.GetTotalDebits()).ToString("c2")}.";
                    MessageBox.Show($"{errorLineItem}", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lstMessages.Items.Add(errorLineItem.Trim());
                    HasErrors = true;
                }

                string errorLines = "";

                var locationsCash = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                     where (genJrn.AccountNumber.Contains("HS109") & 
                                            genJrn.EntityID.Trim() != "551")
                                     group genJrn by genJrn.EntityID.Trim() into locs
                                     select new
                                     {
                                         location = locs.Key.Trim(),
                                         amount = locs.Sum(x => x.Amount)
                                     });

                decimal cashTotal = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                     where (genJrn.Description.Trim() == "Checks" |
                                            genJrn.Description.Trim() == "Cash" |
                                            genJrn.Description.Trim() == "MoneyOrder" |
                                            genJrn.Description.Contains("Not Deposited")                                            )
                                     select genJrn.Amount).Sum();

                decimal depositTotal = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                        where genJrn.AccountNumber.Trim() == depositAccount 
                                        select genJrn.Amount).Sum();

                var descriptions = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                    where genJrn.AccountNumber.Trim() == depositAccount
                                    select genJrn.Description);

                //validates the that the cash totals and deposit totals are equal
                //if (cashTotal != depositTotal)
                //{
                    foreach(var genJrn in locationsCash)
                    {
                            var locationDeposits = (from JournalEntry entry in genJrnEntries.GetItems()
                                                where (entry.AccountNumber == depositAccount &
                                                entry.trueEntity == genJrn.location.Trim() 
                                       //       &  !entry.Description.Contains("Correction") 
                                       )
                                                select entry.Amount).Sum();

                        if (genJrn.amount != locationDeposits)
                        {
                            errorLineItem = "";
                            errorLineItem = $"• {genJrn.location.Trim()} deposits {locationDeposits} do not match the cash {genJrn.amount}. \n";
                            errorLines += errorLineItem.Trim();
                            lstMessages.Items.Add(errorLineItem.Trim());
                        }
                    }

                //    MessageBox.Show($"Cash total {cashTotal.ToString("c2")} need to equal the deposit total {depositTotal.ToString("c2")}.\nProperties with differences:\n{errorLines}", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    HasErrors = true;
                //}

                // check inter company cash accounts
                strSQL = "select * from dbo.hs_udf_GetIntercompanycashAcct()";
                locTbl = sqlAgent.ExecuteQuery(strSQL);
                foreach (DataRow dr in locTbl.Tables[0].Rows)
                {
                    decimal mgmtTotal = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                            where genJrn.AccountNumber.Trim() == dr["INTRACCT"].ToString().Trim() &
                                                  genJrn.EntityID.Trim() == dr["mgmt"].ToString().Trim() 
                                            select genJrn.Amount).Sum();
                    decimal entTotal = (from JournalEntry genJrn in genJrnEntries.GetItems()
                                         where genJrn.AccountNumber.Trim() == dr["INTRACCT"].ToString().Trim() &
                                               genJrn.EntityID.Trim() == dr["entityid"].ToString().Trim()
                                         select genJrn.Amount).Sum();
                    if (mgmtTotal != -entTotal)
                    {
                        errorLineItem = "";
                        errorLineItem = $"• {dr["entityid"].ToString()} Total {entTotal:C} for account {dr["INTRACCT"].ToString()} does not match Managment Co {-mgmtTotal:C}.\n\n\n";
                        errorLines += errorLineItem.Trim();
                        lstMessages.Items.Add(errorLineItem.Trim());
                        HasErrors = true;
                    }
                }


                //checks if the Journal Entries were already imported for the period
                //strSQL = "SELECT * FROM GL_JOURNAL_HISTORY " +
                //         " WHERE PERIOD = '" + year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0') + "'" +
                //         " AND REF = '970' AND SOURCE = 'GJ' AND SITEID = '@' AND ACTIONTYPE = 'CREATE'";

                //locTbl = sqlAgent.ExecuteQuery(strSQL);

                //if (locTbl.Tables[0].Rows.Count > 0)
                //{
                //    MessageBox.Show("The SiteLink data was already imported for this period. You cannot run it again.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    HasErrors = true;
                //    return false;
                //}

                return !HasErrors;
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
                HasErrors = true;
                return false;
            }
        }

        /// <summary>
        /// Writes a Journal Entry.
        /// </summary>
        /// <param name="counter">The counter for the sequence of the record in the batch. It is often used as the item number.</param>
        /// <param name="entityID">The location ID.</param>
        /// <param name="description">The account name or description.</param>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="period">The journal entry period.</param>
        /// <param name="accountNumber">The account number.</param>
        private void InsertJournalEntry(int counter, string entityID, string description, decimal amount, string period, string accountNumber)
        {
            try
            {
                List<SqlParameter> sqlParameters = new List<SqlParameter>();

                strSQL = "INSERT INTO JOURNAL(PERIOD, REF, SOURCE, SITEID, ITEM, ENTITYID, ACCTNUM, DEPARTMENT, AMT, DESCRPN, AddlDesc, ENTRDATE, " +
                             " REVERSAL, STATUS, BASIS, LASTDATE, USERID, OAMT, INTENTENTRY, INTENTTYPE, REVENTRY, OWNNUM, CTRYNUM, " +
                             " INTERFACEID, HS_JVNO, HS_GLBATCHNO) VALUES (@PERIOD, @REF, @SOURCE, @SITEID, @ITEM, @ENTITYID, @ACCTNUM, @DEPARTMENT, " +
                             " @AMT, @DESCRPN, @ADDLDESC,  @ENTRDATE, @REVERSAL, @STATUS, @BASIS, @LASTDATE, @USERID, @OAMT, @INTENTENTRY, @INTENTTYPE, " +
                             " @REVENTRY, @OWNNUM, @CTRYUM, @INTERFACEID, @HS_JVNO, @HS_GLBATCHNO)";

                sqlParameters.Add(new SqlParameter("@PERIOD", period.Trim()));
                sqlParameters.Add(new SqlParameter("@REF", "970"));
                sqlParameters.Add(new SqlParameter("@SOURCE", "GJ"));
                sqlParameters.Add(new SqlParameter("@SITEID", "@"));
                sqlParameters.Add(new SqlParameter("@ITEM", counter));
                sqlParameters.Add(new SqlParameter("@ENTITYID", entityID.Trim()));
                sqlParameters.Add(new SqlParameter("@ACCTNUM", accountNumber.Trim()));
                sqlParameters.Add(new SqlParameter("@DEPARTMENT", "@"));
                description = description.Trim();
                if (description.Length > 34)
                {
                    sqlParameters.Add(new SqlParameter("@DESCRPN", description.Substring(0, 33)));
                    sqlParameters.Add(new SqlParameter("@ADDLDESC", description.Substring(33, description.Length-34)));
                }
                else
                {
                    sqlParameters.Add(new SqlParameter("@DESCRPN", description.Trim()));
                    sqlParameters.Add(new SqlParameter("@ADDLDESC", "" ));
                }
                sqlParameters.Add(new SqlParameter("@ENTRDATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                sqlParameters.Add(new SqlParameter("@REVERSAL", "N"));
                sqlParameters.Add(new SqlParameter("@STATUS", "P"));
                sqlParameters.Add(new SqlParameter("@BASIS", "C"));
                sqlParameters.Add(new SqlParameter("@LASTDATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                sqlParameters.Add(new SqlParameter("@USERID", System.Security.Principal.WindowsIdentity.GetCurrent().Name.Trim()));
                sqlParameters.Add(new SqlParameter("@OAMT", "0.00"));
                sqlParameters.Add(new SqlParameter("@INTENTENTRY", "N"));
                sqlParameters.Add(new SqlParameter("@INTENTTYPE", "S"));
                sqlParameters.Add(new SqlParameter("@REVENTRY", "N"));
                sqlParameters.Add(new SqlParameter("@OWNNUM", "0"));
                sqlParameters.Add(new SqlParameter("@CTRYUM", "0"));
                sqlParameters.Add(new SqlParameter("@INTERFACEID", "0"));
                sqlParameters.Add(new SqlParameter("@HS_JVNO", "20"));
                sqlParameters.Add(new SqlParameter("@HS_GLBATCHNO", "970"));
                sqlParameters.Add(new SqlParameter("@AMT", amount));
                sqlAgent.ExecuteNonQuery(strSQL, sqlParameters);
                sqlParameters.Clear();

            }
            catch (SqlException err)
            {
                HasErrors = true;
                switch (err.Number)
                {
                    //handle error when the record being added is a duplicate
                    case 2627:
                        lstMessages.Items.Add(string.Format("Ref. {0} Site ID {1} Source {2} Item {3} Period {4} already exists. \n \n",
                                                          "970", "@", "GJ", counter, period.Trim()));
                        throw err;
                    //handle error when a value being added is in conflict with a foreign key constraint
                    case 547:
                        lstMessages.Items.Add(string.Format("Ref. {0} Site ID {1} Source {2} Item {3} Period {4} has a value that conflicts with valid values. \n " +
                                                          err.Message + " \n \n",
                                                          "970", "@", "GJ", counter, period.Trim()));
                        throw err;
                    default:
                        lstMessages.Items.Add(string.Format("Ref. {0} Site ID {1} Source {2} Item {3} Period {4} was not added because of an error. \n" +
                                                          err.Message + "\n \n",
                                                         "970", "@", "GJ", counter, period.Trim()));
                        throw err;
                }
            }
        }

        private void btnPostToJournal_Click(object sender, EventArgs e)
        {
            try
            {
                //indicates to the user that program is operating
                this.Wait();
                //sets the error indicator to false
                HasErrors = false;

                List<MailAddress> emailTo = new List<MailAddress>();


                    //sets the counter to 1
                    int counter = 1;
                    DataSet locTbl = new DataSet();

                //if valid
                if (IsValid())
                {
                    sqlAgent.BeginTransaction();
                    //log entry that the Journal Entries were created, the date, and by who
                    string sqlJournalHistory = string.Format("insert into GL_Journal_History (period, ref, source, siteid, actiontype, lastdate, userid) VALUES " +
                        "( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )",
                        year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0'), "970",
                        "GJ", "@", "CREATE", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), System.Security.Principal.WindowsIdentity.GetCurrent().Name);

                    sqlAgent.ExecuteNonQuery(sqlJournalHistory);

                    decimal amount = 0M;

                    DataTable locTblMeta = new DataTable();
                    DataTable locTblB = new DataTable("SiteLink Upload " + cboPeriod.Text.Trim().Replace("/", "-"));
                    DataSet locTblBatch = new DataSet();

                    locTblMeta.Columns.Add("worksheetname");
                    locTblMeta.Columns.Add("skipflag");
                    locTblMeta.Columns.Add("jeVoNo");
                    locTblMeta.Columns.Add("reversal");
                    locTblMeta.Columns.Add("ref");
                    locTblMeta.Columns.Add("batch");

                    locTblB.Columns.Add("ENTITYID");
                    locTblB.Columns.Add("DEPARTMENT");
                    locTblB.Columns.Add("CLASS");
                    locTblB.Columns.Add("DEBIT");
                    locTblB.Columns.Add("CREDIT");
                    locTblB.Columns.Add("Z");
                    locTblB.Columns.Add("DESCRPN");

                    DataRow headerRow = locTblMeta.NewRow();

                    headerRow["worksheetname"] = "SiteLink Upload " + cboPeriod.Text.Trim().Replace("/", "-");
                    headerRow["skipflag"] = "N";
                    headerRow["jeVoNo"] = "20";
                    headerRow["reversal"] = "N";
                    headerRow["ref"] = "970";
                    headerRow["batch"] = "";

                    locTblMeta.Rows.Add(headerRow);

                    foreach (JournalEntry genJrn in genJrnEntries.GetItems())
                    {
                        InsertJournalEntry(counter, genJrn.EntityID.Trim(), genJrn.Description.Trim(), genJrn.Amount, genJrn.Period, genJrn.AccountNumber);
                        DataRow row = locTblB.NewRow();

                        row["ENTITYID"] = genJrn.EntityID.Trim();
                        row["DEPARTMENT"] = genJrn.Department.Trim();
                        row["CLASS"] = genJrn.AccountNumber.Trim();
                        if(genJrn.Amount > 0)
                        {
                            row["DEBIT"] = genJrn.Amount;
                            row["CREDIT"] = "0";
                        } 
                        else if(genJrn.Amount < 0)
                        {
                            row["DEBIT"] = "0";
                            row["CREDIT"] = genJrn.Amount;
                        }
                        else
                        {
                            row["DEBIT"] = "0";
                            row["CREDIT"] = "0";
                        }
                        row["Z"] = "Z";
                        row["descrpn"] = genJrn.Description.Trim();

                        locTblB.Rows.Add(row);

                        if (genJrn.Amount > 0)
                        {
                            amount += genJrn.Amount;
                        }

                        counter++;
                    }

                    locTblBatch.Tables.Add(locTblMeta);
                    locTblBatch.Tables.Add(locTblB);

                    SqlTransactions sqlTransactions = new SqlTransactions(string.Format(CONNECTION_STRING, ConfigurationManager.AppSettings["dbName"]));
                    sqlTransactions.Connect();

                    MemoryStream stream = new MemoryStream();
                    XLWorkbook workbook = CreateJournalBatch(locTblBatch, "SiteLink Upload " + year.ToString().Trim() +
                                                                         month.ToString().Trim().PadLeft(2, '0'),
                                                                         DateTime.Now);
                    workbook.SaveAs(stream);

                    byte[] fileStream = stream.ToArray();

                    sqlJournalHistory = string.Format("insert into hs_glbnctl (glsource, glref, period, basis, batchamt, release, sent, descr, userid, BatchFileName) VALUES " +
                                  "('{0}', '{1}', '{2}', '{3}', {4}, '{5}', 'N', '{6}', '{7}', '{8}')",
                    "GJ", "970", year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0'), "C", amount, "N", "SiteLink Upload", System.Security.Principal.WindowsIdentity.GetCurrent().Name, "SiteLink Upload " + cboPeriod.Text.Trim().Replace("/", "-") + ".xlsx");

                    sqlAgent.ExecuteNonQuery(sqlJournalHistory);

                    sqlAgent.CommitTransaction();

                    sqlTransactions.StreamFile("HS_GLBNCTL", "BatchFile",
                                                $@"PERIOD = '{year.ToString().Trim() + month.ToString().Trim().PadLeft(2, '0')}' 
                                                                       AND GLREF = '970' AND GLSOURCE = 'GJ'",
                                                fileStream);

                    amount = 0M;

                    if (body.Trim() != "" | true)
                    {
                        //compare the totals of the Cash, Check, and Money Orders to the Cash Management Summary
                        if (Debugger.IsAttached)
                        {
                            DataSet locTblEmails = new DataSet();
                            locTblEmails = sqlAgent.ExecuteQuery(@"SELECT HSVAL FROM HS_CODEVAL
                                                              WHERE HSTABLE = 'EMLGRP' AND HSCOL = 'ACTEMLADR' AND HSVAL <> '@@@@@'");

                            foreach (DataRow row in locTblEmails.Tables[0].Rows)
                            {
                                emailTo.Add(new MailAddress(row["HSVAL"].ToString().Trim()));
                            }

                            emailTo.Clear();

                            emailTo.Add(new MailAddress("jkelly@hillmgt.com"));
                            emailTo.Add(new MailAddress("jhall@hillmgt.com"));
                        }
                        else
                        {//send to the accounting group
                            DataSet locTblEmails = new DataSet();
                            locTblEmails = sqlAgent.ExecuteQuery(@"SELECT HSVAL FROM HS_CODEVAL
                                                              WHERE HSTABLE = 'EMLGRP' AND HSCOL = 'ACTEMLADR' AND HSVAL <> '@@@@@'");

                            foreach (DataRow row in locTblEmails.Tables[0].Rows)
                            {
                                emailTo.Add(new MailAddress(row["HSVAL"].ToString().Trim()));
                            }
                        }

                        string recipients = "";

                        foreach (MailAddress email in emailTo)
                        {
                            recipients += email.Address.Trim() + ";";
                        }

                        try
                        {
                            body = body.Trim().Replace('\'', '\"');
                            sqlAgent.ExecuteQuery($@"EXEC msdb.dbo.sp_send_dbmail
	                                                    @profile_name = N'Notifications',
	                                                    @recipients = N'{recipients.Trim()}',
	                                                    @subject = 'SiteLink Correcting Deposits',
	                                                    @body = '{body.Trim()}',
	                                                    @body_format = 'HTML'");
                        }//ignore the errors for now
                        catch (Exception err)
                        {

                        }
                    }
                }
            }
            catch (Exception err)
            {
                HelpfulFunctions.SendErrorToHelpDesk(HelpfulFunctions.HelpDesk.HOME_SALES, HelpfulFunctions.UserID.Trim(), HelpfulFunctions.ProgramName.Trim(),
                                                    err.Message.Trim(), err.StackTrace.Trim(), err.GetType().Name, true);
                HasErrors = true;
                sqlAgent.RollbackTransaction();
            }
            finally
            {
                if (!HasErrors)
                {
                    MessageBox.Show("The SiteLink data was imported successfully.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("There was an error importing some or all the SiteLink data. Please ask for help.", "Attention!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lstMessages.Items.Add(string.Format("[{0}] ERROR: There was an error importing some or all the SiteLink data. Please ask for help.", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")));
                }

                cboPeriod.SelectedIndex = 0;
                genJrnEntries.Clear();
                dgvGeneralJournalEntries.DataSource = null;
                this.Useable();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = $"C:\\Users\\{HelpfulFunctions.UserID.Split('\\')[1].Trim()}\\Documents\\errorExport.txt";

                if (File.Exists(filename))
                    File.Delete(filename);

                for (int i = 0; i < lstMessages.Items.Count; i++)
                {
                    File.AppendAllText(filename, lstMessages.Items[i].ToString() + "\n");
                }

                Process.Start("notepad.exe", filename);
            }
            catch (Exception err)
            {
                //HelpfulClasses.HelpfulFunctions.SendErrorToHelpDesk(HelpfulClasses.HelpfulFunctions.HelpDesk.HOME_SALES,
                //                                                    HelpfulClasses.HelpfulFunctions.UserID.Trim(),
                //                                                    HelpfulClasses.HelpfulFunctions.ProgramName.Trim(),
                //                                                    err.Message.Trim(), err.StackTrace.Trim(),
                //                                                    err.GetType().Name.Trim(), false, "", null, false);

                HelpfulClasses.HelpfulFunctions.LogMessage(err.Message.Trim(), System.Diagnostics.EventLogEntryType.Error);
                HasErrors = true;
                sqlAgent.RollbackTransaction();
            }
        }

        private void dgvGeneralJournalEntries_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if(e.ColumnIndex == dgvGeneralJournalEntries.Columns["DESCRIPTION"].Index)
            {
                if (e.Value.ToString().Trim().ToUpper().Contains("CORR"))
                {
                    foreach (DataGridViewCell c in dgvGeneralJournalEntries.Rows[e.RowIndex].Cells)
                    {
                        c.Style.BackColor = System.Drawing.Color.DarkGoldenrod;
                    }
                }
                if (e.Value.ToString().Trim().ToUpper().Contains("NOT DEP"))
                {
                    foreach (DataGridViewCell c in dgvGeneralJournalEntries.Rows[e.RowIndex].Cells)
                    {
                        c.Style.BackColor = System.Drawing.Color.DarkGoldenrod;
                    }
                }
            }

            if (e.ColumnIndex == dgvGeneralJournalEntries.Columns["CREDIT"].Index)
            {
                foreach (DataGridViewCell c in dgvGeneralJournalEntries.Rows[e.RowIndex].Cells)
                {
                    e.Value = e.Value.ToString().Trim().Replace("-", "");
                }
            }
        }

        class JournalSheet
        {
            public enum WorkSheetType
            {
                SKIP,
                POSTING
            }

            private string _workSheetName = "";
            private int _jeVoNo = 0;
            private DateTime _transactionDate = DateTime.MinValue;
            private bool _reversalFlag = false;
            private string _ref = "";
            private string _batch = "";
            private WorkSheetType _workSheetType = WorkSheetType.SKIP;
            private DataTable _data = new DataTable();

            public string WorkSheetName { get => _workSheetName; set => _workSheetName = value; }
            public int JeVoNo { get => _jeVoNo; set => _jeVoNo = value; }
            public DateTime TransactionDate { get => _transactionDate; set => _transactionDate = value; }
            public bool ReversalFlag { get => _reversalFlag; set => _reversalFlag = value; }
            public string Ref { get => _ref; set => _ref = value; }
            public string Batch { get => _batch; set => _batch = value; }
            internal WorkSheetType workSheetType { get => _workSheetType; set => _workSheetType = value; }
            public DataTable Data { get => _data; set => _data = value; }
        }


    }
}
