﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JournalImportWiz
{
    public class PageInfo
    {
        public string UserId = string.Empty;
        public string Source = string.Empty;
        public string ReferenceNumber = string.Empty;
        public decimal ExpectedBatchTotal = 0;
        public string Period = string.Empty;
        public string Basis = string.Empty;
        public bool IsTaxBasis;
        public string jve = string.Empty;
        public string ExcelFileName = string.Empty;
        public List<string> ValidationMessages = new List<string>();
        public JournalEntries JournalRows = new JournalEntries();
        public bool ErrorHasOccurred = false;
        public decimal GrandTotalDebit = 0;
        public decimal GrandTotalCredit = 0;
        public decimal NonAs400Total = 0;
//        public string AccountNumber = string.Empty;
        public string JobCode = string.Empty;
        public string Reversing = string.Empty;
        public string DatabaseName = string.Empty;
        public string Description = string.Empty;
        public string Release = "N";
        public bool PriorPeriod = true;
        public List<string> CompletionMessages = new List<string>();
        public string JournalTableName
        {
            get
            {
                if (PriorPeriod)
                {
                    return "GHIS";
                }
                else
                {
                    return "JOURNAL";
                }
            }
        }
        public bool IsPeriodClosed(string entityid, string period, int rowNum)
        {
            DataSet dscls = new DataSet();
            string sql = string.Format("select DATECLSD from period where entityid = '{0}' and period = {1}", entityid, period);

//            SELECT @maxopen = maxopen FROM entity WHERE entityid = '067'

//SELECT dtxhill.dbo.addperiod((MIN(period)), @maxopen) FROM period WHERE entityid = '067' AND DATECLSD IS NULL GROUP BY entityid


            try
            {
                SqlAgent sa = new SqlAgent(this);
                dscls = sa.ExecuteQuery(sql);
            }
            catch
            {
                //throw new Exception(string.Format("The period {0} is closed or not available yet.",
                //    period, entityid, rowNum));
            }

            if (dscls.Tables.Count > 0 && dscls.Tables[0].Rows.Count > 0)   // Found!
            {
                if (dscls.Tables[0].Rows[0][0] == DBNull.Value)
                    return false;
                else
                    return true;
            }
            else
            {
                //throw new Exception(string.Format("The period {0} is closed or not available yet.",
                //    period, entityid, rowNum));

            }

            return false;
        }


    }
}
