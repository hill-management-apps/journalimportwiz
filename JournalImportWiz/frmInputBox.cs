﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    public partial class frmInputBox : Form
    {
        public string TextResult = string.Empty;
        public bool OKWasPressed = false;

        public frmInputBox()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OKWasPressed = true;
            TextResult = txtInput.Text;
            this.Close();
        }
    }
}
