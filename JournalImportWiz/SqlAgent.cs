﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;



namespace JournalImportWiz
{
    public class SqlAgent
    {
        private string userId = string.Empty;
        //const string CONNECTION_STRING = @"data source=HMFINSQL;initial catalog=ZTESTHS;Integrated Security=true;";
        const string CONNECTION_STRING = @"data source=HMFINSQL;initial catalog={0};Integrated Security=true;";
        string connStr = string.Empty;

        // Declared here so we can do a multi-part transaction
        // from outside this class.
        SqlConnection cn = null;
        SqlTransaction trans = null;
        SqlCommand command = null;
        string dbName = string.Empty;

        public string DbName { get => dbName; }

        public SqlAgent(PageInfo pi)
        {
            dbName = pi.DatabaseName;
            connStr = string.Format(CONNECTION_STRING, dbName);
        }

        public SqlAgent(string databaseName)
        {
            dbName = databaseName;
            connStr = string.Format(CONNECTION_STRING, dbName);
        }

        public DataSet ExecuteQuery(string sql)
        {
            DataSet ds = new DataSet();
            return this.ExecuteQuery(sql, connStr);
        }

        public DataSet ExecuteQuery(string sql, string connectString)
        {
            DataSet ds = new DataSet();
            //logger.LogMessage("Here in SqlAgent.ExecuteQuery");

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connectString);
                da.Fill(ds);
                return ds;
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }

        public DataSet ExecuteNonQueryConn(string sql)
        {
            DataSet ds = new DataSet();
            //logger.LogMessage("Here in SqlAgent.ExecuteQuery");

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, cn);
                da.Fill(ds);
                return ds;
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
        }

        public void ExecuteNonQuery(string sql, List<SqlParameter> sqlParameters)
        {
            command.Parameters.Clear();

            command.CommandText = sql;

            foreach(SqlParameter sqlParameter in sqlParameters)
            {
                command.Parameters.Add(sqlParameter);
            }

            command.ExecuteNonQuery();
            //            DataSet ds = new DataSet();
        }

        public void ExecuteNonQuery(string sql)
        {
            command.CommandText = sql;
            command.ExecuteNonQuery();
            //            DataSet ds = new DataSet();
        }

        public void ExecuteNonQuery(string sql, string connectionString)
        {
            try
            {
                if (command == null)
                    throw new Exception("Command object is null.  Have you started a SQL Transaction?");

                command.CommandText = sql;
                command.ExecuteNonQuery();
            }
            catch (Exception _ex)
            {
                throw;
            }
        }

        public void BeginTransaction()
        {
            try
            {
                if (cn == null)
                    cn = new SqlConnection(connStr);

                if (command == null)
                    command = new SqlCommand();

                command.Connection = cn;
                command.Connection.Open();
                trans = cn.BeginTransaction(IsolationLevel.ReadUncommitted);
                command.Transaction = trans;
            }
            catch
            {
                throw;
            }
        }

        public void CommitTransaction()
        {
            try
            {
                trans.Commit();
                cn.Dispose();
                cn = null;
            }
            catch
            {
                throw;
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                trans.Rollback();
                cn.Dispose();
                cn = null;
            }
            catch(Exception err)
            {
                
            }
        }
    }
}
