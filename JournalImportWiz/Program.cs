﻿using JournalImport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JournalImportWiz
{
    static class Program
    {
        private static bool _IsRecurring = false;
        readonly public static string userID = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

        public static bool IsRecurring { get => _IsRecurring; set => _IsRecurring = value; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

                if (args.Length != 0)
                {
                    switch (args[0].Trim().ToLower())
                    {
                        case "conservice":
                            Application.Run(new frmHomeSalesConservice());
                            break;
                        case "summary":
                            Application.Run(new frmJournalImport(true));
                            break;
                        case "sitelink":
                            Application.Run(new frmInterfaceSiteLink());
                            break;
                    }
                }
                else
                {
                    Application.Run(new frmJournalImport());
                }
            }

        static public void WriteErrorLog(string[] errorMessages, string logName)
        {
            string filename = "errorExport.txt";

            if (File.Exists(filename))
                File.Delete(filename);

            foreach(string error in errorMessages)
            {
                File.AppendAllText(filename, error);
            }

            Process.Start("notepad.exe", filename);
        }
        }
    }
